import numpy as np
import numpy.testing as npt

from nutopy.tools import *
import nutopy.nle as nle

def test_type_float():

    # float
    sol = nle.solve(f=lambda x: 2.0 * x - 1.0, x0=0.0)
    assert type(sol.x) == float

    # numpy float
    x0=np.float64(0.0)
    sol = nle.solve(lambda x: 2.0 * x - 1.0, x0)
    assert type(x0) == np.float64
    assert isinstance(x0, float) == True
    assert type(sol.x) == np.float64
    assert isinstance(sol.x, float) == True

def test_scalar():
    #
    sol = nle.solve(f=lambda x: 2.0 * x - 1.0, x0=0.0)
    assert sol.x == 0.5

    # with derivative
    def df(x, dx):
        return 2.0

    @tensorize(df)
    def f(x):
        return 2.0 * x - 1.0


    sol = nle.solve(f, 0.0, df=f)
    assert sol.x == 0.5

def test_numpy():
    sol = nle.solve(lambda x: np.array([2.0 * x[0] - 1.0, np.exp(x[1]) - 1.0]), np.array([10.0, 2.0]))
    npt.assert_array_almost_equal(sol.x, [0.5, 0.0])


def test_numpy_jacobian_options():
    sol = nle.solve(f=lambda x: np.array([2.0 * x[0] - 1.0, np.exp(x[1]) - 1.0]),
                    x0=(np.array([10.0, 2.0])),
                    df=lambda x: np.array([[2.0, 0], [0, np.exp(x[1])]]),
                    options=(nle.Options(SolverMethod='hybrj', Display='on', TolX=1e-8)))
    npt.assert_array_almost_equal(sol.x, [0.5, 0.0])


def test_function_arguments():
    sol = nle.solve(f=lambda x, a, b, c=1.0: np.array([a * x[0] - b, np.exp(x[1]) - c]),
                    x0=np.array([10.0, 2.0]),
                    args=(2.0, 1.0),
                    kwargs={'c': 1.0})
    npt.assert_array_almost_equal(sol.x, [1.0 / 2.0, np.log(1.0)])

def test_df_order():

    def df(x, dx):
        dy = np.array([2.0 * dx[0], np.exp(x[1]) * dx[1]])
        return dy

    @tensorize(df)
    def f(x):
        y = np.array([2.0 * x[0] - 1.0, np.exp(x[1]) - 1.0])
        return y

    sol = nle.solve(f, np.array([10.0, 2.0]), df=f)
    npt.assert_array_almost_equal(sol.x, [0.5, 0.0])

#def test_df_tensorized_but_not_given():
#
#    def df(x, dx):
#        dy = np.array([2.0 * dx[0], 2.0 * dx[1]])
#        return dy
#
#    @tensorize(df)
#    def f(x):
#        y = np.array([2.0 * x[0] - 1.0, 2.0 * x[1] - 1.0])
#        return y
#
#    sol  = nle.solve(f, np.array([10.0, 2.0]))
#    sol2 = nle.solve(f, np.array([10.0, 2.0]), df=f)
#    npt.assert_array_almost_equal(sol.x, [0.5, 0.5])
#    npt.assert_equal(sol.nfev, sol2.nfev)


class TestCallback:

    def my_fun(x):
        return np.array([2.0 * x[0] - 1.0])

    def test_callback(self):
        sol = nle.solve(f=TestCallback.my_fun, x0=np.array([0.0]), callback=lambda infos: 0)
        assert sol.status == 1

    def test_callback_stop(self):
        sol = nle.solve(f=TestCallback.my_fun, x0=np.array([0.0]), callback=lambda infos: -1)
        assert sol.status == -1

class TestCallbackArgument:

    def my_fun(x, a):
        return np.array([a * x[0] - 1.0, np.exp(x[0] + x[1]) - 1.0])

    def test_callback_with_arg(self):
        sol = nle.solve(f=TestCallbackArgument.my_fun,
                        x0=np.array([0.0, 0.0]),
                        callback=lambda infos, a: 0,
                        args=2.0)
        assert sol.status == 1

    def test_callback_with_arg_but_not_given(self):
        sol = nle.solve(f=TestCallbackArgument.my_fun,
                        x0=np.array([0.0, 0.0]),
                        callback=lambda infos: 0,
                        args=2.0)
        assert sol.status == 1

    def test_callback_root(self):
        sol = nle.solve(f=TestCallbackArgument.my_fun,
                        x0=np.array([0.0, 0.0]),
                        options=(nle.Options(SolverMethod='broyden1')),
                        callback=lambda infos, a: 0,
                        args=2.0)
        assert sol.status == 1
