import time
import numpy as np

from nutopy.tools import *
import nutopy.path as path

def f(x, p, a):
    d, x, p = order(x, p)
    if d is 0:
        pass
    elif d is 1:
        x, dx = x
        p, dp = p
    else:
        raise WrongOrder(x)

    y    = np.zeros(x.size)
    y[0] = a * x[0] - p[0]
    y[1] = np.exp(x[0] + x[1]) - p[1]
    if d is 0:
        return y

    dy    = np.zeros(x.size)
    dy[0] = a * dx[0] - dp[0]
    dy[1] = np.exp(x[0] + x[1]) * ( dx[0] + dx[1] ) - dp[1]

    return y, dy

x0 = np.array([0.0, 0.0])
p0 = np.array([0.0, 1.0])
pf = np.array([1.0, 2.0])
a  = 2.0

opt = path.Options(Display='off');
et = time.time();
for i in range(200):
    sol = path.solve(f, x0, p0, pf, args=(a), df=f, options=opt)
et = time.time() - et
print('time = ', et)

# entre 1.18 et 1.22 pour 200 xp
# maintenant 1.1
