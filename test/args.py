def dargs(f, order, tvars, *args):
    """
        Parameters
        ----------

        f: callable
            create right tuple from one argument with its increments

        order: int

        tvars: tuple

        Returns
        -------

        tuple...
    """
    y = ()
    i = 0
    j = 1
    n = len(args)
    while i<n:
        if j in tvars :
            y = y + (f(args[i:i+order+1]), )
            i = i + order + 1
        else:
            y = y + ((args[i]), )
            i = i + 1
        j = j + 1
    return y

fun = lambda x : (x[0], 0, x[1])
order = 1
tvars = (1,3)
y     = dargs(fun, order, tvars, 1, 1, 2, 3, 3)
print(y)
