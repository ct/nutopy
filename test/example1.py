import matplotlib.pyplot as plt
import numpy as np
from nutopy import ivp

def f(t, x):
    dx = np.zeros(x.size)
    dx[0] = -x[0] + x[1]
    dx[1] = x[1]
    return dx

x0 = np.array([-1.0, 1.0])
t0 = 0.0
tf = 1.0
sol = ivp.exp(f, tf, t0, x0)
print(sol.xf)

plt.plot(sol.tout, sol.xout[:,0], 'b', sol.tout, sol.xout[:,1], 'r')
plt.show()
