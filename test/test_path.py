import numpy as np
import numpy.testing as npt
import nutopy.path as path
from nutopy.tools import *

class TestPath:

    def test_scalar(self):
        # Example 1: scalar, no Jacobian, no options, no optional arguments
        def myfun(x, l):
            F = np.zeros(x.size)
            F[0] = 2.0 * x[0] - l[0]
            return F

        x0 = np.array([0.0])
        l0 = np.array([0.0])
        lf = np.array([1.0])

        sol = path.solve(myfun, x0, l0, lf)

        npt.assert_array_almost_equal(sol.xf, lf / 2.0)

    def test_no_numpy(self):
        # Example 1: no numpy array
        x0 = 0.0
        l0 = 0.0
        lf = 1.0

        sol = path.solve(lambda x, l: 2 * x - l, x0, l0, lf)

        npt.assert_almost_equal(sol.xf, lf / 2.0)

    def test_optional_arguments(self):
        # Example 2: optional arguments, options, Jacobian by finite diff'
        def myfun(x, l, a):
            F = np.zeros(x.size)
            F[0] = a * x[0] - l[0]
            F[1] = np.exp(x[0] + x[1]) - 1.0
            return F

        x0 = np.array([0.0, 0.0])
        l0 = 0.0
        lf = 1.0
        opt = path.Options(ODESolver='dopri5', MaxIterCorrection=0);
        a = 2.0

        sol = path.solve(myfun, x0, l0, lf, options=opt, args=a)  # not a tuple
        npt.assert_array_almost_equal(sol.xf, np.array([lf / a, -lf / a]))

        sol = path.solve(myfun, x0, l0, lf, options=opt, args=(a,)) # tuple
        npt.assert_array_almost_equal(sol.xf, np.array([lf / a, -lf / a]))

    def test_jacobian(self):
        # Example 2: with Jacobian
        def myfun(x, l, a):
            F = np.zeros(x.size)
            F[0] = a * x[0] - l[0]
            F[1] = np.exp(x[0] + x[1]) - 1.0
            return F

        x0 = np.array([0.0, 0.0])
        l0 = 0.0
        lf = 1.0
        opt = path.Options(ODESolver='dopri5', MaxIterCorrection=0)
        a = 2.0

        def myjac(x, l, a):
            n = x.size
            m = l.size
            dFdx = np.zeros((n, n))
            dFdl = np.zeros((n, m))
            #
            dFdx[0, 0] = a
            dFdx[1, 0] = np.exp(x[0] + x[1])
            dFdx[1, 1] = np.exp(x[0] + x[1])
            #
            dFdl[0, 0] = -1.0
            return (dFdx, dFdl)

        sol = path.solve(myfun, x0, l0, lf, options=opt, args=(a), df=myjac)

        npt.assert_array_almost_equal(sol.xf, np.array([lf / a, -lf / a]))

    def test_change_all_options(self):
        # Example 1: scalar, no Jacobian, no options, no optional arguments
        def myfun(x, l):
            F = np.zeros(x.size)
            F[0] = 2.0 * x[0] - l[0]
            return F

        x0 = np.array([0.0])
        l0 = np.array([0.0])
        lf = np.array([1.0])

        opt = path.Options( DispIter            = 2,
                            Display             = 'off',
                            DoSavePath          = 'off',
                            MaxArcLength        = 1e3,
                            MaxFunNorm          = 1e-2,
                            MaxSteps            = 100,
                            MaxStepSize         = 1.0,
                            ODESolver           = 'radau5',
                            StopAtTurningPoint  = 1,
                            TolOdeAbs           = 1e-8,
                            TolOdeRel           = 1e-6,
                            TolHomparfinalStep  = 1e-6,
                            TolHomParEvolution  = 1e-6,
                            TolXCorrectionStep  = 1e-6,
                            MaxIterCorrection   = 10,
                            MaxStepSizeHomPar   = 1.0)

        sol = path.solve(myfun, x0, l0, lf, options=opt)

        print(sol)
        npt.assert_array_almost_equal(sol.xf, lf / 2.0)

class TestParams:

    def test_no_jacobian(self):
        # Example 1: no Jacobian
        def myfun(x, l, a):
            F = np.zeros(x.size)
            F[0] = a * x[0] - l[0]
            F[1] = np.exp(x[0] + x[1]) - l[1]
            return F

        x0 = np.array([0.0, 0.0])
        l0 = np.array([0.0, 1.0])
        lf = np.array([1.0, 2.0])
        a = 2.0

        sol = path.solve(myfun, x0, l0, lf, args=(a))

        npt.assert_array_almost_equal(sol.xf, np.array([lf[0] / a, np.log(lf[1]) - lf[0] / a]))

    def test_jacobian(self):
        # Example 2: with Jacobian
        def myfun(x, l, a):
            F = np.zeros(x.size)
            F[0] = a * x[0] - l[0]
            F[1] = np.exp(x[0] + x[1]) - l[1]
            return F

        def myjac(x, l, a):
            n = x.size
            m = l.size
            dFdx = np.zeros((n, n))
            dFdl = np.zeros((n, m))
            #
            dFdx[0, 0] = a
            dFdx[1, 0] = np.exp(x[0] + x[1])
            dFdx[1, 1] = np.exp(x[0] + x[1])
            #
            dFdl[0, 0] = -1.0
            dFdl[1, 1] = -1.0
            return (dFdx, dFdl)

        x0 = np.array([0.0, 0.0])
        l0 = np.array([0.0, 1.0])
        lf = np.array([1.0, 2.0])
        a = 2.0

        sol = path.solve(myfun, x0, l0, lf, args=(a), df=myjac)

        npt.assert_array_almost_equal(sol.xf, np.array([lf[0] / a, np.log(lf[1]) - lf[0] / a]))


    def test_df_order(self):
        def df(x, dx, p, dp, a):
            dy    = np.zeros(x.size)
            dy[0] = a * dx[0] - dp[0]
            dy[1] = np.exp(x[0] + x[1]) * ( dx[0] + dx[1] ) - dp[1]
            return dy
            
        @tensorize(df, tvars=(1, 2))
        def f(x, p, a):
            y    = np.zeros(x.size)
            y[0] = a * x[0] - p[0]
            y[1] = np.exp(x[0] + x[1]) - p[1]
            return y

        x0 = np.array([0.0, 0.0])
        p0 = np.array([0.0, 1.0])
        pf = np.array([1.0, 2.0])
        a  = 2.0

        sol = path.solve(f, x0, p0, pf, args=(a), df=f)
        npt.assert_array_almost_equal(sol.xf, np.array([pf[0] / a, np.log(pf[1]) - pf[0] / a]))

#    def test_df_tensorized_but_not_given(self):
#        def df(x, dx, p, dp, a):
#            dy    = np.zeros(x.size)
#            dy[0] = a * dx[0] - dp[0]
#            dy[1] = np.exp(x[0] + x[1]) * ( dx[0] + dx[1] ) - dp[1]
#            return dy
#
#        @tensorize(df, tvars=(1, 2))
#        def f(x, p, a):
#            y    = np.zeros(x.size)
#            y[0] = a * x[0] - p[0]
#            y[1] = np.exp(x[0] + x[1]) - p[1]
#            return y
#
#        x0 = np.array([0.0, 0.0])
#        p0 = np.array([0.0, 1.0])
#        pf = np.array([1.0, 2.0])
#        a  = 2.0
#
#        sol  = path.solve(f, x0, p0, pf, args=(a))
#        sol2 = path.solve(f, x0, p0, pf, df=f, args=(a))
#        npt.assert_array_almost_equal(sol.xf, np.array([pf[0] / a, np.log(pf[1]) - pf[0] / a]))
#        npt.assert_equal(len(sol.dets), len(sol2.dets))

class TestCallback:

    def test_callback(self):
        # Example 1:
        def myfun(x, l):
            F = np.zeros(x.size)
            F[0] = 2.0 * x[0] - l[0]
            return F

        def callback(infos):
            if infos.x[0] >= 1.0 / 4.0:
                return -11
            return 0

        x0 = np.array([0.0])
        l0 = np.array([0.0])
        lf = np.array([1.0])

        sol = path.solve(myfun, x0, l0, lf, callback=callback)

        print(sol.message)
        assert sol.status == -11

    def test_callback_optional_args(self):
        # Example 2: with an optional argument
        def myfun(x, l, a):
            F = np.zeros(x.size)
            F[0] = a * x[0] - l
            F[1] = np.exp(x[0] + x[1]) - 1.0
            return F

        def callback(infos, a):
            if infos.x[0] >= 1.0 / 4.0:
                return -11
            return 0

        x0 = np.array([0.0, 0.0])
        l0 = 0.0
        lf = 1.0
        opt = path.Options(ODESolver='dopri5', MaxIterCorrection=0);
        a = 2.0

        sol = path.solve(myfun, x0, l0, lf, options=opt, callback=callback, args=(a))

        assert sol.status == -11

    def test_callback_with_arg_but_not_given(self):
        # Example 2: with an optional argument not given to the callback function
        def myfun(x, l, a):
            F = np.zeros(x.size)
            F[0] = a * x[0] - l
            F[1] = np.exp(x[0] + x[1]) - 1.0
            return F

        def mycallback(infos):
            if (infos.x[0] >= 1.0 / 4.0):
                return -11
            return 0

        x0 = np.array([0.0, 0.0])
        l0 = 0.0
        lf = 1.0
        opt = path.Options(ODESolver='dopri5', MaxIterCorrection=0);
        a = 2.0

        sol = path.solve(myfun, x0, l0, lf, options=opt, callback=mycallback, args=(a))

        assert sol.status == -11
