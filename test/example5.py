import numpy as np
from nutopy import ivp

def d2f(t, x, dx1, dx2):
    y       = t / np.sin(x)
    dfdx    = - t * np.cos(x) / (np.sin(x) ** 2)
    dy      = dfdx * dx1
    d2fdx2  = t  * (np.sin(x) ** 2 + 2.0 * np.cos(x) ** 2) / (np.sin(x) ** 3)
    d2y     = d2fdx2 * dx1 * dx2
    return y, dy, d2y

t0   = 0.1
tf   = np.sqrt(1.0 + 2.0 * np.cos(np.pi / 2.0) + 0.0 ** 2)
x0   = np.pi / 2.0
dx0  = 1.0
tfd  = 0.0
t0d  = 0.0
x0d  = 1.0
dx0d = 0.0
sol = ivp.djexp(d2f,tf, tfd, t0, t0d, x0, x0d, dx0, dx0d)
print('d^2x/dx0^2(tf, t0, x0) = ', sol.dxfd)
