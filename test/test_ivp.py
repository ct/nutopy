import pytest
import nutopy.ivp as ivp
import nutopy.errors as e
import numpy as np
import numpy.testing as npt

class TestExp:

    def test_exp_t0_is_tf(self):
        def f(t, x):
            y = np.zeros(x.size)
            y[0] = -x[0] + x[1]
            y[1] = x[1]
            return y

        t0 = 0.0
        x0 = np.array([-1.0, 2.0 / (np.exp(2.0) - 1.0)])
        tf = 0.0

        sol = ivp.exp(f, tf, t0, x0)
        npt.assert_array_almost_equal(sol.xf, x0)
        assert sol.status==0
        assert sol.success==True
        assert len(sol.tout)==2

    def test_shape(self):

        # float
        def f(t,x):
            return -x
        sol = ivp.exp(f, 1.0, 0.0, 1.0)
        assert type(sol.xf)==np.float64
        assert type(sol.xout[0])==np.float64
        assert sol.xout.shape==(sol.nsteps,)

        # float + time_steps=[t0, tf]
        sol = ivp.exp(f, 1.0, 0.0, 1.0, time_steps=np.array([0.0, 1.0]))
        assert type(sol.xf)==np.float64
        assert type(sol.xout[0])==np.float64
        assert sol.xout.shape==(2,)

        # array
        def fa(t, x):
            return np.array([-x[0], -x[1]])
        x0 = np.array([1.0, 1.0])
        sol = ivp.exp(fa, 1.0, 0.0, x0)
        assert type(sol.xf)==np.ndarray
        assert type(sol.xout[0])==np.ndarray
        assert sol.xf.shape==x0.shape
        assert sol.xout[0].shape==x0.shape
        assert sol.xout.shape==(sol.nsteps, len(x0))

        # array + time_steps=[t0, tf]
        sol = ivp.exp(fa, 1.0, 0.0, x0, time_steps=np.array([0.0, 1.0]))
        assert type(sol.xf)==np.ndarray
        assert type(sol.xout[0])==np.ndarray
        assert sol.xf.shape==x0.shape
        assert sol.xout[0].shape==x0.shape
        assert sol.xout.shape==(2, len(x0))

    def test_exp_raise_error(self):
        def f(t, x):
            y = np.zeros(x.size)
            y[0] = -x[0] + x[1]
            y[1] = x[1]
            return y

        t0 = 0.0
        x0 = np.array([-1.0, 2.0 / (np.exp(2.0) - 1.0)])
        tf = 1.0

        with pytest.raises(e.ArgumentTypeError) as e_info:
            sol = ivp.exp(f, tf, 'aaa', x0)

        with pytest.raises(e.ArgumentTypeError) as e_info:
            sol = ivp.exp(f, tf, np.array([]), x0)

        with pytest.raises(e.ArgumentTypeError) as e_info:
            sol = ivp.exp(f, 'aaa', t0, x0)

        with pytest.raises(e.ArgumentDimensionError) as e_info:
            sol = ivp.exp(f, tf, t0, np.array([[-1.0, 2.0 / (np.exp(2.0) - 1.0)], [0.0, 0.0]]))

        with pytest.raises(e.ArgumentTypeError) as e_info:
            sol = ivp.exp(f, tf, t0, x0, options=t0)

        with pytest.raises(e.InputArgumentError) as e_info:
            sol = ivp.exp(f, tf, t0, x0, time_steps=np.linspace((tf-t0)/2,tf,10))

        with pytest.raises(e.InputArgumentError) as e_info:
            sol = ivp.exp(f, tf, t0, x0, time_steps=np.linspace(t0,(tf-t0)/2,10))

        with pytest.raises(e.InputArgumentError) as e_info:
            sol = ivp.exp(f, tf, t0, x0, time_steps=t0)

        with pytest.raises(e.InputArgumentError) as e_info:
            sol = ivp.exp(f, tf, t0, x0, time_steps=np.array([t0]))

        def fa(t, x, a):
            y = np.zeros(x.size)
            y[0] = -x[0] + x[1]
            y[1] =  x[1] + a
            return y

        a = 1.0
        with pytest.raises(e.ArgumentDimensionError) as e_info:
            sol = ivp.exp(fa, tf, t0, x0, np.array([[a, 0.0], [0.0, 0.0]]))

    def test_exp_pars(self):
        def f(t, x, a):
            y = np.zeros(x.size)
            y[0] = -x[0] + x[1]
            y[1] =  x[1] + a
            return y

        t0 = 0.0
        x0 = np.array([-1.0, 2.0 / (np.exp(2.0) - 1.0)])
        tf = 1.0
        a  = 1.0

        sol = ivp.exp(f, tf, t0, x0, a)
        npt.assert_array_almost_equal(sol.xf, [x0[1] * np.sinh(tf - t0) + x0[0] * np.exp(-(tf - t0)) + a * (np.cosh(tf-t0) - 1.0),
                                               x0[1] * np.exp(tf - t0) + a * (np.exp(tf-t0) - 1.0)])

    def test_exp_rk_pas_fixe(self):
        def f(t, x, a):
            y = np.zeros(x.size)
            y[0] = -x[0] + x[1]
            y[1] =  x[1] + a
            return y

        t0 = 0.0
        x0 = np.array([-1.0, 2.0 / (np.exp(2.0) - 1.0)])
        tf = 1.0
        a  = 1.0

        opt   = ivp.Options(SolverMethod='gauss4')  #
        tspan = np.linspace(t0, tf, 101)

        sol = ivp.exp(f, tf, t0, x0, args=(a,), options=opt, time_steps=tspan)
        npt.assert_array_almost_equal(sol.xf, [x0[1] * np.sinh(tf - t0) + x0[0] * np.exp(-(tf - t0)) + a * (np.cosh(tf-t0) - 1.0),
                                               x0[1] * np.exp(tf - t0) + a * (np.exp(tf-t0) - 1.0)])
        npt.assert_array_almost_equal(tspan, sol.tout)

    def test_exp_rk_implicit_finite_diff(self):
        def f(t, x, a):
            y = np.zeros(x.size)
            y[0] = -x[0] + x[1]
            y[1] =  x[1] + a
            return y

        t0 = 0.0
        x0 = np.array([-1.0, 2.0 / (np.exp(2.0) - 1.0)])
        tf = 1.0
        a  = 1.0

        opt = ivp.Options(SolverMethod='radau5')  #

        sol = ivp.exp(f, tf, t0, x0, args=a, options=opt)
        npt.assert_array_almost_equal(sol.xf, [x0[1] * np.sinh(tf - t0) + x0[0] * np.exp(-(tf - t0)) + a * (np.cosh(tf-t0) - 1.0),
                                               x0[1] * np.exp(tf - t0) + a * (np.exp(tf-t0) - 1.0)])

    def test_exp_rk_implicit_jac(self):
        def f(t, x, a):
            y = np.zeros(x.size)
            y[0] = -x[0] + x[1]
            y[1] =  x[1] + a
            return y

        def dfdx(t, x, xd, a):
            yd = np.zeros(x.size)
            yd[0] = -xd[0] + xd[1]
            yd[1] =  xd[1]
            return yd

        t0 = 0.0
        x0 = np.array([-1.0, 2.0 / (np.exp(2.0) - 1.0)])
        tf = 1.0
        a  = 0.0

        opt = ivp.Options(SolverMethod='radau5')  #
        tspan = np.linspace(t0, tf, 101) # juste pour le test sur tout=tspan

        sol = ivp.exp(f, tf, t0, x0, args=(a), options=opt, dfdx=dfdx, time_steps=tspan)

        npt.assert_array_almost_equal(sol.xf, [x0[1] * np.sinh(tf - t0) + x0[0] * np.exp(-(tf - t0)) + a * (np.cosh(tf-t0) - 1.0),
                                               x0[1] * np.exp(tf - t0) + a * (np.exp(tf-t0) - 1.0)])
        npt.assert_array_almost_equal(tspan, sol.tout)

    def test_exp_rk_implicit_finite_diff_nonlin(self):
        def f(t, x):
            return t / np.sin(x)

        #
        t0 = 0.1
        tf = np.sqrt(1.0 + 2.0 * np.cos(np.pi / 2.0) + 0.0 ** 2)
        x0 = np.pi / 2.0

        opt = ivp.Options(SolverMethod='radau5')  #

        sol = ivp.exp(f, tf, t0, x0, options=opt)
        npt.assert_almost_equal(sol.xf, np.arccos(np.cos(x0) + 0.5 * (t0 ** 2 - tf ** 2)))

    def test_exp_rk_implicit_jac_nonlin(self):
        def f(t, x):
            return t / np.sin(x)

        def dfdx(t, x, xd):
            return -t * np.cos(x) * xd / (np.sin(x) ** 2)

        #
        t0 = 0.1
        tf = np.sqrt(1.0 + 2.0 * np.cos(np.pi / 2.0) + 0.0 ** 2)
        x0 = np.pi / 2.0

        opt = ivp.Options(SolverMethod='radau5')  #

        sol = ivp.exp(f, tf, t0, x0, options=opt, dfdx=dfdx)
        npt.assert_almost_equal(sol.xf, np.arccos(np.cos(x0) + 0.5 * (t0 ** 2 - tf ** 2)))

# ------------------------------------------------------------------------------------
class TestdExp:

    @classmethod
    def setup_class(cls):
        pass

    def df(t, x, xd):
        y     = np.zeros(x.size)
        y[0]  = -x[0] + x[1]
        y[1]  = x[1]
        yd    = np.zeros(x.size)
        yd[0] = -xd[0] + xd[1]
        yd[1] = xd[1]
        return (y, yd)


    def test_dexp_t0_is_tf(self):
        tf  = 0.0
        t0  = 0.0
        x0  = np.array([-1.0, 2.0 / (np.exp(2.0) - 1.0)])
        x0d = np.array([0.0, 1.0])
        tfd = 0.0
        t0d = 0.0
        sol = ivp.dexp(TestdExp.df, tf, tfd, t0, t0d, x0, x0d)
        npt.assert_array_almost_equal(sol.xf, x0)
        assert sol.status==0
        assert sol.success==True

    def test_dexp_dx_dx0(self):
        tf  = 1.0
        t0  = 0.0
        x0  = np.array([-1.0, 2.0 / (np.exp(2.0) - 1.0)])
        x0d = np.array([0.0, 1.0])
        tfd = 0.0
        t0d = 0.0
        sol = ivp.dexp(TestdExp.df, tf, tfd, t0, t0d, x0, x0d)
        npt.assert_array_almost_equal(sol.xf, [x0[1] * np.sinh(tf - t0) + x0[0] * np.exp(-(tf - t0)),
                                               x0[1] * np.exp(tf - t0)])
        npt.assert_array_almost_equal(sol.xfd, [np.sinh(tf - t0), np.exp(tf - t0)])

    def test_dexp_dx_dt0(self):
        tf  = 1.0
        t0  = 0.0
        x0  = np.array([-1.0, 2.0 / (np.exp(2.0) - 1.0)])
        x0d = np.array([0.0, 0.0])
        tfd = 0.0
        t0d = 1.0
        sol = ivp.dexp(TestdExp.df, tf, tfd, t0, t0d, x0, x0d)
        npt.assert_array_almost_equal(sol.xfd, [-x0[1] * np.cosh(tf - t0) + x0[0] * np.exp(-(tf - t0)),
                                                -x0[1] * np.exp(tf - t0)])

    def test_dexp_dx_dtf(self):
        tf  = 1.0
        t0  = 0.0
        x0  = np.array([-1.0, 2.0 / (np.exp(2.0) - 1.0)])
        x0d = np.array([0.0, 0.0])
        tfd = 1.0
        t0d = 0.0
        sol = ivp.dexp(TestdExp.df, tf, tfd, t0, t0d, x0, x0d)
        np.testing.assert_array_almost_equal(sol.xfd, [x0[1] * np.cosh(tf - t0) - x0[0] * np.exp(-(tf - t0)),
                                                       x0[1] * np.exp(tf - t0)])

    def test_dexp_dx_dpar(self):

        def df(t, x, xd, a, ad):
            dx = x+a
            ddx = xd+ad
            return (dx, ddx)

        tf  = 1.0
        t0  = 0.0
        x0  = 1.0
        a   = 1.0
        tfd = 0.0
        t0d = 0.0
        x0d = 0.0
        ad  = 1.0
        sol = ivp.dexp(df, tf, tfd, t0, t0d, x0, x0d, a, ad)
        np.testing.assert_almost_equal(sol.xfd, np.exp(tf-t0)-1.0)

    def test_dexp_raise_error(self):
        def df(t, x, xd, a, ad):
            dx = x+a
            ddx = xd+ad
            return (dx, ddx)

        tf  = 1.0
        t0  = 0.0
        x0  = 1.0
        a   = 1.0
        tfd = 0.0
        t0d = 0.0
        x0d = 0.0
        ad  = 1.0

        with pytest.raises(e.ArgumentTypeError) as e_info:
            sol = ivp.dexp(df, tf, tfd, 'aaa', t0d, x0, x0d, a, ad)

        with pytest.raises(e.ArgumentTypeError) as e_info:
            sol = ivp.dexp(df, tf, tfd, t0, 'aaa', x0, x0d, a, ad)

        with pytest.raises(e.ArgumentTypeError) as e_info:
            sol = ivp.dexp(df, 'aaa', tfd, t0, t0d, x0, x0d, a, ad)

        with pytest.raises(e.ArgumentTypeError) as e_info:
            sol = ivp.dexp(df, tf, 'aaa', t0, t0d, x0, x0d, a, ad)

        with pytest.raises(e.ArgumentTypeError) as e_info:
            sol = ivp.dexp(df, tf, tfd, np.array([]), t0d, x0, x0d, a, ad)

        with pytest.raises(e.ArgumentTypeError) as e_info:
            sol = ivp.dexp(df, tf, tfd, t0, t0d, x0, x0d, a, ad, options=t0)

        with pytest.raises(e.ArgumentDimensionError) as e_info:
            sol = ivp.dexp(df, tf, tfd, t0, t0d, np.array([[0.0, 0.0], [0.0, 0.0]]), x0d, a, ad)

        with pytest.raises(e.ArgumentDimensionError) as e_info:
            sol = ivp.dexp(df, tf, tfd, t0, t0d, x0, np.array([[0.0, 0.0], [0.0, 0.0]]), a, ad)

        with pytest.raises(e.ArgumentDimensionError) as e_info:
            sol = ivp.dexp(df, tf, tfd, t0, t0d, x0, np.array([0.0, 0.0]), a, ad)

        with pytest.raises(e.ArgumentDimensionError) as e_info:
            sol = ivp.dexp(df, tf, tfd, t0, t0d, x0, x0d, np.array([[0.0, 0.0], [0.0, 0.0]]), ad)

        with pytest.raises(e.ArgumentDimensionError) as e_info:
            sol = ivp.dexp(df, tf, tfd, t0, t0d, x0, x0d, a, np.array([[0.0, 0.0], [0.0, 0.0]]))

        with pytest.raises(e.ArgumentDimensionError) as e_info:
            sol = ivp.dexp(df, tf, tfd, t0, t0d, x0, x0d, a, np.array([0.0, 0.0]))

        with pytest.raises(e.InputArgumentError) as e_info:
            sol = ivp.dexp(df, tf, tfd, t0, t0d, x0, x0d, pars=a)

        with pytest.raises(e.InputArgumentError) as e_info:
            sol = ivp.dexp(df, tf, tfd, t0, t0d, x0, x0d, parsd=ad)

        with pytest.raises(e.InputArgumentError) as e_info:
            sol = ivp.dexp(df, tf, tfd, t0, t0d, x0, x0d, a, ad, time_steps=np.linspace((tf-t0)/2,tf,10))

        with pytest.raises(e.InputArgumentError) as e_info:
            sol = ivp.dexp(df, tf, tfd, t0, t0d, x0, x0d, a, ad, time_steps=np.linspace(t0,(tf-t0)/2,10))

        with pytest.raises(e.InputArgumentError) as e_info:
            sol = ivp.dexp(df, tf, tfd, t0, t0d, x0, x0d, a, ad, time_steps=t0)

        with pytest.raises(e.InputArgumentError) as e_info:
            sol = ivp.dexp(df, tf, tfd, t0, t0d, x0, x0d, a, ad, time_steps=np.array([t0]))

    def test_shape(self):

        t0  = 0.0
        tf  = 1.0
        t0d = 1.0
        tfd = 1.0

        # float
        def df(t, x, xd):
            return (-x, -xd)
        x0  = 1.0
        x0d = 1.0

        sol = ivp.dexp(df, tf, tfd, t0, t0d, x0, x0d)

        assert type(sol.xf)==np.float64
        assert type(sol.xout[0])==np.float64
        assert sol.xout.shape==(sol.nsteps,)

        assert type(sol.xfd)==np.float64
        assert type(sol.xdout[0])==np.float64
        assert sol.xdout.shape==(sol.nsteps,)

        # float + time_steps=[t0, tf]
        sol = ivp.dexp(df, tf, tfd, t0, t0d, x0, x0d, time_steps=np.array([0.0, 1.0]))

        assert type(sol.xf)==np.float64
        assert type(sol.xout[0])==np.float64
        assert sol.xout.shape==(2,)

        assert type(sol.xfd)==np.float64
        assert type(sol.xdout[0])==np.float64
        assert sol.xdout.shape==(2,)

        # array
        def dfa(t, x, xd):
            return (np.array([-x[0], -x[1]]), np.array([-xd[0], -xd[1]]))
        x0  = np.array([1.0, 1.0])
        x0d = np.array([1.0, 1.0])

        sol = ivp.dexp(dfa, tf, tfd, t0, t0d, x0, x0d)

        assert type(sol.xf)==np.ndarray
        assert type(sol.xout[0])==np.ndarray
        assert sol.xf.shape==x0.shape
        assert sol.xout[0].shape==x0.shape
        assert sol.xout.shape==(sol.nsteps, len(x0))

        assert type(sol.xfd)==np.ndarray
        assert type(sol.xdout[0])==np.ndarray
        assert sol.xfd.shape==x0d.shape
        assert sol.xdout[0].shape==x0d.shape
        assert sol.xdout.shape==(sol.nsteps, len(x0d))

        # array + time_steps=[t0, tf]
        sol = ivp.dexp(dfa, tf, tfd, t0, t0d, x0, x0d, time_steps=np.array([0.0, 1.0]))

        assert type(sol.xf)==np.ndarray
        assert type(sol.xout[0])==np.ndarray
        assert sol.xf.shape==x0.shape
        assert sol.xout[0].shape==x0.shape
        assert sol.xout.shape==(2, len(x0))

        assert type(sol.xfd)==np.ndarray
        assert type(sol.xdout[0])==np.ndarray
        assert sol.xfd.shape==x0d.shape
        assert sol.xdout[0].shape==x0d.shape
        assert sol.xdout.shape==(2, len(x0d))

# ------------------------------------------------------------------------------------
class TestJexp:

    @classmethod
    def setup_class(cls):
        pass

    def test_jexp_dx_dx0(self):
        def df(t, x, dx):
            y     = np.zeros(x.size)
            y[0]  = -x[0] + x[1]
            y[1]  = x[1]
            dy    = np.zeros(x.size)
            dy[0] = -dx[0] + dx[1]
            dy[1] = dx[1]
            return (y, dy)

        tf  = 1.0
        t0  = 0.0
        x0  = np.array([-1.0, 2.0 / (np.exp(2.0) - 1.0)])
        dx0 = np.eye(x0.size)
        sol = ivp.jexp(df, tf, t0, x0, dx0)
        np.testing.assert_array_almost_equal(sol.dxf, [[np.exp(-tf), np.sinh(tf)],
                                                       [0.0, np.exp(tf)]])

        #print(sol.dxout[-1])

    def test_jexp_dx_dx0_with_pars(self):
        def df(t, x, dx, a):
            y     = np.zeros(x.size)
            y[0]  = -x[0] + x[1]
            y[1]  = x[1] + a
            dy    = np.zeros(x.size)
            dy[0] = -dx[0] + dx[1]
            dy[1] = dx[1]
            return (y, dy)

        tf  = 1.0
        t0  = 0.0
        x0  = np.array([-1.0, 2.0 / (np.exp(2.0) - 1.0)])
        dx0 = np.eye(x0.size)
        a   = 0.0
        sol = ivp.jexp(df, tf, t0, x0, dx0, a)
        np.testing.assert_array_almost_equal(sol.dxf, [[np.exp(-tf), np.sinh(tf)],
                                                       [0.0, np.exp(tf)]])

    def test_jexp_raise_error(self):
        def df(t, x, xd, a):
            dx = x+a
            ddx = xd
            return (dx, ddx)

        tf  = 1.0
        t0  = 0.0
        x0  = 1.0
        a   = 1.0
        dx0 = 0.0

        with pytest.raises(e.ArgumentTypeError) as e_info:
            sol = ivp.jexp(df, tf, 'aaa', x0, dx0, a)

        with pytest.raises(e.ArgumentTypeError) as e_info:
            sol = ivp.jexp(df, 'aaa', t0, x0, dx0, a)

        with pytest.raises(e.ArgumentTypeError) as e_info:
            sol = ivp.jexp(df, tf, np.array([]), x0, dx0, a)

        with pytest.raises(e.ArgumentTypeError) as e_info:
            sol = ivp.jexp(df, tf, t0, x0, dx0, a, options=t0)

        with pytest.raises(e.ArgumentDimensionError) as e_info:
            sol = ivp.jexp(df, tf, t0, np.array([[0.0, 0.0], [0.0, 0.0]]), dx0, a)

#        with pytest.raises(e.ArgumentDimensionError) as e_info:
#            sol = ivp.jexp(df, tf, t0, x0, np.array([[0.0, 0.0], [0.0, 0.0]]), a)

        with pytest.raises(e.ArgumentDimensionError) as e_info:
            sol = ivp.jexp(df, tf, t0, x0, dx0, np.array([[0.0, 0.0], [0.0, 0.0]]))

        with pytest.raises(e.ArgumentDimensionError) as e_info:
            sol = ivp.jexp(df, tf, t0, x0, np.array([0.0, 0.0]), a)

        with pytest.raises(e.InputArgumentError) as e_info:
            sol = ivp.jexp(df, tf, t0, x0, dx0, a, time_steps=np.linspace((tf-t0)/2,tf,10))

        with pytest.raises(e.InputArgumentError) as e_info:
            sol = ivp.jexp(df, tf, t0, x0, dx0, a, time_steps=np.linspace(t0,(tf-t0)/2,10))

        with pytest.raises(e.InputArgumentError) as e_info:
            sol = ivp.jexp(df, tf, t0, x0, dx0, a, time_steps=t0)

        with pytest.raises(e.InputArgumentError) as e_info:
            sol = ivp.jexp(df, tf, t0, x0, dx0, a, time_steps=np.array([t0]))

    def test_shape(self):

        t0  = 0.0
        tf  = 1.0

        # float
        def df(t, x, dx):
            return (-x, -dx)
        x0  = 1.0
        dx0 = 1.0

        sol = ivp.jexp(df, tf, t0, x0, dx0)

        assert type(sol.xf)==np.float64
        assert type(sol.xout[0])==np.float64
        assert sol.xout.shape==(sol.nsteps,)

        assert type(sol.dxf)==np.float64
        assert type(sol.dxout[0])==np.float64
        assert sol.dxout.shape==(sol.nsteps,)

        # float + time_steps=[t0, tf]
        sol = ivp.jexp(df, tf, t0, x0, dx0, time_steps=np.array([t0, tf]))

        assert type(sol.xf)==np.float64
        assert type(sol.xout[0])==np.float64
        assert sol.xout.shape==(2,)

        assert type(sol.dxf)==np.float64
        assert type(sol.dxout[0])==np.float64
        assert sol.dxout.shape==(2,)

        # array
        def dfa(t, x, dx):
            return (np.array([-x[0], -x[1]]), np.array([-dx[0], -dx[1]]))
        x0  = np.array([1.0, 1.0])
        dx0 = np.array([1.0, 1.0])

        sol = ivp.jexp(dfa, tf, t0, x0, dx0)

        assert type(sol.xf)==np.ndarray
        assert type(sol.xout[0])==np.ndarray
        assert sol.xf.shape==x0.shape
        assert sol.xout[0].shape==x0.shape
        assert sol.xout.shape==(sol.nsteps, len(x0))

        assert type(sol.dxf)==np.ndarray
        assert type(sol.dxout[0])==np.ndarray
        assert sol.dxf.shape==dx0.shape
        assert sol.dxout[0].shape==dx0.shape
        assert sol.dxout.shape==(sol.nsteps, *dx0.shape)

        # array + time_steps=[t0, tf]
        sol = ivp.jexp(dfa, tf, t0, x0, dx0, time_steps=np.array([t0, tf]))

        assert type(sol.xf)==np.ndarray
        assert type(sol.xout[0])==np.ndarray
        assert sol.xf.shape==x0.shape
        assert sol.xout[0].shape==x0.shape
        assert sol.xout.shape==(2, len(x0))

        assert type(sol.dxf)==np.ndarray
        assert type(sol.dxout[0])==np.ndarray
        assert sol.dxf.shape==dx0.shape
        assert sol.dxout[0].shape==dx0.shape
        assert sol.dxout.shape==(2, *dx0.shape)

        # dx0 a matrix
        dx0 = np.eye(x0.size)

        sol = ivp.jexp(dfa, tf, t0, x0, dx0)

        assert type(sol.xf)==np.ndarray
        assert type(sol.xout[0])==np.ndarray
        assert sol.xf.shape==x0.shape
        assert sol.xout[0].shape==x0.shape
        assert sol.xout.shape==(sol.nsteps, len(x0))

        assert type(sol.dxf)==np.ndarray
        assert type(sol.dxout[0])==np.ndarray
        assert sol.dxf.shape==dx0.shape
        assert sol.dxout[0].shape==dx0.shape
        assert sol.dxout.shape==(sol.nsteps, *dx0.shape)

        # dx0 a matrix + time_steps=[t0, tf]
        sol = ivp.jexp(dfa, tf, t0, x0, dx0, time_steps=np.array([t0, tf]))

        assert type(sol.xf)==np.ndarray
        assert type(sol.xout[0])==np.ndarray
        assert sol.xf.shape==x0.shape
        assert sol.xout[0].shape==x0.shape
        assert sol.xout.shape==(2, len(x0))

        assert type(sol.dxf)==np.ndarray
        assert type(sol.dxout[0])==np.ndarray
        assert sol.dxf.shape==dx0.shape
        assert sol.dxout[0].shape==dx0.shape
        assert sol.dxout.shape==(2, *dx0.shape)


# ------------------------------------------------------------------------------------
class TestdJexp:

#    def myd2f(t, x, xd, dx, dxd):
#        y       = t / np.sin(x)
#        dfdx    = - t * np.cos(x) / (np.sin(x) ** 2)
#        yd      = dfdx * xd
#        dy      = dfdx * dx
#        d2fdx2  = t  * (np.sin(x) ** 2 + 2.0 * np.cos(x) ** 2) / (np.sin(x) ** 3)
#        dyd     = d2fdx2 * xd * dx + dfdx * dxd
#        return y, yd, dy, dyd

    def myd2f(t, x, dx1, dx2):
        y       = t / np.sin(x)
        dfdx    = - t * np.cos(x) / (np.sin(x) ** 2)
        dy      = dfdx * dx1
        d2fdx2  = t  * (np.sin(x) ** 2 + 2.0 * np.cos(x) ** 2) / (np.sin(x) ** 3)
        d2y     = d2fdx2 * dx1 * dx2
        return y, dy, d2y

    t0 = 0.1
    tf = np.sqrt(1.0 + 2.0 * np.cos(np.pi / 2.0) + 0.0 ** 2)
    x0 = np.pi / 1.9
    dx0 = 1.0  # we compute the variational equations

    # these depend only on t0, tf and x0
    f = np.cos(x0) + 0.5 * (t0 ** 2 - tf ** 2)
    g = np.sqrt(1.0 - f ** 2)
    dg = f * np.sin(x0) / g

    opt = ivp.Options(SolverMethod='dopri5', TolAbs=1e-14, TolRel=1e-12)  # to increase accuracy

    def test_djexp_dx_dx0_as_jexp(self):
        # similar to jexp
        tfd = 0.0
        t0d = 0.0
        x0d = 0.0
        dx0d = 0.0
        sol = ivp.djexp(TestdJexp.myd2f,
                        TestdJexp.tf, tfd,
                        TestdJexp.t0, t0d,
                        TestdJexp.x0, x0d,
                        TestdJexp.dx0, dx0d, options=TestdJexp.opt)
        npt.assert_almost_equal(sol.xf, np.arccos(TestdJexp.f))  # test on x
        npt.assert_almost_equal(sol.dxf, TestdJexp.dx0 * np.sin(TestdJexp.x0) / TestdJexp.g)  # test on delta_x

    def test_djexp_dy_dx0(self):
        # d2x/dx02 : second order derivative wrt the initial condition
        tfd = 0.0
        t0d = 0.0
        x0d = 1.0
        dx0d = 0.0
        sol = ivp.djexp(TestdJexp.myd2f,
                        TestdJexp.tf, tfd,
                        TestdJexp.t0, t0d,
                        TestdJexp.x0, x0d,
                        TestdJexp.dx0, dx0d, options=TestdJexp.opt)
        npt.assert_almost_equal(sol.dxfd,
                                x0d * TestdJexp.dx0 *
                                (np.cos(TestdJexp.x0) * TestdJexp.g - np.sin(TestdJexp.x0) * TestdJexp.dg) /
                                (TestdJexp.g ** 2))  # test on delta2_x

    def test_djexp_dy_ddx0(self):
        # Variational equations to the variational equations
        tfd = 0.0
        t0d = 0.0
        x0d = 0.0
        dx0d = 1.0
        sol = ivp.djexp(TestdJexp.myd2f,
                        TestdJexp.tf, tfd,
                        TestdJexp.t0, t0d,
                        TestdJexp.x0, x0d,
                        TestdJexp.dx0, dx0d, options=TestdJexp.opt)
        npt.assert_almost_equal(sol.dxfd, dx0d * np.sin(TestdJexp.x0) / TestdJexp.g)

    def test_djexp_dy_dt0(self):
        # dy/dt0
        tfd = 0.0
        t0d = 1.0
        x0d = 0.0
        dx0d = 0.0
        sol = ivp.djexp(TestdJexp.myd2f,
                        TestdJexp.tf, tfd, TestdJexp.t0, t0d, TestdJexp.x0, x0d, TestdJexp.dx0, dx0d,
                        options=TestdJexp.opt)
        npt.assert_almost_equal(sol.dxfd,
                                t0d * TestdJexp.dx0 * np.sin(TestdJexp.x0) * TestdJexp.f *
                                TestdJexp.t0 / (1 - TestdJexp.f ** 2) ** (3.0 / 2.00))

    def test_djexp_dy_dtf(self):
        # dy/dtf
        tfd = 1.0
        t0d = 0.0
        x0d = 0.0
        dx0d = 0.0
        sol = ivp.djexp(TestdJexp.myd2f,
                        TestdJexp.tf, tfd, TestdJexp.t0, t0d, TestdJexp.x0, x0d, TestdJexp.dx0, dx0d, options=TestdJexp.opt)
        npt.assert_almost_equal(sol.dxfd,
                                -tfd * TestdJexp.dx0 * np.sin(TestdJexp.x0) * TestdJexp.f *
                                TestdJexp.tf / (1 - TestdJexp.f ** 2) ** (3.0 / 2.00))



    def test_shape(self):

        t0  = 0.0
        tf  = 1.0
        t0d = 1.0
        tfd = 1.0

        # float
        def d2f(t, x, dx1, dx2):
            return (-x, -dx1, 0.0)
        x0   = 1.0
        x0d  = 1.0
        dx0  = 1.0
        dx0d = 1.0

        sol = ivp.djexp(d2f, tf, tfd, t0, t0d, x0, x0d, dx0, dx0d)

        assert type(sol.xf)==np.float64
        assert type(sol.xout[0])==np.float64
        assert sol.xout.shape==(sol.nsteps,)

        assert type(sol.xfd)==np.float64
        assert type(sol.xdout[0])==np.float64
        assert sol.xdout.shape==(sol.nsteps,)

        assert type(sol.dxf)==np.float64
        assert type(sol.dxout[0])==np.float64
        assert sol.dxout.shape==(sol.nsteps,)

        assert type(sol.dxfd)==np.float64
        assert type(sol.dxdout[0])==np.float64
        assert sol.dxdout.shape==(sol.nsteps,)

        # float + time_steps=[t0, tf]
        sol = ivp.djexp(d2f, tf, tfd, t0, t0d, x0, x0d, dx0, dx0d, time_steps=np.array([t0, tf]))

        assert type(sol.xf)==np.float64
        assert type(sol.xout[0])==np.float64
        assert sol.xout.shape==(2,)

        assert type(sol.xfd)==np.float64
        assert type(sol.xdout[0])==np.float64
        assert sol.xdout.shape==(2,)

        assert type(sol.dxf)==np.float64
        assert type(sol.dxout[0])==np.float64
        assert sol.dxout.shape==(2,)

        assert type(sol.dxfd)==np.float64
        assert type(sol.dxdout[0])==np.float64
        assert sol.dxdout.shape==(2,)

        # array
        def d2fa(t, x, dx1, dx2):
            return (np.array([-x[0], -x[1]]), np.array([-dx1[0], -dx1[1]]), np.zeros(2))

        x0   = np.array([1.0, 1.0])
        x0d  = np.array([1.0, 1.0])
        dx0  = np.array([1.0, 1.0])
        dx0d = np.array([1.0, 1.0])

        sol = ivp.djexp(d2fa, tf, tfd, t0, t0d, x0, x0d, dx0, dx0d)

        assert type(sol.xf)==np.ndarray
        assert type(sol.xout[0])==np.ndarray
        assert sol.xf.shape==x0.shape
        assert sol.xout[0].shape==x0.shape
        assert sol.xout.shape==(sol.nsteps, len(x0))

        assert type(sol.xfd)==np.ndarray
        assert type(sol.xdout[0])==np.ndarray
        assert sol.xfd.shape==x0d.shape
        assert sol.xdout[0].shape==x0d.shape
        assert sol.xdout.shape==(sol.nsteps, len(x0d))

        assert type(sol.dxf)==np.ndarray
        assert type(sol.dxout[0])==np.ndarray
        assert sol.dxf.shape==dx0.shape
        assert sol.dxout[0].shape==dx0.shape
        assert sol.dxout.shape==(sol.nsteps, *dx0.shape)

        assert type(sol.dxfd)==np.ndarray
        assert type(sol.dxdout[0])==np.ndarray
        assert sol.dxfd.shape==dx0d.shape
        assert sol.dxdout[0].shape==dx0d.shape
        assert sol.dxdout.shape==(sol.nsteps, *dx0d.shape)

        # array + time_steps=[t0, tf]
        sol = ivp.djexp(d2fa, tf, tfd, t0, t0d, x0, x0d, dx0, dx0d, time_steps=np.array([t0, tf]))

        assert type(sol.xf)==np.ndarray
        assert type(sol.xout[0])==np.ndarray
        assert sol.xf.shape==x0.shape
        assert sol.xout[0].shape==x0.shape
        assert sol.xout.shape==(2, len(x0))

        assert type(sol.xfd)==np.ndarray
        assert type(sol.xdout[0])==np.ndarray
        assert sol.xfd.shape==x0d.shape
        assert sol.xdout[0].shape==x0d.shape
        assert sol.xdout.shape==(2, len(x0d))

        assert type(sol.dxf)==np.ndarray
        assert type(sol.dxout[0])==np.ndarray
        assert sol.dxf.shape==dx0.shape
        assert sol.dxout[0].shape==dx0.shape
        assert sol.dxout.shape==(2, *dx0.shape)

        assert type(sol.dxfd)==np.ndarray
        assert type(sol.dxdout[0])==np.ndarray
        assert sol.dxfd.shape==dx0d.shape
        assert sol.dxdout[0].shape==dx0d.shape
        assert sol.dxdout.shape==(2, *dx0d.shape)

        # dx0 a matrix
        dx0  = np.eye(x0.size)
        dx0d = np.eye(x0.size)

        sol  = ivp.djexp(d2fa, tf, tfd, t0, t0d, x0, x0d, dx0, dx0d)

        assert type(sol.xf)==np.ndarray
        assert type(sol.xout[0])==np.ndarray
        assert sol.xf.shape==x0.shape
        assert sol.xout[0].shape==x0.shape
        assert sol.xout.shape==(sol.nsteps, len(x0))

        assert type(sol.xfd)==np.ndarray
        assert type(sol.xdout[0])==np.ndarray
        assert sol.xfd.shape==x0d.shape
        assert sol.xdout[0].shape==x0d.shape
        assert sol.xdout.shape==(sol.nsteps, len(x0d))

        assert type(sol.dxf)==np.ndarray
        assert type(sol.dxout[0])==np.ndarray
        assert sol.dxf.shape==dx0.shape
        assert sol.dxout[0].shape==dx0.shape
        assert sol.dxout.shape==(sol.nsteps, *dx0.shape)

        assert type(sol.dxfd)==np.ndarray
        assert type(sol.dxdout[0])==np.ndarray
        assert sol.dxfd.shape==dx0d.shape
        assert sol.dxdout[0].shape==dx0d.shape
        assert sol.dxdout.shape==(sol.nsteps, *dx0d.shape)

        # dx0 a matrix + time_steps=[t0, tf]
        sol = ivp.djexp(d2fa, tf, tfd, t0, t0d, x0, x0d, dx0, dx0d, time_steps=np.array([t0, tf]))

        assert type(sol.xf)==np.ndarray
        assert type(sol.xout[0])==np.ndarray
        assert sol.xf.shape==x0.shape
        assert sol.xout[0].shape==x0.shape
        assert sol.xout.shape==(2, len(x0))

        assert type(sol.xfd)==np.ndarray
        assert type(sol.xdout[0])==np.ndarray
        assert sol.xfd.shape==x0d.shape
        assert sol.xdout[0].shape==x0d.shape
        assert sol.xdout.shape==(2, len(x0d))

        assert type(sol.dxf)==np.ndarray
        assert type(sol.dxout[0])==np.ndarray
        assert sol.dxf.shape==dx0.shape
        assert sol.dxout[0].shape==dx0.shape
        assert sol.dxout.shape==(2, *dx0.shape)

        assert type(sol.dxfd)==np.ndarray
        assert type(sol.dxdout[0])==np.ndarray
        assert sol.dxfd.shape==dx0d.shape
        assert sol.dxdout[0].shape==dx0d.shape
        assert sol.dxdout.shape==(2, *dx0d.shape)

    def test_djexp_raise_error(self):

        def d2f(t, x, dx1, dx2, a, ad):
            return (-x+a, -dx1+ad, 0.0)

        tf   = 1.0
        t0   = 0.0
        x0   = 1.0
        a    = 1.0
        tfd  = 1.0
        t0d  = 1.0
        x0d  = 1.0
        ad   = 1.0
        dx0  = 1.0
        dx0d = 1.0

        with pytest.raises(e.ArgumentTypeError) as e_info:
            sol = ivp.djexp(d2f, tf, tfd, 'aaa', t0d, x0, x0d, dx0, dx0d, a, ad)

        with pytest.raises(e.ArgumentTypeError) as e_info:
            sol = ivp.djexp(d2f, tf, tfd, t0, 'aaa', x0, x0d, dx0, dx0d, a, ad)

        with pytest.raises(e.ArgumentTypeError) as e_info:
            sol = ivp.djexp(d2f, tf, 'aaa', t0, t0d, x0, x0d, dx0, dx0d, a, ad)

        with pytest.raises(e.ArgumentTypeError) as e_info:
            sol = ivp.djexp(d2f, 'aaa', tfd, t0, t0d, x0, x0d, dx0, dx0d, a, ad)

        with pytest.raises(e.ArgumentTypeError) as e_info:
            sol = ivp.djexp(d2f, tf, tfd, np.array([]), t0d, x0, x0d, dx0, dx0d, a, ad)

        with pytest.raises(e.ArgumentTypeError) as e_info:
            sol = ivp.djexp(d2f, tf, tfd, t0, t0d, x0, x0d, dx0, dx0d, a, ad, options=t0)

        with pytest.raises(e.ArgumentDimensionError) as e_info:
            sol = ivp.djexp(d2f, tf, tfd, t0, t0d, np.array([[0.0, 0.0], [0.0, 0.0]]), x0d, dx0, dx0d, a, ad)

        with pytest.raises(e.ArgumentDimensionError) as e_info:
            sol = ivp.djexp(d2f, tf, tfd, t0, t0d, x0, np.array([[0.0, 0.0], [0.0, 0.0]]), dx0, dx0d, a, ad)

        with pytest.raises(e.ArgumentDimensionError) as e_info:
            sol = ivp.djexp(d2f, tf, tfd, t0, t0d, x0, np.array([0.0, 0.0]), dx0, dx0d, a, ad)

        with pytest.raises(e.ArgumentDimensionError) as e_info:
            sol = ivp.djexp(d2f, tf, tfd, t0, t0d, x0, x0d, dx0, dx0d, np.array([[0.0, 0.0], [0.0, 0.0]]), ad)

# dx0 : il n'y a pas tout, cf nombre de lignes different entre dx0 et dx0d
        with pytest.raises(e.ArgumentDimensionError, match=r"Variable dx0 must be either a one or two dimensional numpy array.") as e_info:
            sol = ivp.djexp(d2f, tf, tfd, t0, t0d, x0, x0d, np.array([[[0.0, 0.0], [0.0, 0.0]]]), dx0d, a, ad)

        with pytest.raises(e.ArgumentDimensionError, match=r"Variable dx0d must be either a one or two dimensional numpy array.") as e_info:
            sol = ivp.djexp(d2f, tf, tfd, t0, t0d, x0, x0d, dx0, np.array([[[0.0, 0.0], [0.0, 0.0]]]), a, ad)

        with pytest.raises(e.ArgumentDimensionError, match=r"Variable dx0d must have the same number of columns than x0.") as e_info:
            sol = ivp.djexp(d2f, tf, tfd, t0, t0d, x0, x0d, dx0, np.array([[0.0, 0.0], [0.0, 0.0]]), a, ad)

        with pytest.raises(e.ArgumentDimensionError, match=r"Variable dx0 must have the same number of columns than x0.") as e_info:
            sol = ivp.djexp(d2f, tf, tfd, t0, t0d, x0, x0d, np.array([[0.0, 0.0], [0.0, 0.0]]), dx0d,  a, ad)

# pars
        with pytest.raises(e.ArgumentDimensionError) as e_info:
            sol = ivp.djexp(d2f, tf, tfd, t0, t0d, x0, x0d, dx0, dx0d, a, np.array([[0.0, 0.0], [0.0, 0.0]]))

        with pytest.raises(e.ArgumentDimensionError) as e_info:
            sol = ivp.djexp(d2f, tf, tfd, t0, t0d, x0, x0d, dx0, dx0d, a, np.array([0.0, 0.0]))


        with pytest.raises(e.InputArgumentError) as e_info:
            sol = ivp.djexp(d2f, tf, tfd, t0, t0d, x0, x0d, dx0, dx0d, pars=a)

        with pytest.raises(e.InputArgumentError) as e_info:
            sol = ivp.djexp(d2f, tf, tfd, t0, t0d, x0, x0d, dx0, dx0d, parsd=ad)

        with pytest.raises(e.InputArgumentError) as e_info:
            sol = ivp.djexp(d2f, tf, tfd, t0, t0d, x0, x0d, dx0, dx0d, a, ad, time_steps=np.linspace((tf-t0)/2,tf,10))

        with pytest.raises(e.InputArgumentError) as e_info:
            sol = ivp.djexp(d2f, tf, tfd, t0, t0d, x0, x0d, dx0, dx0d, a, ad, time_steps=np.linspace(t0,(tf-t0)/2,10))

        with pytest.raises(e.InputArgumentError) as e_info:
            sol = ivp.djexp(d2f, tf, tfd, t0, t0d, x0, x0d, dx0, dx0d, a, ad, time_steps=t0)

        with pytest.raises(e.InputArgumentError) as e_info:
            sol = ivp.djexp(d2f, tf, tfd, t0, t0d, x0, x0d, dx0, dx0d, a, ad, time_steps=np.array([t0]))

# ------------------------------------------------------------------------------------
class TestdJexpPars:

#    def myd2f(t, x, xd, dx, dxd, a, ad):
#        y       = a * t / np.sin(x)
#        dfdx    = - a * t * np.cos(x) / (np.sin(x) ** 2)
#        dfda    = t / np.sin(x)
#        yd      = dfdx * xd + dfda * ad
#        dy      = dfdx * dx
#        d2fdx2  = a * t  * (np.sin(x) ** 2 + 2.0 * np.cos(x) ** 2) / (np.sin(x) ** 3)
#        d2fdax  = - t * np.cos(x) / (np.sin(x) ** 2)
#        dyd     = d2fdx2 * xd * dx + d2fdax * ad * dx + dfdx * dxd
#        return y, yd, dy, dyd

    def myd2f(t, x, dx1, dx2, a, ad):
        y       = a * t / np.sin(x)
        dfdx    = - a * t * np.cos(x) / (np.sin(x) ** 2)
        dfda    = t / np.sin(x)
        dy      = dfdx * dx1 + dfda * ad
        d2fdx2  = a * t  * (np.sin(x) ** 2 + 2.0 * np.cos(x) ** 2) / (np.sin(x) ** 3)
        d2fdax  = - t * np.cos(x) / (np.sin(x) ** 2)
        d2y     = d2fdx2 * dx1 * dx2 + d2fdax * ad * dx2
        return y, dy, d2y

    t0  = 0.1
    tf  = np.sqrt(1.0 + 2.0 * np.cos(np.pi / 2.0) + 0.0 ** 2)
    x0  = np.pi / 2.0
    dx0 = 1.0  # we compute the variational equations
    a   = 0.5

    # these depend only on t0, tf and x0
    f = np.cos(x0) + a * 0.5 * (t0 ** 2 - tf ** 2)
    g = np.sqrt(1.0 - f ** 2)
    dg = f * np.sin(x0) / g

    opt = ivp.Options(SolverMethod='dopri5', TolAbs=1e-14, TolRel=1e-12)  # to increase accuracy

    def test_djexp_dx_dx0_as_jexp(self):
        # similar to jexp
        tfd  = 0.0
        t0d  = 0.0
        x0d  = 0.0
        dx0d = 0.0
        ad   = 0.0
        sol  = ivp.djexp(TestdJexpPars.myd2f,
                        TestdJexpPars.tf, tfd,
                        TestdJexpPars.t0, t0d,
                        TestdJexpPars.x0, x0d,
                        TestdJexpPars.dx0, dx0d,
                        TestdJexpPars.a, ad,
                        options=TestdJexpPars.opt)
        npt.assert_almost_equal(sol.xf, np.arccos(TestdJexpPars.f))  # test on x
        npt.assert_almost_equal(sol.dxf, TestdJexpPars.dx0 * np.sin(TestdJexpPars.x0) / TestdJexpPars.g)  # test on delta_x

    def test_djexp_dx_dx0(self):
        # dx/da
        tfd  = 0.0
        t0d  = 0.0
        x0d  = 1.0
        dx0d = 0.0
        ad   = 0.0
        sol  = ivp.djexp(TestdJexpPars.myd2f,
                        TestdJexpPars.tf, tfd,
                        TestdJexpPars.t0, t0d,
                        TestdJexpPars.x0, x0d,
                        TestdJexpPars.dx0, dx0d,
                        TestdJexpPars.a, ad,
                        options=TestdJexpPars.opt)
        npt.assert_almost_equal(sol.xf, np.arccos(TestdJexpPars.f))  # test on x
        npt.assert_almost_equal(sol.xfd, TestdJexpPars.dx0 * np.sin(TestdJexpPars.x0) / TestdJexpPars.g)  # test on delta_x

    def test_djexp_dy_dx0(self):
        # d2x/dx02 : second order derivative wrt the initial condition
        tfd  = 0.0
        t0d  = 0.0
        x0d  = 1.0
        dx0d = 0.0
        ad   = 0.0
        sol  = ivp.djexp(TestdJexpPars.myd2f,
                        TestdJexpPars.tf, tfd,
                        TestdJexpPars.t0, t0d,
                        TestdJexpPars.x0, x0d,
                        TestdJexpPars.dx0, dx0d,
                        TestdJexpPars.a, ad,
                        options=TestdJexpPars.opt)
        npt.assert_almost_equal(sol.dxfd,
                                x0d * TestdJexpPars.dx0 *
                                (np.cos(TestdJexpPars.x0) * TestdJexpPars.g - np.sin(TestdJexpPars.x0) * TestdJexpPars.dg) /
                                (TestdJexpPars.g ** 2))  # test on delta2_x

    def test_djexp_dy_ddx0(self):
        # Variational equations to the variational equations
        tfd  = 0.0
        t0d  = 0.0
        x0d  = 0.0
        dx0d = 1.0
        ad   = 0.0
        sol  = ivp.djexp(TestdJexpPars.myd2f,
                        TestdJexpPars.tf, tfd,
                        TestdJexpPars.t0, t0d,
                        TestdJexpPars.x0, x0d,
                        TestdJexpPars.dx0, dx0d,
                        TestdJexpPars.a, ad,
                        options=TestdJexpPars.opt)
        npt.assert_almost_equal(sol.dxfd, dx0d * np.sin(TestdJexpPars.x0) / TestdJexpPars.g)

    def test_djexp_dy_dt0(self):
        # dy/dt0
        tfd  = 0.0
        t0d  = 1.0
        x0d  = 0.0
        dx0d = 0.0
        ad   = 0.0
        sol  = ivp.djexp(TestdJexpPars.myd2f,
                        TestdJexpPars.tf, tfd,
                        TestdJexpPars.t0, t0d,
                        TestdJexpPars.x0, x0d,
                        TestdJexpPars.dx0, dx0d,
                        TestdJexpPars.a, ad,
                        options=TestdJexpPars.opt)
        npt.assert_almost_equal(sol.dxfd,
                                TestdJexpPars.a * t0d * TestdJexpPars.dx0 * np.sin(TestdJexpPars.x0) * TestdJexpPars.f *
                                TestdJexpPars.t0 / (TestdJexpPars.g ** 3))

    def test_djexp_dy_dtf(self):
        # dy/dtf
        tfd  = 1.0
        t0d  = 0.0
        x0d  = 0.0
        dx0d = 0.0
        ad   = 0.0
        sol  = ivp.djexp(TestdJexpPars.myd2f,
                        TestdJexpPars.tf, tfd,
                        TestdJexpPars.t0, t0d,
                        TestdJexpPars.x0, x0d,
                        TestdJexpPars.dx0, dx0d,
                        TestdJexpPars.a, ad, options=TestdJexpPars.opt)
        npt.assert_almost_equal(sol.dxfd,
                                - TestdJexpPars.a * tfd * TestdJexpPars.dx0 * np.sin(TestdJexpPars.x0) * TestdJexpPars.f *
                                TestdJexpPars.tf / (TestdJexpPars.g ** 3))

    def test_djexp_dx_da(self):
        # dx/da
        tfd  = 0.0
        t0d  = 0.0
        x0d  = 0.0
        dx0d = 0.0
        ad   = 1.0
        sol  = ivp.djexp(TestdJexpPars.myd2f,
                        TestdJexpPars.tf, tfd,
                        TestdJexpPars.t0, t0d,
                        TestdJexpPars.x0, x0d,
                        TestdJexpPars.dx0, dx0d,
                        TestdJexpPars.a, ad,
                        options=TestdJexpPars.opt)
        npt.assert_almost_equal(sol.xf, np.arccos(TestdJexpPars.f))  # test on x
        npt.assert_almost_equal(sol.xfd, -0.5 * (TestdJexpPars.t0**2-TestdJexpPars.tf**2)  / TestdJexpPars.g)

    def test_djexp_dy_da(self):
        # dy/da
        tfd  = 0.0
        t0d  = 0.0
        x0d  = 0.0
        dx0d = 0.0
        ad   = 1.0
        sol  = ivp.djexp(TestdJexpPars.myd2f,
                        TestdJexpPars.tf, tfd,
                        TestdJexpPars.t0, t0d,
                        TestdJexpPars.x0, x0d,
                        TestdJexpPars.dx0, dx0d,
                        TestdJexpPars.a, ad,
                        options=TestdJexpPars.opt)
        npt.assert_almost_equal(sol.dxfd, ad * 0.5 * np.sin(TestdJexpPars.x0) * TestdJexpPars.dx0 * TestdJexpPars.f \
                * (TestdJexpPars.t0**2-TestdJexpPars.tf**2)  / (TestdJexpPars.g ** 3))
