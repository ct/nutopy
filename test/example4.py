import numpy as np
from nutopy import ivp

def df(t, x, dx):
    y     = np.zeros(x.size)
    y[0]  = -x[0] + x[1]
    y[1]  =  x[1]
    dy    = np.zeros(x.size)
    dy[0] = -dx[0] + dx[1]
    dy[1] =  dx[1]
    return (y, dy)

tf = 1.0
t0 = 0.0
x0 = np.array([-1.5, 0.5])
dx0 = np.eye(x0.size)
sol = ivp.jexp(df, tf, t0, x0, dx0)
print('dx/dx0(tf, t0, x0) = ', sol.dxf)

