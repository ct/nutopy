import numpy as np
from nutopy import ivp

def df(t, x, xd):
    y     = np.zeros(x.size)
    y[0]  = -x[0] + x[1]
    y[1]  =  x[1]
    yd    = np.zeros(x.size)
    yd[0] = -xd[0] + xd[1]
    yd[1] =  xd[1]
    return (y, yd)

tf = 1.0
t0 = 0.0
x0 = np.array([-1.5, 0.5])
tfd = 0.0
t0d = 0.0
x0d = np.array([0.0, 1.0])
sol = ivp.dexp(df, tf, tfd, t0, t0d, x0, x0d)
print('dx/dx2(tf, t0, x0) = ', sol.xfd)
