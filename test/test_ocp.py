import numpy as np
import numpy.testing as npt
import pytest

import nutopy.errors as e
from nutopy.tools import *
import nutopy.nle as nle
import nutopy.path as path
from nutopy.ocp import *


class TestSmoothCase:

    def dhfun(t, x, dx, p, dp, l1, dl1, ldum, l2, dl2):
        x1, x2 = x
        p1, p2 = p
        dx1, dx2 = dx
        dp1, dp2 = dp
        hd = dx2*(p1+l1)+x2*(dp1+dl1)+dp2*(p2/2.+l2)+p2*(dp2/2.+dl2)
        return hd

    def d2hfun(t, x, dx, d2x, p, dp, d2p, l1, dl1, d2l1, ldum, l2, dl2, d2l2):
        x1, x2 = x
        p1, p2 = p
        dx1, dx2 = dx
        dp1, dp2 = dp
        d2x1, d2x2 = d2x
        d2p1, d2p2 = d2p
        hdd = dx2*(d2p1+d2l1)+d2x2*(dp1+dl1)+dp2*(d2p2/2.+d2l2)+d2p2*(dp2/2.+dl2)
        return hdd

    def d3hfun(t, x, dx, d2x, d3x, p, dp, d2p, d3p, l1, dl1, d2l1, d3l1, ldum, l2, dl2, d2l2, d3l2):
        x1, x2 = x
        p1, p2 = p
        dx1, dx2 = dx
        dp1, dp2 = dp
        d2x1, d2x2 = d2x
        d2p1, d2p2 = d2p
        d3x1, d3x2 = d3x
        d3p1, d3p2 = d3p
        hddd = 0.0
        return hddd

    @tensorize(dhfun, d2hfun, d3hfun, tvars=(2, 3, 4, 6))
    def hfun(t, x, p, l1, ldum, l2):
        x1, x2 = x
        p1, p2 = p
        h = x2*(p1+l1)+p2*(p2/2.+l2)
        return h

    h    = Hamiltonian(hfun)
    f    = Flow(h)

    t0   = 0.
    x0   = np.array([ -1., 0. ])
    l1   = 0.
    l2   = 0.
    ld   = 0.
    tf   = 1.

    def test_vec(self):
        x    = np.array([ -1., 0.1 ])
        p    = np.array([ 12.1, 6.0 ] )
        t    = 0.1
        l1   = 1.
        l2   = 2.
        ld   = 0.
        print(self.h.vec(t, x, p, l1, ld, l2))
        print(self.h.vec(t, x, p, (l1, 1.), ld, l2))

    def test_flow(self):

        p0   = np.array([ 12.0, 6.0 ] )
        xf, pf = self.f(self.t0, self.x0, p0, self.tf, self.l1, self.ld, self.l2)
        print('xf =', xf) # debug
        print('pf =', pf) # debug
        npt.assert_array_almost_equal(xf, [0., 0.])
        npt.assert_array_almost_equal(pf, [12., -6.])

    def test_shoot(self):

        xf_fixed = np.array([ 0., 0. ]) # target is xf = (0, 0)
        p0 = np.array([ 12.00120012, 6.00060006 ] ) - np.array([ 6., 7. ]) # from BOCOP (+/- some perturbation)

        def dshoot(p0, dp0):
            (xf, dxf), _ = self.f(self.t0, self.x0, (p0, dp0), self.tf, self.l1, self.ld, self.l2)
            s = xf - xf_fixed # code duplication and full=True
            ds = dxf
            return s, ds

        @tensorize(dshoot, full=True)
        def shoot(p0):
            """s = shoot(p0)

            Shooting function associated with flow f"""
            xf, _ = self.f(self.t0, self.x0, p0, self.tf, self.l1, self.ld, self.l2)
            s = xf - xf_fixed
            return s

        sol = nle.solve(shoot, p0, df=shoot); p0_sol = sol.x;

        npt.assert_array_almost_equal(p0_sol, [12., 6.])

    def test_dflow(self):

        # init
        x0   = np.array([ -1., 0.1 ])
        p0   = np.array([ 12.1, 6.0 ] )
        t0   = 0.1
        tf   = 1.2
        l1   = 1.
        l2   = 2.
        ld   = 0.

        a    = x0[0]
        b    = x0[1]
        c    = p0[0]
        d    = p0[1]
        dt   = tf-t0

        # flow
        xf, pf = self.f(t0, x0, p0, tf, l1, ld, l2)
        npt.assert_array_almost_equal(xf, np.array([-(c+l1)*dt**3/6.+(d+l2)*dt**2/2.+b*dt+a, -(c+l1)*dt**2/2.+(d+l2)*dt+b]))
        npt.assert_array_almost_equal(pf, np.array([c, -(c+l1)*dt+d]))

        # dflow
        #
        # dt0
        (xf, dxf), (pf, dpf) = self.f((t0, 1.0), x0, p0, tf, l1, ld, l2)
        npt.assert_array_almost_equal(dxf, np.array([(c+l1)*dt**2/2.-(d+l2)*dt-b, (c+l1)*dt-(d+l2)]))
        npt.assert_array_almost_equal(dpf, np.array([0., (c+l1)]))

        # dtf
        (xf, dxf), (pf, dpf) = self.f(t0, x0, p0, (tf, 1.0), l1, ld, l2)
        npt.assert_array_almost_equal(dxf, np.array([-(c+l1)*dt**2/2.+(d+l2)*dt+b, -(c+l1)*dt+d+l2]))
        npt.assert_array_almost_equal(dpf, np.array([0.0, -(c+l1)]))

        # dx0
        (xf, dxf), (pf, dpf) = self.f(t0, (x0, np.array([1., 0.])), p0, tf, l1, ld, l2)
        npt.assert_array_almost_equal(dxf, np.array([1., 0.]))
        npt.assert_array_almost_equal(dpf, np.array([0., 0.]))

        (xf, dxf), (pf, dpf) = self.f(t0, (x0, np.array([0., 1.])), p0, tf, l1, ld, l2)
        npt.assert_array_almost_equal(dxf, np.array([dt, 1.]))
        npt.assert_array_almost_equal(dpf, np.array([0., 0.]))

        # dp0
        (xf, dxf), (pf, dpf) = self.f(t0, x0, (p0, np.array([1., 0.])), tf, l1, ld, l2)
        npt.assert_array_almost_equal(dxf, np.array([-dt**3/6., -dt**2/2.]))
        npt.assert_array_almost_equal(dpf, np.array([1., -dt]))

        (xf, dxf), (pf, dpf) = self.f(t0, x0, (p0, np.array([0., 1.])), tf, l1, ld, l2)
        npt.assert_array_almost_equal(dxf, np.array([dt**2/2., dt]))
        npt.assert_array_almost_equal(dpf, np.array([0., 1.]))

        # dl1
        (xf, dxf), (pf, dpf) = self.f(t0, x0, p0, tf, (l1, 1.), ld, l2)
        npt.assert_array_almost_equal(dxf, np.array([-dt**3/6., -dt**2/2.]))
        npt.assert_array_almost_equal(dpf, np.array([0., -dt]))

        # dl2
        (xf, dxf), (pf, dpf) = self.f(t0, x0, p0, tf, l1, ld, (l2, 1.))
        npt.assert_array_almost_equal(dxf, np.array([dt**2/2., dt]))
        npt.assert_array_almost_equal(dpf, np.array([0., 0.]))

    def test_d2flow(self):
        # faire une fonction qui depend de x0, puis p0
        # deriver par diff finies puis utiliser d2flow
        # init
        x0   = np.array([ -1., 0.1 ])
        p0   = np.array([ 12.1, 6.0 ] )
        t0   = 0.1
        tf   = 1.2
        l1   = 1.
        l2   = 2.
        ld   = 0.

        # For the moment, there is no order 2 of the flow
        # wrt t0, tf and the parameters
        #
        # Derivatives of the variational euqations wrt initial condition
        #
        def fxx(x):
            (xf, dxf), _ = self.f(t0, (x, np.array([ 1., 1. ])), p0, tf, l1, ld, l2)
            return dxf

        def dfxx(x, dx):
            (xf, dxf, d2xf), _ = self.f(t0, (x, np.array([ 1., 1. ]), dx), p0, tf, l1, ld, l2)
            return d2xf

        #
        def fxp(x):
            _, (pf, dpf) = self.f(t0, (x, np.array([ 1., 1. ])), p0, tf, l1, ld, l2)
            return dpf

        def dfxp(x, dx):
            _, (pf, dpf, d2pf) = self.f(t0, (x, np.array([ 1., 1. ]), dx), p0, tf, l1, ld, l2)
            return d2pf

        #
        def fpx(p):
            (xf, dxf), _ = self.f(t0, x0, (p, np.array([ 1., 1. ])), tf, l1, ld, l2)
            return dxf

        def dfpx(p, dp):
            (xf, dxf, d2xf), _ = self.f(t0, x0, (p, np.array([ 1., 1. ]), dp), tf, l1, ld, l2)
            return d2xf

        #
        def fpp(p):
            _, (pf, dpf) = self.f(t0, x0, (p, np.array([ 1., 1. ])), tf, l1, ld, l2)
            return dpf

        def dfpp(p, dp):
            _, (pf, dpf, d2pf) = self.f(t0, x0, (p, np.array([ 1., 1. ]), dp), tf, l1, ld, l2)
            return d2pf

        # Return f'(x).dx
        def finite_diff(fun, x, dx, *args, **kwargs):
            v_eps = np.finfo(float).eps
            t = np.sqrt(v_eps) * np.sqrt(np.maximum(1.0, np.linalg.norm(x))) / np.sqrt(np.maximum(1.0, np.linalg.norm(dx)))
            j = (fun(x + t*dx, *args, **kwargs) - fun(x, *args, **kwargs)) / t
            return j

        dx = np.array([ 1., 0. ])
        npt.assert_array_almost_equal(finite_diff(fxx, x0, dx), dfxx(x0, dx))
        npt.assert_array_almost_equal(finite_diff(fxp, x0, dx), dfxp(x0, dx))
        dp = np.array([ 1., 0. ])
        npt.assert_array_almost_equal(finite_diff(fpx, p0, dp), dfpx(p0, dp))
        npt.assert_array_almost_equal(finite_diff(fpp, p0, dp), dfpp(p0, dp))

        # derivative of the variational equations wrt to initial increment
        def fdx(dx):
            (xf, dxf), _ = self.f(t0, (x0, dx), p0, tf, l1, ld, l2)
            return dxf

        def dfdx(dx, d2x):
            (xf, dxf), _ = self.f(t0, (x0, d2x), p0, tf, l1, ld, l2)
            return dxf

        dx  = np.array([ 1., 1. ])
        d2x = np.array([ 1., 1. ])
        npt.assert_array_almost_equal(finite_diff(fdx, dx, d2x), dfdx(dx, d2x))

    def test_homotopy(self):

        xf_fixed = np.array([ 0., 0. ]) # target is xf = (0, 0)
        p0 = np.array([ 12.00120012, 6.00060006 ] )

        def dshoot(p0, dp0, l1, dl1):
            (xf, dxf), _ = self.f(self.t0, self.x0, (p0, dp0), self.tf, (l1, dl1), self.ld, self.l2)
            s = xf - xf_fixed # code duplication and full=True
            ds = dxf
            return s, ds

        @tensorize(dshoot, tvars=(1,2), full=True)
        def shoot(p0, l1):
            """s = shoot(p0)

            Shooting function associated with flow f"""
            xf, _ = self.f(self.t0, self.x0, p0, self.tf, l1, self.ld, self.l2)
            s = xf - xf_fixed
            return s

        l1_i = 1.
        fun  = lambda p0 : shoot(p0, l1_i)
        sol  = nle.solve(fun, p0, df=fun); p0_sol = sol.x;

        l1_f = 0.
        sol  = path.solve(shoot, p0_sol, l1_i, l1_f, df=shoot); p0_sol = sol.xf

        npt.assert_array_almost_equal(p0_sol, [12., 6.])

    def test_homotopy_lambda_function(self):

        def dhfun(t, x, dx, p, dp, l1, dl1, ldum, l2, dl2):
            x1, x2 = x
            p1, p2 = p
            dx1, dx2 = dx
            dp1, dp2 = dp
            hd = dx2*(p1+l1)+x2*(dp1+dl1)+dp2*(p2/2.+l2)+p2*(dp2/2.+dl2)
            return hd

        def d2hfun(t, x, dx, d2x, p, dp, d2p, l1, dl1, d2l1, ldum, l2, dl2, d2l2):
            x1, x2 = x
            p1, p2 = p
            dx1, dx2 = dx
            dp1, dp2 = dp
            d2x1, d2x2 = d2x
            d2p1, d2p2 = d2p
            hdd = dx2*(d2p1+d2l1)+d2x2*(dp1+dl1)+dp2*(d2p2/2.+d2l2)+d2p2*(dp2/2.+dl2)
            return hdd

        @tensorize(dhfun, d2hfun, tvars=(2, 3, 4, 6))
        def hfun(t, x, p, l1, ldum, l2):
            x1, x2 = x
            p1, p2 = p
            h = x2*(p1+l1)+p2*(p2/2.+l2)
            return h

        with pytest.raises(e.InputArgumentError) as e_info:
            h   = Hamiltonian(lambda t, x, p, l1, ldum, l2 : hfun(t, x, p, l1, ldum, l2))

        hfun_lambda = lambda t, x, p, l1, ldum, l2 : hfun(t, x, p, l1, ldum, l2)
        h    = Hamiltonian(hfun_lambda, tvars=(2, 3, 4, 6))
        f    = Flow(h)

        xf_fixed = np.array([ 0., 0. ]) # target is xf = (0, 0)

        def dshoot(p0, dp0, l1, dl1):
            (xf, dxf), _ = f(self.t0, self.x0, (p0, dp0), self.tf, (l1, dl1), self.ld, self.l2)
            s = xf - xf_fixed # code duplication and full=True
            ds = dxf
            return s, ds

        @tensorize(dshoot, tvars=(1,2), full=True)
        def shoot(p0, l1):
            """s = shoot(p0)

            Shooting function associated with flow f"""
            xf, _ = f(self.t0, self.x0, p0, self.tf, l1, self.ld, self.l2)
            s = xf - xf_fixed
            return s

        p0   = np.array([ 12.00120012, 6.00060006 ] )
        l1_i = 1.
        fun  = lambda p0 : shoot(p0, l1_i)
        sol  = nle.solve(fun, p0, df=fun); p0_sol = sol.x;

        l1_f = 0.
        sol  = path.solve(shoot, p0_sol, l1_i, l1_f, df=shoot); p0_sol = sol.xf

        npt.assert_array_almost_equal(p0_sol, [12., 6.])

