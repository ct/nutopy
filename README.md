# Welcome to nutopy package

You need first [conda](https://docs.conda.io>) to install `nutopy` package.
You can install `conda` as part of a [Miniforge](https://github.com/conda-forge/miniforge>) installer.

```bash
conda install -c control-toolbox -c conda-forge nutopy
```

Please visite the [documentation](https://ct.gitlabpages.inria.fr/nutopy/index.html) for details about `nutopy api`.
