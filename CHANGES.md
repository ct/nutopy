# Changelog

## version 0.4.2 - 2022-11-03
- version with python 3.11 on windows

## version 0.4.1 - 2022-11-03
- compiled with python 3.11

## version 0.4.0 - 2021-06-06
- changed names expd, dexpd into jexp, djexp
- add tests on flow
- add derivative of the flow wrt parameters
- add second order derivative of the flow (missing order 2 for t0, tf and parameters: need d2exp)

## version 0.3.5 - 2020-09-25
- change MaxSteps in ivp from 10000 to 100000
- remove versions in conda recipe file

## version 0.3.4 - 2020-09-24
- fix windows recipe
- update conda recipe

## version 0.3.2 - 2020-08-22
- fix float case for nle module

## version 0.3.2 - 2020-08-22
- add "from .tools import *" and "from .ocp import *" in __init__.py file to get access of this modules.

## version 0.3.1 - 2020-08-13
- update .gitlab-ci.yml
- bug correction in Flow
- update Hamiltonian
- add references in documentation
- fix bug: MaxSteps integer instead of float
- examples removed and put in gallery
- update tools.py

## version 0.3.0 - 2020-05-19
- creation of examples: smooth_case, kepler, sir
- set up sphinx documentation
- use of pytest for the tests
- single CMake at root does all the work of building, installing and testing
- update print via callbacks
- add derivative wrt pars in ivp module
- add pytest.ini for creating xUnit2 test reports
- add fortran module: mod_expd, mod_dexpd
- add defensive tests in ivp
- add ocp.py module and test_ocp.py
- rename calculus/solvers-ct in nutopy

## version 0.2.1 - 2020-03-20
- remove openblas 0.3.8 package from the build machine
- add cast float and numpy array in nle, path and ivp packages
- changed option class attribut to instance attribut in Options class from nle, path and ivp packages
- add callbacks to nle.solve and path.solve functions
- update message output in nle, path and ivp packages
- update doc and tests
- add jacobian rhs for implicit RK schemes
- add possibility to do a homotopy on more than 1 parameter

## version 0.1.0 - 2020-03-13
- initial release; includes nlesolver, pathsolver and ivpsolver
