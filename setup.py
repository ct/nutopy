def configuration(parent_package='', top_path=None):
    from numpy.distutils.misc_util import Configuration
    config = Configuration('', parent_package, top_path)

    config.add_subpackage(subpackage_name='nutopy', subpackage_path='src/nutopy')
    #config.add_subpackage(subpackage_name='nle', subpackage_path='src/nutopy')
    #config.add_subpackage(subpackage_name='ivp', subpackage_path='src/nutopy')
    #config.add_subpackage(subpackage_name='path', subpackage_path='src/nutopy')

    return config


if __name__ == '__main__':
    from numpy.distutils.core import setup

#    setup(configuration=configuration, requires=['numpy', 'scipy'])
    setup(configuration=configuration, requires=['numpy', 'scipy'], version='0.4.0', name='nutopy')
