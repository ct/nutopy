module mod_djexp

    use mod_ivp_options
    use mod_gest_liste
    use mod_integration

    implicit none

!! -------------------------------------------------------------------------------------------
    type(TLISTE), save :: states_out_mod

!! -------------------------------------------------------------------------------------------
    procedure(), private, pointer :: drhsd_mod

!! -------------------------------------------------------------------------------------------
    type(TLISTE), save      :: time_grid_mod

!! -------------------------------------------------------------------------------------------
    character(32),  private :: integ_method_mod       = "nodef"

contains

    ! ------------------------------------------------------------------------
    ! ------------------------------------------------------------------------
    Subroutine djexpfun(drhsd,n,k,x0,x0d,dx0,dx0d,npars,pars,parsd,nt,tspan,options,ninfos,infos,dimeT,message)
        implicit none
        integer,            intent(in)                      :: n,ninfos, nt, k
        integer,            intent(in)                      :: npars
        double precision,   intent(in)                      :: tspan(nt)
        double precision,   intent(in), dimension(n)        :: x0
        double precision,   intent(in), dimension(n)        :: x0d
        double precision,   intent(in), dimension(n,k)      :: dx0
        double precision,   intent(in), dimension(n,k)      :: dx0d
        double precision,   intent(in), dimension(npars)    :: pars, parsd
        type(ivp_options),  intent(in)                      :: options
        integer,            intent(out)                     :: infos(ninfos)
        integer,            intent(out)                     :: dimeT
        character(len=120), intent(out)                     :: message

        external drhsd

        !local variables
        integer                                     :: flag, icur
        integer                                     :: neq, nparaux
        double precision                            :: t0, tf
        double precision, dimension(2*npars+2)      :: paraux
        double precision, dimension(n*k,1)          :: aux
        double precision, dimension(2*n*(k+1))      :: z0, zf

        ! -------------------------
        ! OPTIONS AND PARAMETERS INIT
        ! -------------------------
        t0 = tspan(1)
        tf = tspan(nt)
        call initStatesGrid ! creer la liste pour sauver la trajectoire
        call initTimeGrid(nt,tspan)
        call initIntegrationMethod(options)
        ! -------------------------
        ! END OPTIONS AND PARAMETERS INIT
        ! -------------------------

        ! -------------------------
        ! SET FUNCTIONS
        ! -------------------------
        call set_dynamics(drhsd) !
        ! -------------------------
        ! END SET FUNCTIONS
        ! -------------------------

        ! -------------------------
        ! CALL TO ODEEXP
        ! -------------------------
        !
        ! z = (x, dx, xd, dxd) : on met dans ce sens pour le controle du pas sur n+n*k
        !
        !
        neq                 = 2*n*(k+1)
        icur                = 1
        z0                  = 0d0
        z0(icur:icur+n-1)   = x0;                       icur = icur + n
        aux                 = reshape(dx0,(/n*k,1/))
        z0(icur:icur+n*k-1) = aux(:,1);                 icur = icur + n*k
        z0(icur:icur+n-1)   = x0d;                      icur = icur + n
        aux                 = reshape(dx0d,(/n*k,1/))
        z0(icur:icur+n*k-1) = aux(:,1);                 icur = icur + n*k

        nparaux         = 2*npars+2
        paraux(npars+1) = dble(n)
        paraux(npars+2) = dble(k)
        if(npars.gt.0)then
            paraux(1:npars) = pars
            paraux(npars+3:nparaux) = parsd
        end if

        call odedjexp(options,neq,z0,paraux,nparaux,n+n*k,t0,tf,zf,ninfos,infos)

        ! -------------------------
        ! POST-CALL TO ODEEXP
        ! -------------------------
        dimeT = LONGUEUR(states_out_mod)

        !
        flag = infos(1)
        select case(flag)
            case(1)
                message = 'Integration successfully completed.'
            case(-1)
                message = 'Inputs are not consistent.'
            case(-2)
                message = 'Maximum number of steps (MaxSteps) reached.'
            case(-3)
                message = 'Step-size (MaxStepSize) is too small.'
            case(-4)
                message = 'The associated differential system is probably stiff.'
            case(-8)
                message = 'The maximum number of iterations to solve the NLE in the IRK method has been reached.'
            case default
                message = ''
        end select

    end subroutine djexpfun

    ! ------------------------------------------------------------------------
    ! ------------------------------------------------------------------------
    subroutine initStatesGrid()
        call CREER_STRUCTURE(states_out_mod)
    end subroutine initStatesGrid

    ! ------------------------------------------------------------------------
    ! ------------------------------------------------------------------------
    subroutine initTimeGrid(n,tspan)
        implicit none
        integer, intent(in)                                 :: n
        double precision, intent(in), dimension(n)          :: tspan

        !local variables
        integer                    :: i
        double precision, dimension(1) :: val

        !tspan must be contained in ti and tspan must be sorted
        i=2
        do while(i.lt.n)
            if((tspan(i)-tspan(i-1))*(tspan(i+1)-tspan(i)).lt.0d0)then
                CALL printandstop('tspan must be sorted')
            end if
            i = i + 1
        end do

        !we add each instant from tspan in the list time_grid_mod
        val(1)=0 ! dummy
        call CREER_STRUCTURE(time_grid_mod,n-3)
        do i=1,n
            call INSERER(tspan(n-i+1),1,val,time_grid_mod)
        end do

    end subroutine initTimeGrid

    ! ------------------------------------------------------------------------
    ! ------------------------------------------------------------------------
    subroutine initIntegrationMethod(options)
        implicit none
        type(ivp_options), intent(in) :: options
        call getIVPOption(options,'ODESolver',integ_method_mod)
    end subroutine initIntegrationMethod

    ! ------------------------------------------------------------------------
    ! ------------------------------------------------------------------------
    subroutine set_dynamics(drhsd)
        external drhsd
        drhsd_mod => drhsd
    end subroutine set_dynamics

    ! ------------------------------------------------------------------------
    ! ------------------------------------------------------------------------
    Subroutine getdstatesd(n,k,dimeT,Ts,xs,xds,dxs,dxds)
        implicit none
        integer,                                intent(in)  :: n
        integer,                                intent(in)  :: k
        integer,                                intent(in)  :: dimeT
        double precision, dimension(dimeT),     intent(out) :: Ts
        double precision, dimension(n,dimeT),   intent(out) :: xs
        double precision, dimension(n,dimeT),   intent(out) :: xds
        double precision, dimension(n,dimeT*k), intent(out) :: dxs
        double precision, dimension(n,dimeT*k), intent(out) :: dxds

        !local variables
        integer                                 :: i, icur
        double precision,dimension(n)           :: x, xd
        double precision,dimension(n,k)         :: dx, dxd
        double precision                        :: time
        double precision, dimension(2*n*(k+1))  :: val

        xs   = 0d0
        xds  = 0d0
        dxs  = 0d0
        dxds = 0d0
        do i=1,dimeT
            call EXTRAIRE(time,2*n*(k+1),val,states_out_mod)
            Ts(dimeT-i+1)                       = time
            icur                                = 1
            x                                   = val(icur:icur+n-1);                    icur = icur + n
            dx                                  = reshape(val(icur:icur+n*k-1),(/n,k/)); icur = icur + n*k
            xd                                  = val(icur:icur+n-1);                    icur = icur + n
            dxd                                 = reshape(val(icur:icur+n*k-1),(/n,k/)); icur = icur + n*k
            xs(:,dimeT-i+1)                     = x
            xds(:,dimeT-i+1)                    = xd
            dxs(:,k*(dimeT-i)+1:k*(dimeT-i+1))  = dx
            dxds(:,k*(dimeT-i)+1:k*(dimeT-i+1)) = dxd
        end do

        call DETRUIRE(states_out_mod)
        call DETRUIRE(time_grid_mod)

    end subroutine getdstatesd

    ! ------------------------------------------------------------------------
    ! ------------------------------------------------------------------------
    Subroutine odedjexp(options,neq,y0,funpar,lfunpar,ncpas,tin,tout,yf,ninfos,infos)
        implicit none
        type(ivp_options),  intent(in)                      :: options
        integer,            intent(in)                      :: ninfos, lfunpar
        integer,            intent(in)                      :: neq
        integer,            intent(in)                      :: ncpas
        double precision,   intent(in),  dimension(neq)     :: y0
        double precision,   intent(in),  dimension(lfunpar) :: funpar
        double precision,   intent(inout)                   :: tin
        double precision,   intent(inout)                   :: tout
        double precision,   intent(out), dimension(neq)     :: yf
        integer,            intent(out)                     :: infos(ninfos)

        !local
        type(integration_parameters)    :: spec

        !
        character(32)                   :: ODESolver
        integer                         :: MaxSteps
        double precision                :: TolAbs, TolRel, MaxStepSize

        ! Defintion de l'ivp
        spec%fun           => jexprhsd
        spec%dfun          => djexprhsd
        spec%phidez        => phidezdjexp
        spec%pdpdez        => pdpdjexp
        spec%solout        => soloutdjexp

        ! Options d'integration
        call setIntegrationParameter(spec, 'irki',              '2')
        call setIntegrationParameter(spec, 'irks',              'pfixe') ! on fait du point fixe car on ne fournit pas la jacobienne
        call setIntegrationParameter(spec, 'ParentMethodName',  'djexp')
        call setIntegrationParameter(spec, 'IntegrationList',   time_grid_mod)

        call setIntegrationParameter(spec, 'Display',           .false.)
        call setIntegrationParameter(spec, 'UseSolout',         .true.)
        call setIntegrationParameter(spec, 'UseRadauHampath',   .false.)

        call getIVPOption(options, 'MaxStepsOde',      MaxSteps   ); call setIntegrationParameter(spec, 'MaxSteps',    MaxSteps)
        call getIVPOption(options, 'TolOdeAbs',        TolAbs     ); call setIntegrationParameter(spec, 'TolAbs',      TolAbs)
        call getIVPOption(options, 'TolOdeRel',        TolRel     ); call setIntegrationParameter(spec, 'TolRel',      TolRel)
        call getIVPOption(options, 'MaxStepSizeOde',   MaxStepSize); call setIntegrationParameter(spec, 'MaxStepSize', MaxStepSize)
        call getIVPOption(options, 'ODESolver',        ODESolver  ); call setIntegrationParameter(spec, 'ODESolver',   ODESolver)

        ! Integration
        call integration(spec,neq,y0,funpar,lfunpar,ncpas,tin,tout,yf,ninfos,infos)

    end subroutine odedjexp

    ! ------------------------------------------------------------------------
    ! ------------------------------------------------------------------------
    Subroutine jexprhsd(neq,t,z,dz,paraux,nparaux)
        implicit none
        integer,      intent(in)                          :: neq
        integer,      intent(in)                          :: nparaux
        double precision, intent(in)                      :: t
        double precision, intent(in),  dimension(neq)     :: z
        double precision, intent(in),  dimension(nparaux) :: paraux
        double precision, intent(out), dimension(neq)     :: dz

        !local variables
        integer             :: npars, n, k, i, icur, nk
        double precision    :: pars((nparaux-2)/2), parsd((nparaux-2)/2)
        double precision, dimension(:), allocatable :: x, xd, dx, dxd, y, yd, dy, dyd

        npars   = (nparaux - 2)/2
        n       = nint(paraux(npars+1))
        k       = nint(paraux(npars+2))
        if(npars.gt.0)then
            pars    = paraux(1:npars)
            parsd   = paraux(npars+3:nparaux)
        end if

        allocate(x(n), xd(n), dx(n), dxd(n), y(n), yd(n), dy(n), dyd(n))

        !  z = (x(n,1), xd(n,1), dx(n,k), dxd(n,k))
        ! dz = (y, yd, dy, dyd)
        nk   = n*k

        x    = z(1:n)
        xd   = z(n*(k+1)+1:n*(k+1)+n)

        !
        icur = n+1
        do i=1,k

            dx   = z(icur:icur+n-1);
            dxd  = z(icur+nk+n:icur+nk+n+n-1);

            call drhsd_mod(t, n, x, xd, dx, dxd, npars, pars, parsd, y, yd, dy, dyd)

            if(i.eq.1)then
                dz(1:n)                 = y
                dz(n*(k+1)+1:n*(k+1)+n) = yd
            end if

            dz(icur:icur+n-1)           = dy
            dz(icur+nk+n:icur+nk+n+n-1) = dyd

            !
            icur = icur + n;

        end do

        deallocate(x, xd, dx, dxd, y, yd, dy, dyd)

    end subroutine jexprhsd

    ! ------------------------------------------------------------------------
    ! ------------------------------------------------------------------------
    Subroutine djexprhsd(neq,t,y,val,paraux,lparaux)
        implicit none
        integer,            intent(in)                      :: neq
        integer,            intent(in)                      :: lparaux
        double precision,   intent(in)                      :: t
        double precision,   intent(in),  dimension(lparaux) :: paraux
        double precision,   intent(in),  dimension(neq)     :: y
        double precision,   intent(out), dimension(neq,neq) :: val

        !local variables
        integer :: j
        double precision :: eps, fvec1(neq), fvec2(neq), ytemp(neq), h

        eps  = epsilon(1d0)

        ytemp = y
        call jexprhsd(neq,t,ytemp,fvec2,paraux,lparaux)

        do j=1,neq
            h = dsqrt(eps*max(1.d-5,abs(y(j))))
            ytemp(j) = y(j) + h
            call jexprhsd(neq,t,ytemp,fvec1,paraux,lparaux)
            ytemp(j) = y(j)
            val(:,j) = (fvec1 - fvec2)/h
        end do

    end subroutine djexprhsd

    ! ------------------------------------------------------------------------
    ! ------------------------------------------------------------------------
    subroutine phidezdjexp(nz, Zc, fvec, iflag, fpar, lfpar)
        implicit none
        integer,            intent(in)                          :: nz, lfpar
        double precision,   intent(in),  dimension(nz)          :: Zc
        double precision,   intent(out), dimension(nz)          :: fvec
        integer,            intent(inout)                       :: iflag
        double precision,   intent(inout),  dimension(lfpar)    :: fpar

!        external exphvrhs

        call phidez(nz, Zc, fvec, iflag, fpar, lfpar, jexprhsd)

    end subroutine phidezdjexp

    ! ------------------------------------------------------------------------
    ! ------------------------------------------------------------------------
    subroutine pdpdjexp(nz,Zc,fpar,lfpar,fvec,fjac,ldfjac,iflag)
        implicit none
        integer,          intent(in)                        :: nz
        integer,          intent(in)                        :: lfpar
        integer,          intent(in)                        :: ldfjac
        double precision, intent(inout), dimension(lfpar)   :: fpar
        double precision, intent(in), dimension(nz)         :: Zc
        double precision, intent(out), dimension(nz)        :: fvec
        double precision, intent(out), dimension(ldfjac,nz) :: fjac
        integer,          intent(inout)                     :: iflag

!        external exphvrhs
!        external dexphvrhs

        call phidphidez(nz,Zc,fpar,lfpar,fvec,fjac,ldfjac,iflag,jexprhsd,djexprhsd)

    end subroutine pdpdjexp

    ! ------------------------------------------------------------------------
    ! ------------------------------------------------------------------------
    Subroutine soloutdjexp(Niter,told,time,y,neq,dpar,ldpar,ipar,lipar,fun,funpar,lfunpar,irtrn)
        implicit none
        integer,            intent(in)                         :: Niter
        double precision,   intent(in)                         :: told
        double precision,   intent(inout)                      :: time
        integer,            intent(in)                         :: neq
        integer,            intent(in)                         :: ldpar
        integer,            intent(in)                         :: lipar
        integer,            intent(in)                         :: lfunpar
        double precision,   intent(inout), dimension(neq)      :: y
        double precision,   intent(in),    dimension(ldpar)    :: dpar
        integer,            intent(in),    dimension(lipar)    :: ipar
        double precision,   intent(in),    dimension(lfunpar)  :: funpar
        integer,            intent(inout)                      :: irtrn

        interface
        Subroutine fun(neq,time,y,val,funpar,lfunpar)
            implicit none
            integer,           intent(in)                      :: neq
            integer,           intent(in)                      :: lfunpar
            double precision,  intent(in)                      :: time
            double precision,  intent(in),  dimension(lfunpar) :: funpar
            double precision,  intent(in),  dimension(neq)     :: y
            double precision,  intent(out), dimension(neq)     :: val
        end Subroutine fun
        end interface

        !local variables
        character(32) :: nameinte

        nameinte    = integ_method_mod

        call addstatesgeneral(addstatesdjexp,neq,time,y,told,lipar,ipar,ldpar,dpar,nameinte)

    end subroutine soloutdjexp

    ! ------------------------------------------------------------------------
    ! ------------------------------------------------------------------------
    Subroutine addstatesgeneral(addstates,neq,time,y,told,lipar,ipar,ldpar,dpar,nameinte)
        implicit none
        integer,            intent(in)                      :: neq
        double precision,   intent(in)                      :: time
        double precision,   intent(in), dimension(neq)      :: y
        double precision,   intent(in)                      :: told
        integer,            intent(in)                      :: ldpar
        double precision,   intent(in), dimension(ldpar)    :: dpar
        integer,            intent(in)                      :: lipar
        integer,            intent(in), dimension(lipar)    :: ipar
        character(32),      intent(in)                      :: nameinte

        interface
        Subroutine addstates(nz,time,z)
            implicit none
            integer, intent(in)                             :: nz
            double precision, intent(in)                    :: time
            double precision, intent(in), dimension(nz)     :: z
        end Subroutine addstates
        end interface

        !local variables
        integer                          :: i
        integer                          :: fini
        double precision, dimension(1)   :: val
        double precision, dimension(neq) :: yg
        double precision                 :: t,hstep
        double precision :: eps
        eps  = 10d0*epsilon(1d0)

        select case (nameinte)

        case ('dopri5','radau5old','dop853','radau5','radau9','radau13','radau','radause')

                if(GET_PARAM(time_grid_mod).EQ.-1)then !if tspan given by the user is [t0 tf]
                    call addstates(neq,time,y)
                else                            !if tspan given by the user is [t0 t1 .. tf]
                !we have to find all ti from tspan (ie time_grid_mod) contained in [told time]
                !and use dense output to get the value
                t     = GET_TIME(time_grid_mod)
                fini  = 0
                hstep = time - told
                    do while(((told-t)*(time-t).LE.0).AND.(fini.EQ.0))
                        call EXTRAIRE(t,1,val,time_grid_mod)
                        if(abs(time-t).gt.eps)then
                            do i=1,neq
                                call denseOutput(nameinte,i,t,yg(i),told,time,hstep,lipar,ipar,ldpar,dpar)
                            end do
                        else
                            yg=y
                        end if
                        call addstates(neq,t,yg)
                        if(LONGUEUR(time_grid_mod).EQ.0)then
                            fini=1
                        else
                            t = GET_TIME(time_grid_mod)
                        end if
                    end do
                end if

            case default

                call addstates(neq,time,y)

        end select

    end subroutine addstatesgeneral

    ! ------------------------------------------------------------------------
    ! ------------------------------------------------------------------------
    Subroutine addstatesdjexp(nz,time,z)
        implicit none

        integer, intent(in)                         :: nz
        double precision, intent(in)                :: time
        double precision, intent(in), dimension(nz) :: z

        !local variables
        !double precision :: eps
        !eps  = 10d0*epsilon(1d0)

        !if(LONGUEUR(states_out_mod).GE.1)then
            !if(abs(time-GET_TIME(states_out_mod)).gt.eps)then
                !call INSERER(time,nz,z,states_out_mod)
            !end if
        !else
            call INSERER(time,nz,z,states_out_mod)
        !end if

    end subroutine addstatesdjexp

end module mod_djexp

