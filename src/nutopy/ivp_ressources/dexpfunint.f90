!! -----------------------------------------------------------------------------
!!
!>     @ingroup ivpsolvePackage
!!     @brief  Interface of expfun from module mod_exp
!!
!!        @param[in]    n         State dimension
!!        @param[in]    z0        initial flow
!!        @param[in]    npars      Number of optional parameters
!!        @param[in]    par       Optional parameters
!!        @param[in]    nt        Dimension of tspan
!!        @param[in]    tspan     Grid time : [t0 t1 ... tf]
!!        @param[in]    nbarc     Number of arcs
!!        @param[in]    ti        Contains t0, tf and the intermediate times if any. 
!!                                ti = [t0 t1 .. t_{nbarc-1} tf]
!!        \param[in]    ninfos      dimension of infos
!!        \param[in]    ndw       Size of dw
!!        \param[in]    niw       Size of iw
!!        \param[in]    nsw       Size of lsw
!!        \param[in]    iw        Integer hampath code options
!!        \param[in]    dw        Double precision hampath code options
!!        \param[in]    sw        String hampath code options
!!        \param[in]    lsw       Length of each string option
!!
!!        @param[out]   infos       information on the integration process
!!                                  infos(1) = flag : should be 1
!!                                  infos(2) = nfev ; number of function evaluations
!!        @param[out]   dimeT    Number of steps calculated
!!
!!  \author Olivier Cots (INP-ENSEEIHT-IRIT)
!!  \date   2009-2015
!!  \copyright LGPL
!!
Subroutine dexpfunint(drhs,n,x0,x0d,npars,pars,parsd,nt,tspan,ninfos,infos,dimeT,message,     &
                      ndw,niw,nsw,dw,iw,sw,lsw)
    use mod_ivp_options
    use mod_dexp
    implicit none
    integer,            intent(in)                     :: n,ninfos
    integer,            intent(in)                     :: npars
    integer,            intent(in)                     :: nt
    double precision,   intent(in), dimension(nt)      :: tspan
    double precision,   intent(in), dimension(n)       :: x0
    double precision,   intent(in), dimension(n)       :: x0d
    double precision,   intent(in), dimension(npars)   :: pars, parsd
    integer,            intent(out)                    :: infos(ninfos)
    integer,            intent(out)                    :: dimeT
    character(len=120), intent(out)                    :: message
    integer,          intent(in)                       :: ndw, niw, nsw
    double precision, intent(in)                       :: dw(ndw)
    integer,          intent(in)                       :: iw(niw), lsw(nsw)
    character(32*nsw),intent(in)                       :: sw

    external drhs

    ! local variables
    type(ivp_options)  :: options

    !Initialize options and call the mere function
    call arraysToIVPOptions(ndw,niw,nsw,dw,iw,sw,lsw,options)
    call dexpfun(drhs,n,x0,x0d,npars,pars,parsd, &
        nt,tspan,options,ninfos,infos,dimeT,message)

end subroutine dexpfunint

