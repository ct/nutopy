module mod_exp

    use mod_ivp_options
    use mod_gest_liste
    use mod_integration

    implicit none

!! -------------------------------------------------------------------------------------------
    type(TLISTE), save :: states_out_mod

!! -------------------------------------------------------------------------------------------
    procedure(), private, pointer :: fun_dynamics_mod
    procedure(), private, pointer :: jac_dynamics_mod

!! -------------------------------------------------------------------------------------------
    type(TLISTE), save      :: time_grid_mod ! pb si on parallelise lors d'un tir multiple si integrateur a pas fixe il semblerait

!! -------------------------------------------------------------------------------------------
    character(32),  private :: integ_method_mod       = "nodef"

contains

    ! ------------------------------------------------------------------------
    ! ------------------------------------------------------------------------
    Subroutine expfun(rhs,drhs,n,x0,npar,par,nt,tspan,options,ninfos,infos,dimeT,message)
        implicit none
        integer,            intent(in)                      :: n,ninfos, nt
        integer,            intent(in)                      :: npar
        double precision,   intent(in)                      :: tspan(nt)
        double precision,   intent(in), dimension(n)        :: x0
        double precision,   intent(in), dimension(npar)     :: par
        type(ivp_options),  intent(in)                      :: options
        integer,            intent(out)                     :: infos(ninfos)
        integer,            intent(out)                     :: dimeT
        character(len=120), intent(out)                     :: message

        external rhs
        external drhs

        !local variables
        integer                                     :: flag
        double precision                            :: t0, tf
        double precision, dimension(n)              :: xf

        ! -------------------------
        ! OPTIONS AND PARAMETERS INIT
        ! -------------------------
        t0 = tspan(1)
        tf = tspan(nt)

        call initStatesGrid ! creer la liste pour sauver les points d'integration
        call initTimeGrid(nt,tspan)
        call initIntegrationMethod(options)
        ! -------------------------
        ! END OPTIONS AND PARAMETERS INIT
        ! -------------------------

        ! -------------------------
        ! SET FUNCTIONS
        ! -------------------------
        call set_dynamics(rhs, drhs) !
        ! -------------------------
        ! END SET FUNCTIONS
        ! -------------------------

        ! -------------------------
        ! CALL TO ODEEXP
        ! -------------------------
        call odeexp(options,n,x0,par,npar,n,t0,tf,xf,ninfos,infos)

        ! -------------------------
        ! POST-CALL TO ODEEXP
        ! -------------------------
        dimeT = LONGUEUR(states_out_mod)

        !
        flag = infos(1)
        select case(flag)
            case(0)
                message = 'No integration step since t0=tf'
            case(1)
                message = 'Integration successfully completed.'
            case(-1)
                message = 'Inputs are not consistent.'
            case(-2)
                message = 'Maximum number of steps (MaxSteps) reached.'
            case(-3)
                message = 'Step-size (MaxStepSize) is too small.'
            case(-4)
                message = 'The associated differential system is probably stiff.'
            case(-8)
                message = 'The maximum number of iterations to solve the NLE in the IRK method has been reached.'
            case default
                message = ''
        end select

    end subroutine expfun

    ! ------------------------------------------------------------------------
    ! ------------------------------------------------------------------------
    subroutine initStatesGrid()
        call CREER_STRUCTURE(states_out_mod)
    end subroutine initStatesGrid

    ! ------------------------------------------------------------------------
    ! ------------------------------------------------------------------------
    subroutine initTimeGrid(n,tspan)
        implicit none
        integer, intent(in)                                 :: n
        double precision, intent(in), dimension(n)          :: tspan

        !local variables
        integer                    :: i
        double precision, dimension(1) :: val

        !tspan must be sorted
        i=2
        do while(i.lt.n)
            if((tspan(i)-tspan(i-1))*(tspan(i+1)-tspan(i)).lt.0d0)then
                CALL printandstop('tspan must be sorted')
            end if
            i = i + 1
        end do

        !we add each instant from tspan in the list time_grid_mod
        val(1)=0 ! dummy
        call CREER_STRUCTURE(time_grid_mod,n-3)
        do i=1,n
            call INSERER(tspan(n-i+1),1,val,time_grid_mod)
        end do

    end subroutine initTimeGrid

    ! ------------------------------------------------------------------------
    ! ------------------------------------------------------------------------
    subroutine initIntegrationMethod(options)
        implicit none
        type(ivp_options), intent(in) :: options
        call getIVPOption(options,'ODESolver',integ_method_mod)
    end subroutine initIntegrationMethod

    ! ------------------------------------------------------------------------
    ! ------------------------------------------------------------------------
    subroutine set_dynamics(rhs, drhs)
        external rhs
        external drhs
        fun_dynamics_mod => rhs
        jac_dynamics_mod => drhs
    end subroutine set_dynamics

    ! ------------------------------------------------------------------------
    ! ------------------------------------------------------------------------
    Subroutine getstates(n,dimeT,Ts,Ys)
        implicit none
        integer,                                intent(in)  :: n
        integer,                                intent(in)  :: dimeT
        double precision,dimension(dimeT),     intent(out)  :: Ts
        double precision,dimension(n,dimeT),   intent(out)  :: Ys

        !local variables
        integer                             :: i
        double precision                    :: time
        double precision, dimension(n)      :: val

        do i=1,dimeT
            call EXTRAIRE(time,n,val,states_out_mod)
            Ts(dimeT-i+1) = time
            Ys(:,dimeT-i+1) = val
        end do

        call DETRUIRE(states_out_mod)
        call DETRUIRE(time_grid_mod)

    end subroutine getstates

    ! ------------------------------------------------------------------------
    ! ------------------------------------------------------------------------
    Subroutine odeexp(options,neq,y0,funpar,lfunpar,ncpas,tin,tout,yf,ninfos,infos)
        implicit none
        type(ivp_options),  intent(in)                      :: options
        integer,            intent(in)                      :: ninfos, lfunpar
        integer,            intent(in)                      :: neq
        integer,            intent(in)                      :: ncpas
        double precision,   intent(in),  dimension(neq)     :: y0
        double precision,   intent(in),  dimension(lfunpar) :: funpar
        double precision,   intent(inout)                   :: tin
        double precision,   intent(inout)                   :: tout
        double precision,   intent(out), dimension(neq)     :: yf
        integer,            intent(out)                     :: infos(ninfos)

        !local
        type(integration_parameters)    :: spec

        !
        character(32)                   :: ODESolver
        integer                         :: MaxSteps
        double precision                :: TolAbs, TolRel, MaxStepSize

        ! Defintion de l'ivp
        spec%fun           => exprhs
        spec%dfun          => dexprhs
        spec%phidez        => phidezExp
        spec%pdpdez        => pdpExp
        spec%solout        => soloutExp

        ! Options d'integration
        call setIntegrationParameter(spec, 'irki',              '2')
        call setIntegrationParameter(spec, 'irks',              'newton')
        call setIntegrationParameter(spec, 'ParentMethodName',  'exp')
        call setIntegrationParameter(spec, 'IntegrationList',   time_grid_mod)

        call setIntegrationParameter(spec, 'Display',           .false.)
        call setIntegrationParameter(spec, 'UseSolout',         .true.)
        call setIntegrationParameter(spec, 'UseRadauHampath',   .false.)

        call getIVPOption(options, 'MaxStepsOde',      MaxSteps   ); call setIntegrationParameter(spec, 'MaxSteps',    MaxSteps)
        call getIVPOption(options, 'TolOdeAbs',        TolAbs     ); call setIntegrationParameter(spec, 'TolAbs',      TolAbs)
        call getIVPOption(options, 'TolOdeRel',        TolRel     ); call setIntegrationParameter(spec, 'TolRel',      TolRel)
        call getIVPOption(options, 'MaxStepSizeOde',   MaxStepSize); call setIntegrationParameter(spec, 'MaxStepSize', MaxStepSize)
        call getIVPOption(options, 'ODESolver',        ODESolver  ); call setIntegrationParameter(spec, 'ODESolver',   ODESolver)

        ! Integration
        call integration(spec,neq,y0,funpar,lfunpar,ncpas,tin,tout,yf,ninfos,infos)

    end subroutine odeexp

    ! ------------------------------------------------------------------------
    ! ------------------------------------------------------------------------
    Subroutine exprhs(n,t,x,y,par,npar)
        implicit none
        integer,      intent(in)                          :: n
        integer,      intent(in)                          :: npar
        double precision, intent(in)                      :: t
        double precision, intent(in),  dimension(n)       :: x
        double precision, intent(in),  dimension(npar)    :: par
        double precision, intent(out), dimension(n)       :: y

        call fun_dynamics_mod(t, n, x, npar, par, y)

    end subroutine exprhs

    ! ------------------------------------------------------------------------
    ! ------------------------------------------------------------------------
    Subroutine dexprhs(n,t,x,jac,par,npar)
        implicit none
        integer,            intent(in)                      :: n
        integer,            intent(in)                      :: npar
        double precision,   intent(in)                      :: t
        double precision,   intent(in),  dimension(npar)    :: par
        double precision,   intent(in),  dimension(n)       :: x
        double precision,   intent(out), dimension(n,n)     :: jac

        !local variables
        integer          :: i
        double precision :: dx(n)

        integer :: j
        double precision :: eps, fvec1(n), fvec2(n), ytemp(n), h, jac_aux(n,n)

        dx  = 0d0
        jac = 0d0
        do i=1,n
            dx(i) = 1d0
            call jac_dynamics_mod(t, n, x, dx, npar, par, jac(:,i))
            dx(i) = 0d0
        end do

!        eps   = epsilon(1d0)
!        ytemp = x
!        call exprhs(n,t,ytemp,fvec2,paraux,nparaux)
!        do j=1,n
!            h = dsqrt(eps*max(1.d-5,abs(x(j))))
!            ytemp(j) = x(j) + h
!            call exprhs(n,t,ytemp,fvec1,paraux,nparaux)
!            ytemp(j) = x(j)
!            jac_aux(:,j) = (fvec1 - fvec2)/h
!        end do
!        write(*,*) "diffjac = ", jac-jac_aux

    end subroutine dexprhs

    ! ------------------------------------------------------------------------
    ! ------------------------------------------------------------------------
    subroutine phidezExp(nz, Zc, fvec, iflag, fpar, lfpar)
        implicit none
        integer,            intent(in)                          :: nz, lfpar
        double precision,   intent(in),  dimension(nz)          :: Zc
        double precision,   intent(out), dimension(nz)          :: fvec
        integer,            intent(inout)                       :: iflag
        double precision,   intent(inout),  dimension(lfpar)    :: fpar

!        external exphvrhs

        call phidez(nz, Zc, fvec, iflag, fpar, lfpar, exprhs)

    end subroutine phidezExp

    ! ------------------------------------------------------------------------
    ! ------------------------------------------------------------------------
    subroutine pdpExp(nz,Zc,fpar,lfpar,fvec,fjac,ldfjac,iflag)
        implicit none
        integer,          intent(in)                        :: nz
        integer,          intent(in)                        :: lfpar
        integer,          intent(in)                        :: ldfjac
        double precision, intent(inout), dimension(lfpar)   :: fpar
        double precision, intent(in), dimension(nz)         :: Zc
        double precision, intent(out), dimension(nz)        :: fvec
        double precision, intent(out), dimension(ldfjac,nz) :: fjac
        integer,          intent(inout)                     :: iflag

!        external exphvrhs
!        external dexphvrhs

        call phidphidez(nz,Zc,fpar,lfpar,fvec,fjac,ldfjac,iflag,exprhs,dexprhs)

    end subroutine pdpExp

    ! ------------------------------------------------------------------------
    ! ------------------------------------------------------------------------
    Subroutine soloutExp(Niter,told,time,y,neq,dpar,ldpar,ipar,lipar,fun,funpar,lfunpar,irtrn)
        implicit none
        integer,            intent(in)                         :: Niter
        double precision,   intent(in)                         :: told
        double precision,   intent(inout)                      :: time
        integer,            intent(in)                         :: neq
        integer,            intent(in)                         :: ldpar
        integer,            intent(in)                         :: lipar
        integer,            intent(in)                         :: lfunpar
        double precision,   intent(inout), dimension(neq)      :: y
        double precision,   intent(in),    dimension(ldpar)    :: dpar
        integer,            intent(in),    dimension(lipar)    :: ipar
        double precision,   intent(in),    dimension(lfunpar)  :: funpar
        integer,            intent(inout)                      :: irtrn

        interface
        Subroutine fun(neq,time,y,val,funpar,lfunpar)
            implicit none
            integer,           intent(in)                      :: neq
            integer,           intent(in)                      :: lfunpar
            double precision,  intent(in)                      :: time
            double precision,  intent(in),  dimension(lfunpar) :: funpar
            double precision,  intent(in),  dimension(neq)     :: y
            double precision,  intent(out), dimension(neq)     :: val
        end Subroutine fun
        end interface

        !local variables
        character(32) :: nameinte

        nameinte    = integ_method_mod

        call addstatesgeneral(addstatesexp,neq,time,y,told,lipar,ipar,ldpar,dpar,nameinte)

    end subroutine soloutExp

    ! ------------------------------------------------------------------------
    ! ------------------------------------------------------------------------
    Subroutine addstatesgeneral(addstates,neq,time,y,told,lipar,ipar,ldpar,dpar,nameinte)
        implicit none
        integer,            intent(in)                      :: neq
        double precision,   intent(in)                      :: time
        double precision,   intent(in), dimension(neq)      :: y
        double precision,   intent(in)                      :: told
        integer,            intent(in)                      :: ldpar
        double precision,   intent(in), dimension(ldpar)    :: dpar
        integer,            intent(in)                      :: lipar
        integer,            intent(in), dimension(lipar)    :: ipar
        character(32),      intent(in)                      :: nameinte

        interface
        Subroutine addstates(nz,time,z)
            implicit none
            integer, intent(in)                             :: nz
            double precision, intent(in)                    :: time
            double precision, intent(in), dimension(nz)     :: z
        end Subroutine addstates
        end interface

        !local variables
        integer                          :: i
        integer                          :: fini
        double precision, dimension(1)   :: val
        double precision, dimension(neq) :: yg
        double precision                 :: t,hstep
        double precision :: eps
        eps  = 10d0*epsilon(1d0)

        select case (nameinte)

            case ('dopri5','radau5old','dop853','radau5','radau9','radau13','radau','radause')

                if(GET_PARAM(time_grid_mod).EQ.-1)then !if tspan given by the user is [t0 tf]
                    call addstates(neq,time,y)
                else                            !if tspan given by the user is [t0 t1 .. tf]
                !we have to find all ti from tspan (ie time_grid_mod) contained in [told time]
                !and use dense output to get the value
                t     = GET_TIME(time_grid_mod)
                fini  = 0
                hstep = time - told
                    do while(((told-t)*(time-t).LE.0).AND.(fini.EQ.0))
                        call EXTRAIRE(t,1,val,time_grid_mod)
                        if(abs(time-t).gt.eps)then
                            do i=1,neq
                                call denseOutput(nameinte,i,t,yg(i),told,time,hstep,lipar,ipar,ldpar,dpar)
                            end do
                        else
                            yg=y
                        end if
                        call addstates(neq,t,yg)
                        if(LONGUEUR(time_grid_mod).EQ.0)then
                            fini=1
                        else
                            t = GET_TIME(time_grid_mod)
                        end if
                    end do
                end if

            case default

                call addstates(neq,time,y)

        end select

    end subroutine addstatesgeneral

    ! ------------------------------------------------------------------------
    ! ------------------------------------------------------------------------
    Subroutine addstatesexp(nz,time,z)
        implicit none

        integer, intent(in)                         :: nz
        double precision, intent(in)                :: time
        double precision, intent(in), dimension(nz) :: z

        !local variables
        !double precision :: eps
        !eps  = 10d0*epsilon(1d0)

        !if(LONGUEUR(states_out_mod).GE.1)then
            !if(abs(time-GET_TIME(states_out_mod)).gt.eps)then
                !call INSERER(time,nz,z,states_out_mod)
            !end if
        !else
            call INSERER(time,nz,z,states_out_mod)
        !end if

    end subroutine addstatesexp

end module mod_exp
