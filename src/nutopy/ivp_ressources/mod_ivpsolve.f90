module mod_ivpsolve

    implicit none

    double precision, allocatable, dimension(:)     :: tout

    ! pour djexp
    double precision, allocatable, dimension(:,:)   :: xout
    double precision, allocatable, dimension(:,:)   :: xdout
    double precision, allocatable, dimension(:,:)   :: dxout
    double precision, allocatable, dimension(:,:)   :: dxdout

    contains


!!********************************************************************
!!
!!     Subroutine nlesolvepy
!!
!>     @ingroup ivpsolvePackage
!!     @brief  Interface specific for python of expfunint.
!!
!!        @param[in]    n         State dimension
!!        @param[in]    z0        initial flow
!!        @param[in]    npars      Number of optional parameters
!!        @param[in]    par       Optional parameters
!!        @param[in]    nt        Dimension of tspan
!!        @param[in]    tspan     Grid time : [t0 t1 ... tf]
!!        @param[in]    nbarc     Number of arcs
!!        @param[in]    ti        Contains t0, tf and the intermediate times if any.
!!                                ti = [t0 t1 .. t_{nbarc-1} tf]
!!        \param[in]    ninfos      dimension of infos
!!        \param[in]    ndw       Size of dw
!!        \param[in]    niw       Size of iw
!!        \param[in]    nsw       Size of lsw
!!        \param[in]    nswInt    Size of swInt
!!        \param[in]    iw        Integer hampath code options
!!        \param[in]    dw        Double precision hampath code options
!!        \param[in]    swInt     integer hampath code options
!!        \param[in]    lsw       Length of each string option
!!
!!        @param[out]   infos       information on the integration process
!!                                  infos(1) = flag : should be 1
!!                                  infos(2) = nfev ; number of function evaluations
!!        @param[out]   dimeT    Number of steps calculated
!!
!!  \author Olivier Cots (INP-ENSEEIHT-IRIT)
!!  \date   2020
!!  \copyright LGPL
!!
    Subroutine expfunpy(rhs,drhs,n,x0,npars,pars,nt,tspan,flag,nfev,dimeT,message, &
                        ndw,niw,nsw,nswInt,dw,iw,swInt,lsw)
        use mod_exp
        implicit none
        integer,            intent(in)                      :: n
        integer,            intent(in)                      :: npars
        integer,            intent(in)                      :: nt
        double precision,   intent(in), dimension(nt)       :: tspan
        double precision,   intent(in), dimension(n)        :: x0
        double precision,   intent(in), dimension(npars)    :: pars
        integer,            intent(out)                     :: dimeT
        integer,            intent(out)                     :: flag
        integer,            intent(out)                     :: nfev
        character(len=120), intent(out)                     :: message
        integer,          intent(in)                        :: ndw, niw, nsw, nswInt
        double precision, intent(in)                        :: dw(ndw)
        integer,          intent(in)                        :: iw(niw), lsw(nsw), swInt(nswInt)

        external rhs
        external drhs

        !local variable
        integer                 :: i, ninfos, infos(2)
        character(len=32*nsw)   :: sw


        sw = ''
        do i=1,nswInt
            sw(i:i) = char(swInt(i))
        end do

        ninfos = 2
        call expfunint(rhs,drhs,n,x0,npars,pars,nt,tspan,ninfos,infos,dimeT,message,ndw,niw,nsw,dw,iw,sw,lsw)
        flag = infos(1)
        nfev = infos(2)

        if(allocated(tout))then
            deallocate(tout)
        end if

        if(allocated(xout))then
            deallocate(xout)
        end if

        allocate(tout(dimeT), xout(n,dimeT))

        call getstates(n,dimeT,tout,xout)

    end subroutine expfunpy

    Subroutine dexpfunpy(drhs,n,x0,x0d,npars,pars,parsd,nt,tspan,flag,nfev,dimeT,message, &
                         ndw,niw,nsw,nswInt,dw,iw,swInt,lsw)
        use mod_dexp
        implicit none
        integer,            intent(in)                      :: n
        integer,            intent(in)                      :: npars
        integer,            intent(in)                      :: nt
        double precision,   intent(in), dimension(nt)       :: tspan
        double precision,   intent(in), dimension(n)        :: x0
        double precision,   intent(in), dimension(n)        :: x0d
        double precision,   intent(in), dimension(npars)    :: pars, parsd
        integer,            intent(out)                     :: dimeT
        integer,            intent(out)                     :: flag
        integer,            intent(out)                     :: nfev
        character(len=120), intent(out)                     :: message
        integer,          intent(in)                        :: ndw, niw, nsw, nswInt
        double precision, intent(in)                        :: dw(ndw)
        integer,          intent(in)                        :: iw(niw), lsw(nsw), swInt(nswInt)

        external drhs

        !local variable
        integer                 :: i, ninfos, infos(2)
        character(len=32*nsw)   :: sw

        sw = ''
        do i=1,nswInt
            sw(i:i) = char(swInt(i))
        end do

        ninfos = 2
        call dexpfunint(drhs,n,x0,x0d,npars,pars,parsd,nt,tspan,ninfos,infos,dimeT,message,   &
                        ndw,niw,nsw,dw,iw,sw,lsw)
        flag = infos(1)
        nfev = infos(2)

        if(allocated(tout))then
            deallocate(tout)
        end if

        if(allocated(xout))then
            deallocate(xout)
        end if

        if(allocated(xdout))then
            deallocate(xdout)
        end if

        allocate(tout(dimeT), xout(n,dimeT), xdout(n,dimeT))

        call getdstates(n,dimeT,tout,xout,xdout)

    end subroutine dexpfunpy

    Subroutine jexpfunpy(rhsd,n,k,x0,dx0,npars,pars,nt,tspan,flag,nfev,dimeT,message, &
                         ndw,niw,nsw,nswInt,dw,iw,swInt,lsw)
        use mod_jexp
        implicit none
        integer,            intent(in)                      :: n, k
        integer,            intent(in)                      :: npars
        integer,            intent(in)                      :: nt
        double precision,   intent(in), dimension(nt)       :: tspan
        double precision,   intent(in), dimension(n)        :: x0
        double precision,   intent(in), dimension(n,k)      :: dx0
        double precision,   intent(in), dimension(npars)    :: pars
        integer,            intent(out)                     :: dimeT
        integer,            intent(out)                     :: flag
        integer,            intent(out)                     :: nfev
        character(len=120), intent(out)                     :: message
        integer,          intent(in)                        :: ndw, niw, nsw, nswInt
        double precision, intent(in)                        :: dw(ndw)
        integer,          intent(in)                        :: iw(niw), lsw(nsw), swInt(nswInt)

        external rhsd

        !local variable
        integer                 :: i, ninfos, infos(2)
        character(len=32*nsw)   :: sw

        sw = ''
        do i=1,nswInt
            sw(i:i) = char(swInt(i))
        end do

        ninfos = 2
        call jexpfunint(rhsd,n,k,x0,dx0,npars,pars,nt,tspan,ninfos,infos,dimeT,message,   &
                        ndw,niw,nsw,dw,iw,sw,lsw)
        flag = infos(1)
        nfev = infos(2)

        if(allocated(tout))then
            deallocate(tout)
        end if

        if(allocated(xout))then
            deallocate(xout)
        end if

        if(allocated(dxout))then
            deallocate(dxout)
        end if

        allocate(tout(dimeT), xout(n,dimeT), dxout(n,k*dimeT))

        call getstatesd(n,k,dimeT,tout,xout,dxout)

    end subroutine jexpfunpy

    Subroutine djexpfunpy(drhsd,n,k,x0,x0d,dx0,dx0d,npars,pars,parsd,nt,tspan,flag,nfev,dimeT,message, &
                         ndw,niw,nsw,nswInt,dw,iw,swInt,lsw)
        use mod_djexp
        implicit none
        integer,            intent(in)                      :: n, k
        integer,            intent(in)                      :: npars
        integer,            intent(in)                      :: nt
        double precision,   intent(in), dimension(nt)       :: tspan
        double precision,   intent(in), dimension(n)        :: x0
        double precision,   intent(in), dimension(n)        :: x0d
        double precision,   intent(in), dimension(n,k)      :: dx0
        double precision,   intent(in), dimension(n,k)      :: dx0d
        double precision,   intent(in), dimension(npars)    :: pars, parsd
        integer,            intent(out)                     :: dimeT
        integer,            intent(out)                     :: flag
        integer,            intent(out)                     :: nfev
        character(len=120), intent(out)                     :: message
        integer,          intent(in)                        :: ndw, niw, nsw, nswInt
        double precision, intent(in)                        :: dw(ndw)
        integer,          intent(in)                        :: iw(niw), lsw(nsw), swInt(nswInt)

        external drhsd

        !local variable
        integer                 :: i, ninfos, infos(2)
        character(len=32*nsw)   :: sw

        sw = ''
        do i=1,nswInt
            sw(i:i) = char(swInt(i))
        end do

        ninfos = 2
        call djexpfunint(drhsd,n,k,x0,x0d,dx0,dx0d,npars,pars,parsd,nt,tspan,ninfos,infos,dimeT,message,   &
                        ndw,niw,nsw,dw,iw,sw,lsw)
        flag = infos(1)
        nfev = infos(2)

        if(allocated(tout))then
            deallocate(tout)
        end if

        if(allocated(xout))then
            deallocate(xout)
        end if

        if(allocated(xdout))then
            deallocate(xdout)
        end if

        if(allocated(dxout))then
            deallocate(dxout)
        end if

        if(allocated(dxdout))then
            deallocate(dxdout)
        end if

        allocate(tout(dimeT), xout(n,dimeT), xdout(n,dimeT), dxout(n,k*dimeT), dxdout(n,k*dimeT))

        call getdstatesd(n,k,dimeT,tout,xout,xdout,dxout,dxdout)

    end subroutine djexpfunpy

end module mod_ivpsolve

