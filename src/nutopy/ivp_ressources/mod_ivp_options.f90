module mod_ivp_options

    implicit none

    ! ------------------------------------------------------------------------
    ! ------------------------------------------------------------------------
    type ivp_options
        integer             :: MaxStepsOde          = 100000
        double precision    :: MaxStepSizeOde       = 0d0
        character(32)       :: ODESolver            = 'dopri5'
        double precision    :: TolOdeAbs            = 1d-10
        double precision    :: TolOdeRel            = 1d-8
    end type ivp_options

    interface getIVPOption
        module procedure getIVPInt, getIVPReal, getIVPChar
    end interface

    interface setIVPOption
        module procedure setIVPInt, setIVPReal, setIVPChar
    end interface

    contains

    ! ------------------------------------------------------------------------
    ! ------------------------------------------------------------------------
    subroutine IVPOptionsToArrays(options,ndw,niw,nsw,dw,iw,sw,lsw)
        type(ivp_options), intent(in)                               :: options
        integer,                                        intent(out) :: ndw, niw, nsw
        double precision,   dimension(:), allocatable,  intent(out) :: dw
        integer,            dimension(:), allocatable,  intent(out) :: iw, lsw
        character(len=:),                 allocatable,  intent(out) :: sw

        !local variables
        integer         :: i, j
        character(32)   :: optValue

        ndw = 3
        niw = 1
        nsw = 1

        allocate(dw(ndw),iw(niw),lsw(nsw))
        allocate(character(len=32*nsw) :: sw)

        i = 1
        call getIVPOption(options,'MaxStepSizeOde'      ,dw(i)); i = i + 1;
        call getIVPOption(options,'TolOdeAbs'           ,dw(i)); i = i + 1;
        call getIVPOption(options,'TolOdeRel'           ,dw(i)); i = i + 1;

        i = 1
        call getIVPOption(options,'MaxStepsOde'         ,iw(i)); i = i + 1;

        i = 1
        j = 1
!        sw      = ''
        call getIVPOption(options,'ODESolver'  ,optValue); lsw(i) = len_trim(optValue); !sw = sw // trim(optValue); i = i + 1;
        sw(j:j+lsw(i)-1) = trim(optValue); j = j + lsw(i); i = i + 1;

    end subroutine IVPOptionsToArrays

    ! ------------------------------------------------------------------------
    ! ------------------------------------------------------------------------
    subroutine arraysToIVPOptions(ndw,niw,nsw,dw,iw,sw,lsw,options)
        integer,            intent(in)  :: ndw, niw, nsw
        double precision,   intent(in)  :: dw(ndw)
        integer,            intent(in)  :: iw(niw), lsw(nsw)
        character(32*nsw),  intent(in)  :: sw
        type(ivp_options),     intent(out) :: options

        !local variables
        integer            :: MaxStepsOde

        double precision   :: MaxStepSizeOde
        double precision   :: TolOdeAbs
        double precision   :: TolOdeRel

        integer            :: dimODESolver

        character(32)      :: ODESolver

        integer :: i

        !Integer
        i = 1
        MaxStepsOde             = iw(i); i = i + 1;

        !Float
        i = 1
        MaxStepSizeOde          = dw(i); i = i + 1;
        TolOdeAbs               = dw(i); i = i + 1;
        TolOdeRel               = dw(i); i = i + 1;

        !String
        i = 1
        dimODESolver            = lsw(i); i = i + 1;

        i = 1
        ODESolver   = ''

        ODESolver(1:dimODESolver)       = sw(i:i+dimODESolver   -1); i = i + dimODESolver;

        call setIVPOption(options,'MaxStepsOde'        ,MaxStepsOde        )

        call setIVPOption(options,'MaxStepSizeOde'     ,MaxStepSizeOde     )
        call setIVPOption(options,'TolOdeAbs'          ,TolOdeAbs          )
        call setIVPOption(options,'TolOdeRel'          ,TolOdeRel          )

        call setIVPOption(options,'ODESolver'          ,ODESolver          )

    end subroutine arraysToIVPOptions

    ! ------------------------------------------------------------------------
    ! ------------------------------------------------------------------------
    subroutine printIVPOptions(options)
        type(ivp_options), intent(in) :: options

        !local variables
        CHARACTER(len=120)                  :: LINE

        WRITE (LINE,'(a)')  'Options :'; call myprint(LINE,.true.);

        WRITE (LINE,'("  MaxStepsOde        = ",  i0.1)')  options%MaxStepsOde;         call myprint(LINE,.true.)
        WRITE (LINE,'("  MaxStepSizeOde     = ",e23.15)')  options%MaxStepSizeOde;      call myprint(LINE,.true.)
        WRITE (LINE,'("  ODESolver          = ",     a)')  options%ODESolver;           call myprint(LINE,.true.)
        WRITE (LINE,'("  TolOdeAbs          = ",e23.15)')  options%TolOdeAbs;           call myprint(LINE,.true.)
        WRITE (LINE,'("  TolOdeRel          = ",e23.15)')  options%TolOdeRel;           call myprint(LINE,.true.)
        !!!
        WRITE (LINE,'(a)')  ''; call myprint(LINE,.true.);
    end subroutine printIVPOptions

    ! ------------------------------------------------------------------------
    ! ------------------------------------------------------------------------
    subroutine getIVPInt(options,optName,optValue)
        type(ivp_options), intent(in)  :: options
        character(LEN=*)  , intent(in)  :: optName
        integer,        intent(out) :: optValue

        CHARACTER(len=120)  :: LINE
        select case (trim(optName))
            case ('MaxStepsOde')
                optValue = options%MaxStepsOde
            case default
            WRITE (LINE,*) 'Invalid option: ', trim(optName), ' or value option: ', optValue, '!'
            CALL printandstop(LINE)
        end select

    end subroutine getIVPInt

    ! ------------------------------------------------------------------------
    ! ------------------------------------------------------------------------
    subroutine getIVPReal(options,optName,optValue)
        type(ivp_options),     intent(in)  :: options
        character(LEN=*),     intent(in)  :: optName
        double precision,   intent(out) :: optValue

        CHARACTER(len=120)  :: LINE
        select case (trim(optName))
            case ('MaxStepSizeOde')
                optValue = options%MaxStepSizeOde
            case ('TolOdeAbs')
                optValue = options%TolOdeAbs
            case ('TolOdeRel')
                optValue = options%TolOdeRel
            case default
            WRITE (LINE,*) 'Invalid option: ', trim(optName), ' or value option: ', optValue, '!'
            CALL printandstop(LINE)
        end select

    end subroutine getIVPReal

    ! ------------------------------------------------------------------------
    ! ------------------------------------------------------------------------
    subroutine getIVPChar(options,optName,optValue)
        type(ivp_options), intent(in)  :: options
        character(LEN=*)  , intent(in)  :: optName
        character(LEN=*)  , intent(out) :: optValue

        CHARACTER(len=120)  :: LINE
        select case (trim(optName))
            case ('ODESolver')
                optValue = options%ODESolver
            case default
            WRITE (LINE,*) 'Invalid option: ', trim(optName), ' or value option: ', optValue, '!'
            CALL printandstop(LINE)
        end select

    end subroutine getIVPChar

    ! ------------------------------------------------------------------------
    ! ------------------------------------------------------------------------
    subroutine setIVPInt(options,optName,optValue)
        type(ivp_options),   intent(inout)   :: options
        character(LEN=*), intent(in)      :: optName
        integer,          intent(in)      :: optValue

        CHARACTER(len=120)  :: LINE
        select case (trim(optName))
            case ('MaxStepsOde')
                  options%MaxStepsOde        = optValue
            case default
            WRITE (LINE,*) 'Invalid option: ', trim(optName), ' or value option: ', optValue, '!'
            CALL printandstop(LINE)
        end select

    end subroutine setIVPInt

    ! ------------------------------------------------------------------------
    ! ------------------------------------------------------------------------
    subroutine setIVPReal(options,optName,optValue)
        type(ivp_options),     intent(inout)   :: options
        character(LEN=*),   intent(in)      :: optName
        double precision,   intent(in)      :: optValue

        CHARACTER(len=120)  :: LINE
        select case (trim(optName))
            case ('MaxStepSizeOde')
                  options%MaxStepSizeOde        = optValue
            case ('TolOdeAbs')
                  options%TolOdeAbs             = optValue
            case ('TolOdeRel')
                  options%TolOdeRel             = optValue
            case default
            WRITE (LINE,*) 'Invalid option: ', trim(optName), ' or value option: ', optValue, '!'
            CALL printandstop(LINE)
        end select

    end subroutine setIVPReal

    ! ------------------------------------------------------------------------
    ! ------------------------------------------------------------------------
    subroutine setIVPChar(options,optName,optValue)
        type(ivp_options), intent(inout)   :: options
        character(LEN=*)  , intent(in)      :: optName
        character(LEN=*)  , intent(in)      :: optValue

        CHARACTER(len=120)  :: LINE
        select case (trim(optName))
            case ('ODESolver')
                  options%ODESolver        = trim(optValue)
            case default
            WRITE (LINE,*) 'Invalid option: ', trim(optName), ' or value option: ', optValue, '!'
            CALL printandstop(LINE)
        end select

    end subroutine setIVPChar

end module mod_ivp_options
