from sys import platform

def configuration(parent_package='', top_path=None):

    from numpy.distutils.misc_util import Configuration

    third_parties_dir   = '../thirdparties/'
    shared_dir          = '../shared/'
    ivp_dir = './ivp_ressources/'
    nle_dir = './nle_ressources/'
    path_dir = './path_ressources/'

    config              = Configuration('nutopy', parent_package, top_path)

    # config IVP
    source_files        = []
    if platform == "win32":
        source_files.append([third_parties_dir + 'MINPACK_f77.F'])

    source_files.append([third_parties_dir + 'dc_decsol_f77.F',
                         third_parties_dir + 'dc_decsol_hampath_f77.F',
                         third_parties_dir  + 'decsol_f77.F',
                         third_parties_dir  + 'dop853_f77.F',
                         third_parties_dir  + 'dopri5_f77.F',
                         third_parties_dir  + 'hybrd_f77.F',
                         third_parties_dir  + 'hybrj_f77.F',
                         third_parties_dir  + 'radau_f77.F',
                         third_parties_dir  + 'radau_hampath_f77.F',
                         shared_dir + 'myprint.f90',
                         shared_dir + 'printandstop.f90',
                         shared_dir + 'hybrdint.f90',
                         shared_dir + 'hybrjint.f90',
                         shared_dir + 'mod_gest_liste.f90',
                         shared_dir + 'String_Utility.f90',
                         shared_dir + 'utils.f90',
                         shared_dir + 'mod_integration.f90',
                         ivp_dir + 'mod_ivp_options.f90',
                         ivp_dir + 'mod_exp.f90',
                         ivp_dir + 'expfunint.f90',
                         ivp_dir + 'mod_dexp.f90',
                         ivp_dir + 'dexpfunint.f90',
                         ivp_dir + 'mod_jexp.f90',
                         ivp_dir + 'jexpfunint.f90',
                         ivp_dir + 'mod_djexp.f90',
                         ivp_dir + 'djexpfunint.f90',
                         ivp_dir + 'mod_ivpsolve.f90',
                         ivp_dir + 'mod_ivpsolve.pyf'])

    config.add_extension(name='mod_ivpsolve',
                         sources=source_files,
                         libraries=['lapack', 'blas', 'cminpack'] if platform != "win32" else ['openblas'])

    # config NLE
    source_files = []
    if platform == "win32":
        source_files.append([third_parties_dir + 'MINPACK_f77.F'])

    source_files.append([third_parties_dir + 'hybrj_f77.F',
                         shared_dir + 'printandstop.f90',
                         shared_dir + 'myprint.f90',
                         nle_dir + 'mod_options.f90',
                         nle_dir + 'mod_hybrj.f90',
                         nle_dir + 'nlesolveint.f90',
                         nle_dir + 'mod_nlesolve.f90',
                         nle_dir + 'mod_nlesolve.pyf'])

    config.add_extension(name='mod_nlesolve',  # name of pyf file
                         sources=source_files,
                         libraries=['cminpack'] if platform != "win32" else [])

    # config PATH
    source_files = []
    if platform == "win32":
        source_files.append([third_parties_dir + 'MINPACK_f77.F'])

    source_files.append([third_parties_dir + 'dc_decsol_f77.F',
                         third_parties_dir + 'dc_decsol_hampath_f77.F',
                         third_parties_dir + 'decsol_f77.F',
                         third_parties_dir + 'dop853_f77.F',
                         third_parties_dir  + 'dopri5_f77.F',
                         third_parties_dir  + 'hybrd_f77.F',
                         third_parties_dir  + 'hybrj_f77.F',
                         third_parties_dir  + 'radau_f77.F',
                         third_parties_dir  + 'radau_hampath_f77.F',
                         shared_dir         + 'myprint.f90',
                         shared_dir         + 'printandstop.f90',
                         shared_dir         + 'hybrdint.f90',
                         shared_dir         + 'hybrjint.f90',
                         shared_dir         + 'mod_gest_liste.f90',
                         shared_dir         + 'String_Utility.f90',
                         shared_dir         + 'utils.f90',
                         shared_dir         + 'mod_integration.f90',
                         path_dir           + 'mod_pcam.F90',
                         path_dir           + 'pathsolveint.f90',
                         path_dir           + 'mod_pathsolve.f90',
                         path_dir           + 'mod_pathsolve.pyf'])

    config.add_extension(name='mod_pathsolve',
                         sources=source_files,
                         libraries=['lapack', 'blas', 'cminpack'] if platform != "win32" else ['openblas'])

    return config


if __name__ == '__main__':
    from numpy.distutils.core import setup

    setup(configuration=configuration)
