

!>     @ingroup nlesolvePackage
!!     @brief   Subroutine given to hybrj. Call sfun or sjac,
!!              depending on the value iflag.
!!
!!        @param[in]    ny        Shooting variable dimension
!!        @param[in]    x0        Initial guess: shooting variable
!!        @param[in]    npar      Number of optional parameters
!!        @param[in]    par       Optional parameters
!!        \param[in]    ndw       Size of dw
!!        \param[in]    niw       Size of iw
!!        \param[in]    nsw       Size of lsw
!!        \param[in]    iw        Integer hampath code options
!!        \param[in]    dw        Double precision hampath code options
!!        \param[in]    sw        String hampath code options
!!        \param[in]    lsw       Length of each string option
!!
!!        @param[out]    xsol   Solution : y solution
!!        @param[out]    Fsol   Solution : sfun(xsol)
!!        @param[out]    nfev   Number of sfun evaluation
!!        @param[out]    njev   Number of jacfun evaluation
!!        @param[out]    flag   solver output (should be 1)
!!
!!  \author Olivier Cots (INP-ENSEEIHT-IRIT)
!!  \date   2019
!!  \copyright LGPL
!!
Subroutine nlesolveint(fun,jac,ny,x0,npar,par,callback,     &
                        xsol,Fsol,nfev,njev,flag,message,   &
                            ndw,niw,nsw,dw,iw,sw,lsw,isFloat)
    use mod_options
    use mod_hybrj
    implicit none
    integer,            intent(in)                      :: ny
    double precision,   intent(in),  dimension(ny)      :: x0
    integer,            intent(in)                      :: npar
    double precision,   intent(in),  dimension(npar)    :: par
    double precision,   intent(out), dimension(ny)      :: xsol
    double precision,   intent(out), dimension(ny)      :: Fsol
    integer,            intent(out)                     :: nfev
    integer,            intent(out)                     :: njev
    integer,            intent(out)                     :: flag
    character(len=120), intent(out)                     :: message
    integer,            intent(in)                      :: ndw, niw, nsw
    double precision,   intent(in)                      :: dw(ndw)
    integer,            intent(in)                      :: iw(niw), lsw(nsw)
    character(len=32*nsw),  intent(in)                  :: sw
    logical, intent(in)                                 :: isFloat

    external fun, jac, callback

    ! local variables
    type(TOptions)  :: options
    character(32)   :: SolverMethod

!    call fun(ny, x0, npar, par, xsol)
!    call jac(ny, x0, npar, par, xsol)
!    Fsol = 0d0
!    nfev = 0
!    njev = 0
!    flag = 0

    ! rediriger vers la bonne methode
    ! creer les options et les faire passer a hybrjint

    ! On cree les options
    call arraysToOptions(ndw,niw,nsw,dw,iw,sw,lsw,options)

    ! suivant le solverMethod, on redirige
    call getOption(options,'SolverMethod',SolverMethod)

    select case (SolverMethod)
        case ('hybrj')
            call hybrjint(fun,jac,ny,x0,npar,par,options,callback,isFloat, &
                            xsol,Fsol,nfev,njev,flag,message)
        case default
            CALL printandstop('  ||| ERROR: Solving -> Unknown solver ' // SolverMethod)
    end select

end subroutine nlesolveint

