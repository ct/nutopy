!! -----------------------------------------------------------------------------
!!
!! Copyright 2016, Olivier Cots.
!!
!! This file is part of HamPath.
!!
!! HamPath is free software: you can redistribute it and/or modify
!! it under the terms of the GNU Lesser General Public License as published by
!! the Free Software Foundation, either version 3 of the License, or
!! (at your option) any later version.
!!
!! HamPath is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU Lesser General Public License for more details.
!!
!! You should have received a copy of the GNU Lesser General Public License
!! along with HamPath. If not, see <http://www.gnu.org/licenses/>.
!!
!! -----------------------------------------------------------------------------
!!
!% Options for hampath packages.
module mod_options
    !use utils
    implicit none

    !!-------------------------------------------------------!!
    !!-------------------------------------------------------!!
    !>  @ingroup modulesInterfaces
    !!  @brief   Options structure.
    !!
    !!  \author Olivier Cots (INP-ENSEEIHT-IRIT)
    !!  \date   2009-2015
    !!  \copyright LGPL
    !!
    type TOptions
        !>  @ingroup modulesInterfaces
        !! \brief see \ref defs
        !! \memberof mod_options::toptions
        character(32)       :: Display              = 'on'
        !! \memberof mod_options::toptions
        double precision    :: TolX                 = 1d-8
        !>  @ingroup modulesInterfaces
        !! \brief see \ref defs
        !! \memberof mod_options::toptions
        character(32)       :: SolverMethod         = 'hybrj'
        !>  @ingroup modulesInterfaces
        !! \brief see \ref defs
        !! \memberof mod_options::toptions
        integer             :: MaxFEval             = 2000
   end type TOptions

    !!-------------------------------------------------------!!
    !!-------------------------------------------------------!!
    !>  @ingroup modulesInterfaces
    !!  @brief   getOption interface.
    !!  \details It is used by fortran user or called by other language interface.
    !!           It makes the link between the user and the core of the program.
    !!
    !!  \author Olivier Cots
    !!  \date   2009-2013
    !!  \copyright LGPL
    !!
    interface getOption
        module procedure getInt, getReal, getChar
    end interface

    !!-------------------------------------------------------!!
    !!-------------------------------------------------------!!
    !>  @ingroup modulesInterfaces
    !!  @brief   setOption interface.
    !!  \details It is used by fortran user or called by other language interface.
    !!           It makes the link between the user and the core of the program.
    !!
    !!  \author Olivier Cots
    !!  \date   2009-2013
    !!  \copyright LGPL
    !!
    interface setOption
        module procedure setInt, setReal, setChar
    end interface

    contains


    !!-------------------------------------------------------!!
    !!-------------------------------------------------------!!
    !>  @ingroup modulesInterfaces
    !!  @brief  Transform options into dw, iw, sw and lsw.
    !!      @param[in]  options     stucture containing all options
    !!        \param[out]    ndw       Size of dw
    !!        \param[out]    niw       Size of iw
    !!        \param[out]    nsw       Size of lsw
    !!        \param[out]    iw        Integer hampath code options
    !!        \param[out]    dw        Double precision hampath code options
    !!        \param[out]    sw        String hampath code options
    !!        \param[out]    lsw       Length of each string option
    !!
    !!  \author Olivier Cots (INP-ENSEEIHT-IRIT)
    !!  \date   2015
    !!  \copyright LGPL
    !!
    subroutine optionsToArrays(options,ndw,niw,nsw,dw,iw,sw,lsw)
        type(TOptions), intent(in)          :: options
        integer,                                        intent(out) :: ndw, niw, nsw
        double precision,   dimension(:), allocatable,  intent(out) :: dw
        integer,            dimension(:), allocatable,  intent(out) :: iw, lsw
        character(len=:),                 allocatable,  intent(out) :: sw

        !local variables
        integer         :: i, j
        character(32)   :: optValue

        ndw = 1
        niw = 1
        nsw = 2

        allocate(dw(ndw),iw(niw),lsw(nsw))
        allocate(character(len=32*nsw) :: sw)

        i = 1
        call getOption(options,'TolX'                ,dw(i)); i = i + 1;

        i = 1
        call getOption(options,'MaxFEval'            ,iw(i)); i = i + 1;

        i = 1
        j = 1
!        sw      = ''
        call getOption(options,'Display'   ,optValue); lsw(i) = len_trim(optValue); !sw = sw // trim(optValue); i = i + 1;
        sw(j:j+lsw(i)-1) = trim(optValue); j = j + lsw(i); i = i + 1;
        call getOption(options,'SolverMethod',optValue); lsw(i) = len_trim(optValue); !sw = sw // trim(optValue); i = i + 1;
        sw(j:j+lsw(i)-1) = trim(optValue); j = j + lsw(i); i = i + 1;

    end subroutine optionsToArrays


    !!-------------------------------------------------------!!
    !!-------------------------------------------------------!!
    !>  @ingroup modulesInterfaces
    !!  @brief  Transform options into dw, iw, sw and lsw.
    !!      @param[in]  options     stucture containing all options
    !!        \param[out]    ndw       Size of dw
    !!        \param[out]    niw       Size of iw
    !!        \param[out]    nsw       Size of lsw
    !!        \param[out]    iw        Integer hampath code options
    !!        \param[out]    dw        Double precision hampath code options
    !!        \param[out]    sw        String hampath code options
    !!        \param[out]    lsw       Length of each string option
    !!
    !!  \author Olivier Cots (INP-ENSEEIHT-IRIT)
    !!  \date   2015
    !!  \copyright LGPL
    !!
    subroutine arraysToOptions(ndw,niw,nsw,dw,iw,sw,lsw,options)
        integer,            intent(in)  :: ndw, niw, nsw
        double precision,   intent(in)  :: dw(ndw)
        integer,            intent(in)  :: iw(niw), lsw(nsw)
        character(32*nsw),  intent(in)  :: sw
        type(TOptions),     intent(out) :: options

        !local variables
        integer            :: MaxFEval

        double precision   :: TolX

        integer            :: dimDisplay
        integer            :: dimSolverMethod

        character(32)      :: Display
        character(32)      :: SolverMethod

        integer :: i

        !Integer
        i = 1
        MaxFEval               = iw(i); i = i + 1;

        !Float
        i = 1
        TolX                   = dw(i); i = i + 1;

        !String
        i = 1
        dimDisplay             = lsw(i); i = i + 1;
        dimSolverMethod        = lsw(i); i = i + 1;

        i = 1
        Display     = ''
        SolverMethod= ''

        Display(1:dimDisplay)           = sw(i:i+dimDisplay     -1); i = i + dimDisplay;
        SolverMethod(1:dimSolverMethod) = sw(i:i+dimSolverMethod-1); i = i + dimSolverMethod;

        call setOption(options,'Display'            ,Display  )
        call setOption(options,'SolverMethod'       ,SolverMethod  )

        call setOption(options,'MaxFEval'           ,MaxFEval  )

        call setOption(options,'TolX'               ,TolX  )

    end subroutine arraysToOptions

    !!-------------------------------------------------------!!
    !!-------------------------------------------------------!!
    !>  @ingroup modulesInterfaces
    !!  @brief  Print options.
    !!  @param[in] options options to be printed
    !!
    !!  \author Olivier Cots
    !!  \date   2009-2016
    !!  \copyright LGPL
    !!
    subroutine printOptions(options)
        type(TOptions), intent(in) :: options

        !local variables
        CHARACTER(len=120)                  :: LINE

        WRITE (LINE,'(a)')  'Options :'; call myprint(LINE,.true.);

        WRITE (LINE,'("  Display            = ",     a)')  options%Display;             call myprint(LINE,.true.)
        WRITE (LINE,'("  MaxFEval           = ",  i0.1)')  options%MaxFEval;            call myprint(LINE,.true.)
        WRITE (LINE,'("  SolverMethod       = ",     a)')  options%SolverMethod;        call myprint(LINE,.true.)
        WRITE (LINE,'("  TolX               = ",e23.15)')  options%TolX;                call myprint(LINE,.true.)
        !!!
        WRITE (LINE,'(a)')  ''; call myprint(LINE,.true.);

    end subroutine printOptions

    !!-------------------------------------------------------!!
    !!-------------------------------------------------------!!
    !>  @ingroup modulesInterfaces
    !!  @brief  Get integer option.
    !!  @param[in]  options     stucture containing all options
    !!  @param[in]  optName     name of the option to get
    !!  @param[out] optValue    value of the option
    !!
    !!  \author Olivier Cots
    !!  \date   2009-2013
    !!  \copyright LGPL
    !!
    subroutine getInt(options,optName,optValue)
        type(TOptions), intent(in)  :: options
        character(LEN=*)  , intent(in)  :: optName
        integer,        intent(out) :: optValue

        CHARACTER(len=120)  :: LINE
        select case (trim(optName))
            case ('MaxFEval')
                optValue = options%MaxFEval
            case default
            WRITE (LINE,*) 'Invalid option: ', trim(optName), ' or value option: ', optValue, '!'
            CALL printandstop(LINE)
        end select

    end subroutine getInt

    !!-------------------------------------------------------!!
    !!-------------------------------------------------------!!
    !>  @ingroup modulesInterfaces
    !!  @brief  Get real option.
    !!  @param[in]  options     stucture containing all options
    !!  @param[in]  optName     name of the option to get
    !!  @param[out] optValue    value of the option
    !!
    !!  \author Olivier Cots
    !!  \date   2009-2013
    !!  \copyright LGPL
    !!
    subroutine getReal(options,optName,optValue)
        type(TOptions),     intent(in)  :: options
        character(LEN=*),     intent(in)  :: optName
        double precision,   intent(out) :: optValue

        CHARACTER(len=120)  :: LINE
        select case (trim(optName))
            case ('TolX')
                optValue = options%TolX
            case default
            WRITE (LINE,*) 'Invalid option: ', trim(optName), ' or value option: ', optValue, '!'
            CALL printandstop(LINE)
        end select

    end subroutine getReal

    !!-------------------------------------------------------!!
    !!-------------------------------------------------------!!
    !>  @ingroup modulesInterfaces
    !!  @brief  Get string option.
    !!  @param[in]  options     stucture containing all options
    !!  @param[in]  optName     name of the option to get
    !!  @param[out] optValue    value of the option
    !!
    !!  \author Olivier Cots (INP-ENSEEIHT-IRIT)
    !!  \date   2009-2015
    !!  \copyright LGPL
    !!
    subroutine getChar(options,optName,optValue)
        type(TOptions), intent(in)  :: options
        character(LEN=*)  , intent(in)  :: optName
        character(LEN=*)  , intent(out) :: optValue

        CHARACTER(len=120)  :: LINE
        select case (trim(optName))
            case ('Display')
                optValue = options%Display
            case ('SolverMethod')
                optValue = options%SolverMethod
            case default
            WRITE (LINE,*) 'Invalid option: ', trim(optName), ' or value option: ', optValue, '!'
            CALL printandstop(LINE)
        end select

    end subroutine getChar

    !!-------------------------------------------------------!!
    !!-------------------------------------------------------!!
    !>  @ingroup modulesInterfaces
    !!  @brief  Set integer option.
    !!  @param[in,out]   options     stucture containing all options
    !!  @param[in]      optName     name of the option to set
    !!  @param[in]      optValue    value of the option
    !!
    !!  \author Olivier Cots
    !!  \date   2009-2013
    !!  \copyright LGPL
    !!
    subroutine setInt(options,optName,optValue)
        type(TOptions),   intent(inout)   :: options
        character(LEN=*), intent(in)      :: optName
        integer,          intent(in)      :: optValue

        CHARACTER(len=120)  :: LINE
        select case (trim(optName))
            case ('MaxFEval')
                  options%MaxFEval              = optValue
            case default
            WRITE (LINE,*) 'Invalid option: ', trim(optName), ' or value option: ', optValue, '!'
            CALL printandstop(LINE)
        end select

    end subroutine setInt

    !!-------------------------------------------------------!!
    !!-------------------------------------------------------!!
    !>  @ingroup modulesInterfaces
    !!  @brief  Set real option.
    !!  @param[in,out]   options     stucture containing all options
    !!  @param[in]      optName     name of the option to set
    !!  @param[in]      optValue    value of the option
    !!
    !!  \author Olivier Cots
    !!  \date   2009-2013
    !!  \copyright LGPL
    !!
    subroutine setReal(options,optName,optValue)
        type(TOptions),     intent(inout)   :: options
        character(LEN=*),   intent(in)      :: optName
        double precision,   intent(in)      :: optValue

        CHARACTER(len=120)  :: LINE
        select case (trim(optName))
            case ('TolX')
                  options%TolX              = optValue
            case default
            WRITE (LINE,*) 'Invalid option: ', trim(optName), ' or value option: ', optValue, '!'
            CALL printandstop(LINE)
        end select

    end subroutine setReal

    !!-------------------------------------------------------!!
    !!-------------------------------------------------------!!
    !>  @ingroup modulesInterfaces
    !!  @brief  Set string option.
    !!  @param[in,out]   options     stucture containing all options
    !!  @param[in]      optName     name of the option to set
    !!  @param[in]      optValue    value of the option
    !!
    !!  \author Olivier Cots (INP-ENSEEIHT-IRIT)
    !!  \date   2009-2015
    !!  \copyright LGPL
    !!
    subroutine setChar(options,optName,optValue)
        type(TOptions), intent(inout)   :: options
        character(LEN=*)  , intent(in)      :: optName
        character(LEN=*)  , intent(in)      :: optValue

        CHARACTER(len=120)  :: LINE
        select case (trim(optName))
            case ('Display')
                  options%Display       = trim(optValue)
            case ('SolverMethod')
                  options%SolverMethod  = trim(optValue)
            case default
            WRITE (LINE,*) 'Invalid option: ', trim(optName), ' or value option: ', optValue, '!'
            CALL printandstop(LINE)
        end select

    end subroutine setChar

end module mod_options
