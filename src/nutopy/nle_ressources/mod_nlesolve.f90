module mod_nlesolve

    implicit none

    contains

!!********************************************************************
!!
!!     Subroutine nlesolvepy
!!
!>     @ingroup nlesolvePackage
!!     @brief  Interface specific for python of nlesolveint.
!!
!!        @param[in]    ny        Shooting variable dimension
!!        @param[in]    x0        Initial guess: shooting variable
!!        @param[in]    npar      Number of optional parameters
!!        @param[in]    par       Optional parameters
!!        \param[in]    ndw       Size of dw
!!        \param[in]    niw       Size of iw
!!        \param[in]    nsw       Size of lsw
!!        \param[in]    nswInt    Size of swInt
!!        \param[in]    iw        Integer hampath code options
!!        \param[in]    dw        Double precision hampath code options
!!        \param[in]    swInt     Integer hampath code options
!!        \param[in]    lsw       Length of each string option
!!
!!        @param[out]    xsol   Solution : y solution
!!        @param[out]    Fsol   Solution : sfun(xsol)
!!        @param[out]    nfev   Number of sfun evaluation
!!        @param[out]    njev   Number of jacfun evaluation
!!        @param[out]    flag   solver output (should be 1)
!!
!!  \author Olivier Cots (INP-ENSEEIHT-IRIT)
!!  \date   2019
!!  \copyright LGPL
!!
    Subroutine nlesolvepy(fun,jac,ny,x0,npar,par,callback,          &
                            xsol,Fsol,nfev,njev,flag,message,       &
                                ndw,niw,nsw,nswInt,dw,iw,swInt,lsw)
        implicit none
        integer,            intent(in)                      :: ny
        double precision,   intent(in),  dimension(ny)      :: x0
        integer,            intent(in)                      :: npar
        double precision,   intent(in),  dimension(npar)    :: par
        double precision,   intent(out), dimension(ny)      :: xsol
        double precision,   intent(out), dimension(ny)      :: Fsol
        integer,            intent(out)                     :: nfev
        integer,            intent(out)                     :: njev
        integer,            intent(out)                     :: flag
        character(len=120), intent(out)                     :: message
        integer,            intent(in)                      :: ndw, niw, nsw, nswInt
        double precision,   intent(in)                      :: dw(ndw)
        integer,            intent(in)                      :: iw(niw), lsw(nsw)
        integer,            intent(in)                      :: swInt(nswInt)

        external fun, jac, callback

        !local variable
        integer                 :: i
        character(len=32*nsw)   :: sw

        logical                 :: isFloat

        sw = ''
        do i=1,nswInt
            sw(i:i) = char(swInt(i))
        end do

        ! if x0 is a float or not
        isFloat = .False.

        call nlesolveint(fun,jac,ny,x0,npar,par,callback,       &
                         xsol,Fsol,nfev,njev,flag,message,      &
                         ndw,niw,nsw,dw,iw,sw,lsw,isFloat)

   end subroutine nlesolvepy


    Subroutine nlesolvepy_float(fun,jac,x0,callback,          &
                            xsol,Fsol,nfev,njev,flag,message,       &
                                ndw,niw,nsw,nswInt,dw,iw,swInt,lsw)
        implicit none
        double precision,   intent(in)      :: x0
        double precision,   intent(out)     :: xsol
        double precision,   intent(out)     :: Fsol
        integer,            intent(out)     :: nfev
        integer,            intent(out)     :: njev
        integer,            intent(out)     :: flag
        character(len=120), intent(out)     :: message
        integer,            intent(in)      :: ndw, niw, nsw, nswInt
        double precision,   intent(in)      :: dw(ndw)
        integer,            intent(in)      :: iw(niw), lsw(nsw)
        integer,            intent(in)      :: swInt(nswInt)

        external fun, jac, callback

        !local variable
        integer                 :: i
        character(len=32*nsw)   :: sw

        integer             :: nx, npar
        double precision    :: x0_aux(1), xsol_aux(1), Fsol_aux(1), par_dummy(1)

        logical             :: isFloat

        sw = ''
        do i=1,nswInt
            sw(i:i) = char(swInt(i))
        end do

        ! if x0 is a float or not
        isFloat     = .True.
        nx          = 1
        x0_aux(1)   = x0

        npar        = 1
        par_dummy   = 0d0

        call nlesolveint(fun,jac,nx,x0_aux,npar,par_dummy,callback,       &
                         xsol_aux,Fsol_aux,nfev,njev,flag,message,      &
                         ndw,niw,nsw,dw,iw,sw,lsw,isFloat)

        xsol = xsol_aux(1)
        Fsol = Fsol_aux(1)

   end subroutine nlesolvepy_float

end module mod_nlesolve

