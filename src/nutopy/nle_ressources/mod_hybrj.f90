module mod_hybrj

    implicit none

    procedure(), private, pointer :: jac_mod
    procedure(), private, pointer :: fun_mod
    procedure(), private, pointer :: callback_mod
    procedure(), private, pointer :: jac_mod_float
    procedure(), private, pointer :: fun_mod_float
    procedure(), private, pointer :: callback_mod_float

    logical, private    :: display_mod

    !! -------------------------------------------------------------------------------------------
    logical, private    :: solver_stopped_by_user

    logical, private    :: isFloat_mod

    contains

    !>     @ingroup nlesolvePackage
    !!     @brief  hybrj interface.
    !!     @param[in] jacfun    gives function for which one wants to find a zero and its Jacobian
    !!    \param[in] ny         dimension of y
    !!    \param[in] x0i        initial guess
    !!    \param[in] lparaux    number of parameters
    !!    \param[in] paraux     parameters given to jacfun
    !!    \param[in] xtol       tolerance
    !!    \param[in] maxfev     maximum number of evaluations of fcn
    !!    \param[in] xsol       solution
    !!    \param[in] Fsol      evaluation of fcn at the solution
    !!    \param[in] nfev       number of evaluations of the function
    !!    \param[in] njac       number of evaluations of the Jacobian
    !!    \param[in] flag       should be 1
    !!
    !!  \author Olivier Cots
    !!  \date   2019
    !!  \copyright LGPL
    !!
    Subroutine hybrjint(fun,jac,ny,x0i,npar,par,options,callback,isFloat, &
                        xsol,Fsol,nfev,njev,flag,message)
        use mod_options
        implicit none
        integer,            intent(in)                    :: ny
        integer,            intent(in)                    :: npar
        double precision,   intent(in), dimension(npar)   :: par
        double precision,   intent(in), dimension(ny)     :: x0i
        type(TOptions),     intent(in)                    :: options
        logical, intent(in)                               :: isFloat
        double precision,   intent(out), dimension(ny)    :: xsol
        double precision,   intent(out), dimension(ny)    :: Fsol
        integer,            intent(out)                   :: nfev
        integer,            intent(out)                   :: njev
        integer,            intent(out)                   :: flag
        character(len=120), intent(out)                   :: message

        external fun, jac, callback

        !options
        integer                                     :: maxfev
        double precision                            :: xtol

        !local declarations for HYBRJ
        integer                                     :: mode
        integer                                     :: np
        integer                                     :: l
        integer                                     :: ifail
        integer                                     :: lparaux
        double precision                            :: fa
        double precision, dimension(ny)             :: diag
        double precision, dimension(ny)             :: qtf
        double precision, dimension(ny)             :: w1
        double precision, dimension(ny)             :: w2
        double precision, dimension(ny)             :: w3
        double precision, dimension(ny)             :: w4
        double precision, dimension((ny*(ny+1))/2)  :: r
        double precision, dimension(ny,ny)          :: fjac
        double precision, dimension(npar)           :: paraux

        ! for display
        integer                                     :: i
        CHARACTER(len=120)                          :: LINE
        character(32)                               :: disp

        !
        isFloat_mod = isFloat

        !on les recopie car hybrj peut vouloir modifier par ??
        lparaux = npar
        paraux  = par

        ! set callbacks functions
        if(isFloat)then
            fun_mod_float => fun
            jac_mod_float => jac
            callback_mod_float => callback
        else
            fun_mod => fun
            jac_mod => jac
            callback_mod => callback
        end if

        ! init
        solver_stopped_by_user = .false.

        ! options
        call getOption(options,'Display',disp)
        select case (trim(disp))
            case ('on')
                display_mod = .TRUE.
            case ('off')
                display_mod = .FALSE.
            case default
                CALL printandstop('invalid options.Display: on or off ? ')
        end select
        call getOption(options,'TolX',xtol)
        call getOption(options,'MaxFEval',maxfev)

        !---------------------------------------!
        !--------------- DISPLAY ---------------!
        !---------------------------------------!
!        if(display_mod) then
!            WRITE (LINE,'(a)') '';  call myprint(LINE,.true.)
!            WRITE (LINE,'(a)') '     Calls  |F(x)|                 |x|                    '
!            call myprint(LINE,.true.)
!            WRITE (LINE,'(a)') '';  call myprint(LINE,.true.)
!        end if
        !---------------------------------------!
        !---------------------------------------!

        !HYBRJ solver
        ifail   = -1
        mode    = 1
        fa      = 100d0
        np      = 0
        l       = (ny*(ny+1))/2
        njev    = 0
        nfev    = 0
        w1      = 0d0
        w2      = 0d0
        w3      = 0d0
        w4      = 0d0
        qtf     = 0d0
        Fsol    = 0d0
        diag    = 1d0
        fjac    = 0d0
        r       = 0d0

        xsol    = x0i

        call hybrj(funjac,ny,xsol,paraux,lparaux,Fsol,fjac,ny,xtol,maxfev, &
                    diag,mode,fa,np,ifail,nfev,njev,r,l,qtf,w1,w2,w3,w4)

        flag    = ifail

        !---------------------------------------!
        !--------------- DISPLAY ---------------!
        !---------------------------------------!
        !
        ! l'affichage se fait via des callback maintenant
        ! attention avant on ecrivait un message que si display or on le veut tout le temps
        !
!        if(display_mod) then
!
!            WRITE (LINE,'(a)') '';  call myprint(LINE,.true.)
!            WRITE (LINE,'(a)') ' Results of the nlesolver method: '; call myprint(LINE,.true.);
!
!            WRITE (LINE,'(a)') '';  call myprint(LINE,.true.)
!            WRITE (LINE,'(a)') ' xsol    = ['
!            call myprint(LINE,.false.)
!            DO i = 1, ny
!                if(i.eq.1)then
!                    WRITE (LINE,'(e23.15)') xsol(i)
!                    call myprint(LINE,.true.)
!                elseif(i.eq.ny)then
!                    WRITE (LINE,'("            ",e23.15)') xsol(i)
!                    call myprint(LINE,.false.)
!                else
!                    WRITE (LINE,'("            ",e23.15)') xsol(i)
!                    call myprint(LINE,.true.)
!                end if
!            END DO
!            WRITE (LINE,'(a)') ']'
!            call myprint(LINE,.true.)
!
!            WRITE (LINE,'(a)') '';  call myprint(LINE,.true.)
!            WRITE (LINE,'(a)') ' F(xsol) = ['
!            call myprint(LINE,.false.)
!            DO i = 1, ny
!                if(i.eq.1)then
!                    WRITE (LINE,'(e23.15)') Fsol(i)
!                    call myprint(LINE,.true.)
!                elseif(i.eq.ny)then
!                    WRITE (LINE,'("            ",e23.15)') Fsol(i)
!                    call myprint(LINE,.false.)
!                else
!                    WRITE (LINE,'("            ",e23.15)') Fsol(i)
!                    call myprint(LINE,.true.)
!                end if
!            END DO
!            WRITE (LINE,'(a)') ']'
!            call myprint(LINE,.true.)
!
!            WRITE (LINE,'(a)') '';  call myprint(LINE,.true.)
!            WRITE (LINE,'(" nfev   = ",i0.1)') nfev;    call myprint(LINE,.true.)
!            WRITE (LINE,'(a)') '';  call myprint(LINE,.true.)
!            WRITE (LINE,'(" njev   = ",i0.1)') njev;    call myprint(LINE,.true.)
!            WRITE (LINE,'(a)') '';  call myprint(LINE,.true.)
!            WRITE (LINE,'(" status = ",i0.1)') flag;    call myprint(LINE,.true.)
!            WRITE (LINE,'(a)') '';  call myprint(LINE,.true.)

            if(solver_stopped_by_user)then
!                WRITE (LINE,'(a)') '';  call myprint(LINE,.true.)
!                write(LINE,'(a)') 'Solver stopped by user callback'
!                call myprint(LINE,.true.)
                message = 'solver stopped by user callback'
            else
                if(flag.eq.1)then
!                    WRITE (LINE,'(a)') '';  call myprint(LINE,.true.)
!                    write(LINE,'(a)') 'Successfully completed: relative error between two consecutive'
!                    call myprint(LINE,.true.)
!                    write(LINE,'(a)') '                         iterates is at most TolX.'
                    message = 'Successfully completed: relative error between two consecutive iterates is at most TolX.'
                else
!                    WRITE (LINE,'(a)') '';  call myprint(LINE,.true.)
!                    call myprint('  ||| WARNING: Solver (hybrj)  ->  ',.false.)
                    select case (flag)
                        case (0)
!                            WRITE (LINE,'(a)') 'Improper input parameters.'
                            message = 'Improper input parameters.'
                        !case (1)
                        case (2)
!                            write(LINE,'(a)') 'Number of calls to fun'
!                            call myprint(LINE,.true.)
!                            write(LINE,'(a)') 'with iflag = 1 has reached MaxFEval'
                            message = 'Number of calls to fun has reached MaxFEval'
                        case (3)
!                            write(LINE,'(a)') 'TolX is too small. No further'
!                            call myprint(LINE,.true.)
!                            write(LINE,'(a)') 'improvement in the approximate solution x is possible'
                            message = 'TolX is too small. No further improvement in the approximate solution x is possible.'
                        case (4)
!                            write(LINE,'(a)') 'Iteration is not making good progress,'
!                            call myprint(LINE,.true.)
!                            write(LINE,'(a)') 'as measured by the improvement from the last five Jacobian evaluations'
                            message = 'Iteration is not making good progress, as measured by the improvement from the &
                               & last five Jacobian evaluations'
                        case (5)
!                            write(LINE,'(a)') 'Iteration is not making good progress,'
!                            call myprint(LINE,.true.)
!                            write(LINE,'(a)') 'as measued by the improvement from the last ten iterations'
                            message = 'Iteration is not making good progress, as measued by the improvement from the &
                               & last ten iterations'
                        case default
                            message = ''
                    end select
!                    WRITE (LINE,'(a)') '';  call myprint(LINE,.true.)
                end if
!                call myprint(LINE,.true.)
            end if
!        end if !display
        !---------------------------------------!
        !---------------------------------------!

    end subroutine hybrjint

    !>     @ingroup nlesolvePackage
    !!     @brief   Subroutine given to hybrj. Call sfun or sjac,
    !!              depending on the value iflag.
    !!        @param[in]    ny       State and costate dimension
    !!        @param[in]    ldfjac   Leading dimension of fjac
    !!        @param[in]    y        tate and costate
    !!        @param[in]    lparaux  Number of optional parameters
    !!        @param[in]    paraux   Optional parameters
    !!        @param[in]    nfev     Number of calls to fvec
    !!        @param[in]    ldfjac   leading dimension of fjac
    !!        @param[in]    iflag    if iflag = 1 then call sfun else call sjac
    !!
    !!        @param[out]   fvec     Shooting function value
    !!        @param[out]   fjac     Shooting function jacobian
    !!
    !!  \author Olivier Cots (INP-ENSEEIHT-IRIT)
    !!  \date   2009-2015
    !!  \copyright LGPL
    !!
    Subroutine funjac(ny,y,par,npar,nfev,fvec,fjac,ldfjac,iflag)
!        use utils
!        use defs
        implicit none
        integer,          intent(in)                            :: ny
        integer,          intent(in)                            :: npar
        integer,          intent(in)                            :: ldfjac
        integer,          intent(in)                            :: nfev
        double precision, intent(in), dimension(npar)           :: par
        double precision, intent(in), dimension(ny)             :: y
        double precision, intent(inout), dimension(ny)          :: fvec
        double precision, intent(inout), dimension(ldfjac,ny)   :: fjac
        integer,          intent(inout)                         :: iflag

        !Local variables
        double precision    :: DNRM2
        CHARACTER(len=120)  :: LINE
        integer             :: callback_flag

        ! iflag = 1 : function at y and return fvec. do not alter fjac.
        ! iflag = 2 : jacobian at y and return fjac. do not alter fvec.

        if (iflag == 1) then

            if(isFloat_mod)then ! y is a float

                !Shooting Function evaluation
                fvec = 0d0
                call fun_mod_float(y(1), fvec(1))

                ! callback
                call callback_mod_float(y(1), fvec(1), callback_flag)


            else

                !Shooting Function evaluation
                fvec = 0d0
                call fun_mod(ny, y, fvec)

                ! callback
                call callback_mod(ny, y, fvec, callback_flag)

            end if

            if(callback_flag.lt.0)then
                solver_stopped_by_user = .true.
                iflag = callback_flag
            end if

        elseif(iflag == 2) then

            if(isFloat_mod)then

                !Jacobian evaluation
                fjac = 0d0
                call jac_mod_float(y(1),fjac(1,1))

            else

                !Jacobian evaluation
                fjac = 0d0
                call jac_mod(ny,y,fjac)

            end if

        end if

    end subroutine funjac

end module mod_hybrj
