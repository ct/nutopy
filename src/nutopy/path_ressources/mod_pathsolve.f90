module mod_pathsolve

    implicit none

    double precision, allocatable, dimension(:)     :: sout, dets, normF, ps
    double precision, allocatable, dimension(:,:)   :: parout, xout, viout

    contains

!!********************************************************************
!!
!!     Subroutine pathsolvepy
!!
!>     @ingroup pathsolvePackage
!!     @brief  Interface specific for python of pathsolveint.
!!
!!        @param[in]    nx        Shooting variable dimension
!!        @param[in]    x0        Initial solution, ie "sfun(x0,options,par0) = 0"
!!        @param[in]    nparspan  Number of optional parameters
!!        @param[in]    mparspan  Size of homotopic parameters grid
!!        @param[in]    parspan   Parameters grid
!!        \param[in]    ndw       Size of dw
!!        \param[in]    niw       Size of iw
!!        \param[in]    nsw       Size of lsw
!!        \param[in]    nswInt    Size of swInt
!!        \param[in]    iw        Integer hampath code options
!!        \param[in]    dw        Double precision hampath code options
!!        \param[in]    swInt     Integer hampath code options
!!        \param[in]    lsw       Length of each string option
!!
!!        @param[out]    dimepath    Number of steps
!!        @param[out]    flag        Integration output (should be 1)!!
!!  \author Olivier Cots (INP-ENSEEIHT-IRIT)
!!  \date   2019
!!  \copyright LGPL
!!
    Subroutine pathsolvepy( fun,jac,nx,x0,nparspan,mparspan,parspan,callback,   &
                            dimepath,flag,message,                              &
                            ndw,niw,nsw,nswInt,dw,iw,swInt,lsw)
        use mod_pcam
        implicit none
        integer,            intent(in)                                  :: nx,nparspan,mparspan
        double precision,   intent(in), dimension(nx)                   :: x0
        double precision,   intent(in), dimension(nparspan,mparspan)    :: parspan
        integer,            intent(out)                                 :: dimepath,flag
        character(len=120), intent(out)                                 :: message
        integer,            intent(in)                                  :: ndw, niw, nsw, nswInt
        double precision,   intent(in)                                  :: dw(ndw)
        integer,            intent(in)                                  :: iw(niw), lsw(nsw)
        integer,            intent(in)                                  :: swInt(nswInt)

        external fun, jac, callback

        !local variable
        integer                 :: i
        character(len=32*nsw)   :: sw

        sw = ''
        do i=1,nswInt
            sw(i:i) = char(swInt(i))
        end do

        call pathsolveint(  fun,jac,nx,x0,nparspan,mparspan,parspan,callback,   &
                            dimepath,flag,message,                              &
                            ndw,niw,nsw,dw,iw,sw,lsw)

        if(allocated(parout))then
            deallocate(parout)
        end if

        if(allocated(xout))then
            deallocate(xout)
        end if

        if(allocated(viout))then
            deallocate(viout)
        end if

        if(allocated(sout))then
            deallocate(sout)
        end if

        if(allocated(normF))then
            deallocate(normF)
        end if

        if(allocated(dets))then
            deallocate(dets)
        end if

        if(allocated(ps))then
            deallocate(ps)
        end if

        allocate(parout(nparspan,dimepath),xout(nx,dimepath),viout(nx+1,dimepath))
        allocate(sout(dimepath),dets(dimepath),normF(dimepath),ps(dimepath))

        call getpath(nx,nparspan,dimepath,parout,xout,viout,sout,dets,normF,ps)

   end subroutine pathsolvepy

end module mod_pathsolve

