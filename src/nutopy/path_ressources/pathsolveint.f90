!! -----------------------------------------------------------------------------
!!
!>     @ingroup nlesolvePackage
!!     @brief  Interface for the continuation method.
!!
!!        @param[in]    nx        Shooting variable dimension
!!        @param[in]    x0        Initial solution, ie "sfun(x0,options,par0) = 0"
!!        @param[in]    nparspan  Number of optional parameters
!!        @param[in]    mparspan  Size of homotopic parameters grid
!!        @param[in]    parspan   Parameters grid
!!        \param[in]    ndw       Size of dw
!!        \param[in]    niw       Size of iw
!!        \param[in]    nsw       Size of lsw
!!        \param[in]    iw        Integer hampath code options
!!        \param[in]    dw        Double precision hampath code options
!!        \param[in]    sw        String hampath code options
!!        \param[in]    lsw       Length of each string option
!!
!!        @param[out]    dimepath    Number of steps
!!        @param[out]    flag        Integration output (should be 1)
!!
!!  \author Olivier Cots (INP-ENSEEIHT-IRIT)
!!  \date   2009-2015
!!  \copyright LGPL
!!
Subroutine pathsolveint(fun,jac,nx,x0,nparspan,mparspan,parspan,callback,   &
                            dimepath,flag,message,                          &
                            ndw,niw,nsw,dw,iw,sw,lsw)
    use mod_pcam
    implicit none
    integer,            intent(in)                                  :: nx,nparspan,mparspan
    double precision,   intent(in), dimension(nx)                   :: x0
    double precision,   intent(in), dimension(nparspan,mparspan)    :: parspan
    integer,            intent(out)                                 :: dimepath,flag
    character(len=120), intent(out)                                 :: message
    integer,            intent(in)                                  :: ndw, niw, nsw
    double precision,   intent(in)                                  :: dw(ndw)
    integer,            intent(in)                                  :: iw(niw), lsw(nsw)
    character(32*nsw),  intent(in)                                  :: sw

    external fun, jac, callback

    ! local variables
    type(pcam_options)  :: options

    ! ajouter redirection suivant une option pour la methode : gestion a part

    !Initialize options and call the mere function
    call arraysToPCAMOptions(ndw,niw,nsw,dw,iw,sw,lsw,options)
    call pathfollowing(fun,jac,nx,x0,nparspan,mparspan,parspan,options,callback,dimepath,flag,message)

end subroutine pathsolveint

