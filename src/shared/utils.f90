!! -----------------------------------------------------------------------------
!!
!! Copyright 2016, Olivier Cots.
!!
!! This file is part of HamPath.
!!
!! HamPath is free software: you can redistribute it and/or modify
!! it under the terms of the GNU Lesser General Public License as published by
!! the Free Software Foundation, either version 3 of the License, or
!! (at your option) any later version.
!!
!! HamPath is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU Lesser General Public License for more details.
!!
!! You should have received a copy of the GNU Lesser General Public License
!! along with HamPath. If not, see <http://www.gnu.org/licenses/>.
!!
!! -----------------------------------------------------------------------------
!!
module utils

    implicit none
    contains

    ! ------------------------------------------------------------------------
    ! ------------------------------------------------------------------------
    subroutine interpolationLagrange(neq,ntin,tin,yin,ntout,tout,yout)
        implicit none
        integer, intent(in)             :: ntin, ntout, neq
        double precision, intent(in)    :: tin(ntin), yin(neq,ntin)
        double precision, intent(out)   :: tout(ntout), yout(neq,ntout)

        !local variables
        integer :: i, n, j, k
        double precision :: ti, tj, L(1,ntout)

        !n : degre du polynome
        n = ntin - 1

        yout = 0d0
        do i=1,n+1

            ti = tin(i)
            L  = 1d0
            do j=1,i-1
                tj      = tin(j)
                do k=1,ntout
                    L(1,k)  = L(1,k) * (tout(k) - tj)/(ti - tj)
                end do
            end do
            do j=i+1,n+1
                tj = tin(j)
                do k=1,ntout
                    L(1,k)  = L(1,k) * (tout(k) - tj)/(ti - tj)
                end do
            end do

            call DGEMM('N','N',neq,ntout,1,1d0,yin(:,i),neq,L,1,1d0,yout,neq)

        end do

    end subroutine interpolationLagrange


!! -----------------------------------------------------------------------------
!!
!>     \ingroup commonUtils
!!     \brief   Kronecker product C = A x B.
!!       \param[in]  A           dimension(ma,na)
!!       \param[in]  B           dimension(mb,nb)
!!       \param[out] C           dimension(ma*mb,na*nb)
!!
!!  \author Olivier Cots
!!  \date   2014
!!  \copyright LGPL
!!
subroutine kronecker(ma,na,A,mb,nb,B,C)
    implicit none
    integer, intent(in)             :: ma, na, mb, nb
    double precision, intent(in)    :: A(ma,na), B(mb,nb)
    double precision, intent(out)   :: C(ma*mb,na*nb)

    !local variables
    integer :: i, j

    do j=1,na
        do i=1,ma

            C((i-1)*mb+1:(i-1)*mb+mb,(j-1)*nb+1:(j-1)*nb+nb) = A(i,j) * B

        end do
    end do


end subroutine kronecker

!! -----------------------------------------------------------------------------
!!
!>     \ingroup commonUtils
!!     \brief   Print a matrix to the terminal.
!!       \param[in] nom         name of the matrix to print
!!       \param[in] A           matrix to print
!!       \param[in] m           number of rows of A
!!       \param[in] n           number of columns of A
!!
!!  \author Olivier Cots
!!  \date   2009-2014
!!  \copyright LGPL
!!
    subroutine printMatrice(nom,A,m,n)
        implicit none
        CHARACTER(len=*),   intent(in)  :: nom
        integer,            intent(in)  :: m, n
        double precision,   intent(in)  :: A(m,n)

        integer :: i, j
        CHARACTER(len=120)  :: LINE

        WRITE (LINE,'(a)') '';  call myprint(LINE,.true.)
        WRITE (LINE,'(a)') trim(nom)    ; call myprint(LINE,.false.)
        WRITE (LINE,'(a)') ' = ['       ; call myprint(LINE,.false.)
        DO i = 1, m

            DO j=1,n
                if(j.eq.1)then
                    if(i.eq.1)then
                        WRITE (LINE,'(e23.15)') A(i,j)
                    else
                        WRITE (LINE,'("          ",e23.15)') A(i,j)
                    end if
                    call myprint(LINE,.false.)
                elseif(j.eq.n)then
                    if(i.eq.m)then
                        WRITE (LINE,'(e23.15)') A(i,j)
                        call myprint(LINE,.false.)
                    else
                        WRITE (LINE,'(e23.15)') A(i,j)
                        call myprint(LINE,.true.)
                    end if
                else
                    WRITE (LINE,'(e23.15)') A(i,j)
                    call myprint(LINE,.false.)
                end if
            END DO

        END DO
        WRITE (LINE,'(a)') ']'
        call myprint(LINE,.true.)
        !WRITE (LINE,'(a)') '';  call myprint(LINE,.true.)

    end subroutine printMatrice

!! -----------------------------------------------------------------------------
!!
!>     \ingroup commonUtils
!!     \brief   Print a vector to the terminal.
!!       \param[in] nom         name of the vector to print
!!       \param[in] v           vector to print
!!       \param[in] n           number of elements of v
!!
!!  \author Olivier Cots
!!  \date   2009-2014
!!  \copyright LGPL
!!
    subroutine printVector(nom,v,n)
        implicit none
        CHARACTER(len=*),   intent(in)  :: nom
        integer,            intent(in)  :: n
        double precision,   intent(in)  :: v(n)

        integer :: i
        CHARACTER(len=120)  :: LINE

        WRITE (LINE,'(a)') '';  call myprint(LINE,.true.)
        WRITE (LINE,'(a)') trim(nom)    ; call myprint(LINE,.false.)
        WRITE (LINE,'(a)') ' = ['       ; call myprint(LINE,.false.)

        DO i=1,n
            if(i.eq.1)then
                WRITE (LINE,'(e23.15)') v(i)
            else
                WRITE (LINE,'("          ",e23.15)') v(i)
            end if
            call myprint(LINE,.false.)
        END DO

        WRITE (LINE,'(a)') ']'
        call myprint(LINE,.true.)
        !WRITE (LINE,'(a)') '';  call myprint(LINE,.true.)

    end subroutine printVector

!! -----------------------------------------------------------------------------
!!
!>     \ingroup commonUtils
!!     \brief   Write an integer in a file, well formatted for matlab.
!!       \param[in] inte          data to print
!!       \param[in] unitfileout   unit file for writing
!!       \param[in] namevec       the name of the variable corresponding to inte
!!
!!  \author Olivier Cots
!!  \date   2009-2014
!!  \copyright LGPL
!!
    subroutine writeIntegerMatlab(inte,namevec,unitfileout)
        implicit none
        integer         , intent(in) :: inte,unitfileout
        character(len=*), intent(in) :: namevec

        !local variables
        CHARACTER(len=120)  :: LINE

        WRITE (LINE,*) trim(namevec),' = '
        WRITE (unit=unitfileout,fmt='(a)'   ,advance='no') trim(LINE)
        WRITE (unit=unitfileout,fmt='(i0.1)',advance='no') inte
        WRITE (unit=unitfileout,fmt='(a)'   ,advance='yes') ';'

    end subroutine writeIntegerMatlab

!! -----------------------------------------------------------------------------
!!
!>     \ingroup commonUtils
!!     \brief   Write a row vector in a file, well formatted for matlab.
!!       \param[in] vec           datas to print
!!       \param[in] n             number of elements of vec
!!       \param[in] unitfileout   unit file for writing
!!       \param[in] namevec       the name of the variable corresponding to vec
!!
!!  \author Olivier Cots
!!  \date   2009-2014
!!  \copyright LGPL
!!
    subroutine writeRowVectorDbleMatlab(vec,n,namevec,unitfileout)
        implicit none
        integer         , intent(in) :: n,unitfileout
        double precision, dimension(n), intent(in) :: vec
        character(len=*), intent(in) :: namevec

        !local variables
        CHARACTER(len=120)  :: LINE
        integer :: i

        WRITE (LINE,*) trim(namevec),' = ['
        WRITE (unit=unitfileout,fmt='(a)',advance='no') trim(LINE)
        do i=1,n
            WRITE (unit=unitfileout,fmt='(e23.15)',advance='no') vec(i)
        end do
        WRITE (unit=unitfileout,fmt='(a)',advance='yes') '];'

    end subroutine writeRowVectorDbleMatlab

!! -----------------------------------------------------------------------------
!!
!>     \ingroup commonUtils
!!     \brief   Write a column vector in a file, well formatted for matlab.
!!       \param[in] vec           datas to print
!!       \param[in] n             number of elements of vec
!!       \param[in] unitfileout   unit file for writing
!!       \param[in] namevec       the name of the variable corresponding to vec
!!
!!  \author Olivier Cots
!!  \date   2009-2014
!!  \copyright LGPL
!!
    subroutine writeColVectorDbleMatlab(vec,n,namevec,unitfileout)
        implicit none
        integer         , intent(in) :: n,unitfileout
        double precision, dimension(n), intent(in) :: vec
        character(len=*), intent(in) :: namevec

        !local variables
        CHARACTER(len=120)  :: LINE
        integer :: i

        WRITE (LINE,*) trim(namevec),' = ['
        WRITE (unit=unitfileout,fmt='(a)',advance='no') trim(LINE)
        do i=1,n
            if(i.eq.n)THEN
                WRITE (unit=unitfileout,fmt='(e23.15)',advance='no') vec(i)
            ELSE
                WRITE (unit=unitfileout,fmt='(e23.15)',advance='no') vec(i)
                WRITE (unit=unitfileout,fmt='(a)',advance='yes') ';'
            end if
        end do
        WRITE (unit=unitfileout,fmt='(a)',advance='yes') '];'

    end subroutine writeColVectorDbleMatlab

!! -----------------------------------------------------------------------------
!!
!>     \ingroup commonUtils
!!     \brief   Write a matrix in a file, well formatted for matlab.
!!       \param[in] mat           datas to print
!!       \param[in] m             number of rows of mat
!!       \param[in] n             number of column of mat
!!       \param[in] unitfileout   unit file for writing
!!       \param[in] namemat       the name of the variable corresponding to mat
!!
!!  \author Olivier Cots
!!  \date   2009-2014
!!  \copyright LGPL
!!
    subroutine writeMatrixDbleMatlab(mat,m,n,namemat,unitfileout)
        implicit none
        integer         , intent(in) :: m,n,unitfileout
        double precision, dimension(m,n), intent(in) :: mat
        character(len=*), intent(in) :: namemat

        !local variables
        CHARACTER(len=120)  :: LINE
        integer :: i,j

        WRITE (LINE,*) trim(namemat),' = ['
        WRITE (unit=unitfileout,fmt='(a)',advance='no') trim(LINE)
        do i=1,m
            do j=1,n
                if(j.eq.n)THEN
                    if(i.eq.m)THEN
                        WRITE (unit=unitfileout,fmt='(e23.15)',advance='no') mat(i,j)
                    ELSE
                        WRITE (unit=unitfileout,fmt='(e23.15)',advance='no') mat(i,j)
                        WRITE (unit=unitfileout,fmt='(a)',advance='yes') ';'
                    end IF
                ELSE
                    WRITE (unit=unitfileout,fmt='(e23.15)',advance='no') mat(i,j)
                end if
            end do
        end do
        WRITE (unit=unitfileout,fmt='(a)',advance='yes') '];'

    end subroutine writeMatrixDbleMatlab

end module utils
