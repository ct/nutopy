module mod_integration

    use String_Utility
    use mod_gest_liste
    use utils
    implicit none

    ! ------------------------------------------------------------------------
    ! ------------------------------------------------------------------------
!    logical, private               :: IsFirstDisplay_mod      != .true

    ! ------------------------------------------------------------------------
    ! ------------------------------------------------------------------------
!!     \param[in]       fun         RHS
!!     \param[in]       dfun        Jacobian of the RHS
!!     \param[in]       choixsolout id giving the method which has called dopri5int, see \ref globalVariables
!!     \param[in]       solout      auxiliary function called after accepted steps
!!     \param[in]       phidezV     non linear equations to solve (implicit scheme)
!!     \param[in]       phidphidezV redirect to phidez or dphidez
    type integration_parameters
        procedure(proc_fun),     pointer, nopass :: fun
        procedure(proc_dfun),    pointer, nopass :: dfun
        procedure(proc_phidez),  pointer, nopass :: phidez
        procedure(proc_pdpdez),  pointer, nopass :: pdpdez
        procedure(proc_solout),  pointer, nopass :: solout
!        integer                                  :: choixsolout = -1
        character(32)                            :: irki                = '2'
        character(32)                            :: irks                = 'newton'
        character(32)                            :: ParentMethodName    = ''
        character(32)                            :: ODESolver           = 'dopri5'
        type(TLISTE)                             :: IntegrationList
        integer                                  :: MaxSteps            = 100000
        double precision                         :: TolAbs              = 1d-10
        double precision                         :: TolRel              = 1d-8
        double precision                         :: MaxStepSize         = -1d0
        logical                                  :: Display             = .true.
        !logical                                  :: IsFirstDisplay      = .true.
        logical                                  :: UseSolout           = .true.
        logical                                  :: UseRadauHampath     = .false.
    end type integration_parameters

    interface getIntegrationParameter
        module procedure getRKInt, getRKReal, getRKChar, getRKBool, getRKList
    end interface

    interface setIntegrationParameter
        module procedure setRKInt, setRKReal, setRKChar, setRKBool, setRKList
    end interface

    abstract interface
    Subroutine proc_fun(neq,time,y,val,funpar,lfunpar)
        implicit none
        integer,           intent(in)                      :: neq
        integer,           intent(in)                      :: lfunpar
        double precision,  intent(in)                      :: time
        double precision,  intent(in),  dimension(lfunpar) :: funpar
        double precision,  intent(in),  dimension(neq)     :: y
        double precision,  intent(out), dimension(neq)     :: val
    end Subroutine proc_fun
    end interface

    abstract interface
    Subroutine proc_dfun(nz,t,z,val,paraux,lparaux)
        implicit none
        integer,            intent(in)                      :: nz
        integer,            intent(in)                      :: lparaux
        double precision,   intent(in)                      :: t
        double precision,   intent(in),  dimension(lparaux) :: paraux
        double precision,   intent(in),  dimension(nz)      :: z
        double precision,   intent(out), dimension(nz,nz)   :: val
    end Subroutine proc_dfun
    end interface

    abstract interface
    subroutine proc_phidez(nz, Zc, fvec, iflag, fpar, lfpar)
        implicit none
        integer,            intent(in)                          :: nz, lfpar
        double precision,   intent(in),  dimension(nz)          :: Zc
        double precision,   intent(out), dimension(nz)          :: fvec
        integer,            intent(inout)                       :: iflag
        double precision,   intent(inout),  dimension(lfpar)    :: fpar
    end subroutine proc_phidez
    end interface

    abstract interface
    subroutine proc_pdpdez(nz,Zc,fpar,lfpar,fvec,fjac,ldfjac,iflag)
        implicit none
        integer,          intent(in)                        :: nz
        integer,          intent(in)                        :: lfpar
        integer,          intent(in)                        :: ldfjac
        double precision, intent(inout), dimension(lfpar)   :: fpar
        double precision, intent(in), dimension(nz)         :: Zc
        double precision, intent(out), dimension(nz)        :: fvec
        double precision, intent(out), dimension(ldfjac,nz) :: fjac
        integer,          intent(inout)                     :: iflag
    end subroutine proc_pdpdez
    end interface

    abstract interface
    subroutine proc_solout(Niter,told,time,y,neq,dpar,ldpar,ipar,lipar,fun,funpar,lfunpar,irtrn)
        implicit none
        integer,            intent(in)                         :: Niter
        double precision,   intent(in)                         :: told
        double precision,   intent(inout)                      :: time
        integer,            intent(in)                         :: neq
        integer,            intent(in)                         :: ldpar
        integer,            intent(in)                         :: lipar
        integer,            intent(in)                         :: lfunpar
        double precision,   intent(inout), dimension(neq)      :: y
        double precision,   intent(in),    dimension(ldpar)    :: dpar
        integer,            intent(in),    dimension(lipar)    :: ipar
        double precision,   intent(in),    dimension(lfunpar)  :: funpar
        integer,            intent(inout)                      :: irtrn
        interface
        Subroutine fun(neq,time,y,val,funpar,lfunpar)
            implicit none
            integer,           intent(in)                      :: neq
            integer,           intent(in)                      :: lfunpar
            double precision,  intent(in)                      :: time
            double precision,  intent(in),  dimension(lfunpar) :: funpar
            double precision,  intent(in),  dimension(neq)     :: y
            double precision,  intent(out), dimension(neq)     :: val
        end Subroutine fun
        end interface
    end subroutine proc_solout
    end interface

    ! ------------------------------------------------------------------------
    ! ------------------------------------------------------------------------
    type TButcherTable
        !>  @ingroup commonLists
        !! \memberof modgestbutchertable::tbutchertable
        double precision, dimension(:,:), pointer :: A => null()
        !>  @ingroup commonLists
        !! \memberof modgestbutchertable::tbutchertable
        double precision, dimension(:),   pointer :: b => null()
        !>  @ingroup commonLists
        !! \memberof modgestbutchertable::tbutchertable
        double precision, dimension(:),   pointer :: c => null()
        !>  @ingroup commonLists
        !! \memberof modgestbutchertable::tbutchertable
        integer                                   :: s = 0
        !>  @ingroup commonLists
        !! \memberof modgestbutchertable::tbutchertable
        integer                                   :: ordre = 0
        !>  @ingroup commonLists
        !! \memberof modgestbutchertable::tbutchertable
        character(8)                              :: infos
    end type TButcherTable

    contains

    ! ------------------------------------------------------------------------
    ! ------------------------------------------------------------------------
    !
    ! PARAMETERS
    !
    ! ------------------------------------------------------------------------
    ! ------------------------------------------------------------------------

!
! GETTERS
!

    ! ------------------------------------------------------------------------
    ! ------------------------------------------------------------------------
    subroutine getRKList(params,optName,optValue)
        type(integration_parameters),   intent(in)  :: params
        character(LEN=*),               intent(in)  :: optName
        type(TLISTE),                   intent(out) :: optValue

        CHARACTER(len=120)  :: LINE
        select case (trim(optName))
            case ('IntegrationList')
                optValue = params%IntegrationList
            case default
            WRITE (LINE,*) 'Invalid parameter: ', trim(optName), '!'
            CALL printandstop(LINE)
        end select
    end subroutine getRKList

    ! ------------------------------------------------------------------------
    ! ------------------------------------------------------------------------
    subroutine getRKInt(params,optName,optValue)
        type(integration_parameters),   intent(in)  :: params
        character(LEN=*),               intent(in)  :: optName
        integer,                        intent(out) :: optValue

        CHARACTER(len=120)  :: LINE
        select case (trim(optName))
            case ('MaxSteps')
                optValue = params%MaxSteps
            case default
            WRITE (LINE,*) 'Invalid parameter: ', trim(optName), ' or value: ', optValue, '!'
            CALL printandstop(LINE)
        end select
    end subroutine getRKInt

    ! ------------------------------------------------------------------------
    ! ------------------------------------------------------------------------
    subroutine getRKReal(params,optName,optValue)
        type(integration_parameters),   intent(in)  :: params
        character(LEN=*),               intent(in)  :: optName
        double precision,               intent(out) :: optValue

        CHARACTER(len=120)  :: LINE
        select case (trim(optName))
            case ('TolAbs')
                optValue = params%TolAbs
            case ('TolRel')
                optValue = params%TolRel
            case ('MaxStepSize')
                optValue = params%MaxStepSize
            case default
            WRITE (LINE,*) 'Invalid parameter: ', trim(optName), ' or value: ', optValue, '!'
            CALL printandstop(LINE)
        end select
    end subroutine getRKReal

    ! ------------------------------------------------------------------------
    ! ------------------------------------------------------------------------
    subroutine getRKChar(params,optName,optValue)
        type(integration_parameters),   intent(in)  :: params
        character(LEN=*),               intent(in)  :: optName
        character(LEN=*),               intent(out) :: optValue

        CHARACTER(len=120)  :: LINE
        select case (trim(optName))
            case ('irki')
                optValue = params%irki
            case ('irks')
                optValue = params%irks
            case ('ParentMethodName')
                optValue = params%ParentMethodName
            case ('ODESolver')
                optValue = params%ODESolver
            case default
            WRITE (LINE,*) 'Invalid parameter: ', trim(optName), ' or value: ', optValue, '!'
            CALL printandstop(LINE)
        end select
    end subroutine getRKChar

    ! ------------------------------------------------------------------------
    ! ------------------------------------------------------------------------
    subroutine getRKBool(params,optName,optValue)
        type(integration_parameters),   intent(in)  :: params
        character(LEN=*),               intent(in)  :: optName
        logical,                        intent(out) :: optValue

        CHARACTER(len=120)  :: LINE
        select case (trim(optName))
            case ('Display')
                optValue = params%Display
            case ('UseSolout')
                optValue = params%UseSolout
            case ('UseRadauHampath')
                optValue = params%UseRadauHampath
            case default
            WRITE (LINE,*) 'Invalid parameter: ', trim(optName), ' or value: ', optValue, '!'
            CALL printandstop(LINE)
        end select
    end subroutine getRKBool

!
! SETTERS
!

    ! ------------------------------------------------------------------------
    ! ------------------------------------------------------------------------
    subroutine setRKList(params,optName,optValue)
        type(integration_parameters),   intent(inout)  :: params
        character(LEN=*),               intent(in)  :: optName
        type(TLISTE),                   intent(in) :: optValue

        CHARACTER(len=120)  :: LINE
        select case (trim(optName))
            case ('IntegrationList')
                params%IntegrationList = optValue
            case default
            WRITE (LINE,*) 'Invalid parameter: ', trim(optName), '!'
            CALL printandstop(LINE)
        end select
    end subroutine setRKList

    ! ------------------------------------------------------------------------
    ! ------------------------------------------------------------------------
    subroutine setRKInt(params,optName,optValue)
        type(integration_parameters),   intent(inout)  :: params
        character(LEN=*),               intent(in)  :: optName
        integer,                        intent(in) :: optValue

        CHARACTER(len=120)  :: LINE
        select case (trim(optName))
            case ('MaxSteps')
                params%MaxSteps = optValue
            case default
            WRITE (LINE,*) 'Invalid parameter: ', trim(optName), ' or value: ', optValue, '!'
            CALL printandstop(LINE)
        end select
    end subroutine setRKInt

    ! ------------------------------------------------------------------------
    ! ------------------------------------------------------------------------
    subroutine setRKReal(params,optName,optValue)
        type(integration_parameters),   intent(inout)  :: params
        character(LEN=*),               intent(in)  :: optName
        double precision,               intent(in) :: optValue

        CHARACTER(len=120)  :: LINE
        select case (trim(optName))
            case ('TolAbs')
                params%TolAbs = optValue
            case ('TolRel')
                params%TolRel = optValue
            case ('MaxStepSize')
                params%MaxStepSize = optValue
            case default
            WRITE (LINE,*) 'Invalid parameter: ', trim(optName), ' or value: ', optValue, '!'
            CALL printandstop(LINE)
        end select
    end subroutine setRKReal

    ! ------------------------------------------------------------------------
    ! ------------------------------------------------------------------------
    subroutine setRKChar(params,optName,optValue)
        type(integration_parameters),   intent(inout)  :: params
        character(LEN=*),               intent(in)  :: optName
        character(LEN=*),               intent(in) :: optValue

        CHARACTER(len=120)  :: LINE
        select case (trim(optName))
            case ('irki')
                params%irki = optValue
            case ('irks')
                params%irks = optValue
            case ('ParentMethodName')
                params%ParentMethodName = optValue
            case ('ODESolver')
                params%ODESolver = optValue
            case default
            WRITE (LINE,*) 'Invalid parameter: ', trim(optName), ' or value: ', optValue, '!'
            CALL printandstop(LINE)
        end select
    end subroutine setRKChar

    ! ------------------------------------------------------------------------
    ! ------------------------------------------------------------------------
    subroutine setRKBool(params,optName,optValue)
        type(integration_parameters),   intent(inout)  :: params
        character(LEN=*),               intent(in)  :: optName
        logical,                        intent(in) :: optValue

        CHARACTER(len=120)  :: LINE
        select case (trim(optName))
            case ('Display')
                params%Display = optValue
            case ('UseSolout')
                params%UseSolout = optValue
            case ('UseRadauHampath')
                params%UseRadauHampath = optValue
            case default
            WRITE (LINE,*) 'Invalid parameter: ', trim(optName), ' or value: ', optValue, '!'
            CALL printandstop(LINE)
        end select
    end subroutine setRKBool

    ! ------------------------------------------------------------------------
    ! ------------------------------------------------------------------------
    !
    ! MERE FUNCTION
    !
    ! ------------------------------------------------------------------------
    ! ------------------------------------------------------------------------

    ! ------------------------------------------------------------------------
    ! ------------------------------------------------------------------------
    subroutine integration(spec,neq,x0,funpar,lfunpar,ncpas,tin,tout,xf,ninfos,infos)
        implicit none
        type(integration_parameters),    intent(in)                      :: spec
        integer,            intent(in)                      :: lfunpar
        integer,            intent(in)                      :: neq
        integer,            intent(in)                      :: ncpas
        integer,            intent(in)                      :: ninfos
        double precision,   intent(inout)                   :: tin
        double precision,   intent(inout)                   :: tout
        double precision,   intent(in),  dimension(neq)     :: x0
        double precision,   intent(in),  dimension(lfunpar) :: funpar
        integer,            intent(out), dimension(ninfos)  :: infos
        double precision,   intent(out), dimension(neq)     :: xf

        !local variables
        CHARACTER(len=120)  :: LINE
        character(8)        :: infosBT
        type(TButcherTable) :: BTable
        integer             :: flag, nfev, dummyInt(1)
        double precision    :: eps, dummyDbl(1)

        procedure(),  pointer :: fun
        procedure(),  pointer :: dfun
        procedure(),  pointer :: phidez
        procedure(),  pointer :: pdpdez
        procedure(),  pointer :: solout

        ! passage des parametres en arguments
        character(32)       :: irki                 != 'nodef'
        character(32)       :: irks                 != 'nodef'
        character(32)       :: ParentMethodName     != ''
        character(32)       :: ODESolver            != 'nodef'
        type(TLISTE)        :: IntegrationList
        logical             :: Display              != .true.
        integer             :: MaxSteps             != -1
        double precision    :: TolAbs               != -1d0
        double precision    :: TolRel               != -1d0
        double precision    :: MaxStepSize          != -1d0
        logical             :: UseSolout            != .true.
        logical             :: UseRadauHampath      != .false.

        infos = 0d0
        xf = 0d0

        ! on initialise les parametres : faire avec des setters plus tard
        fun                 => spec%fun
        dfun                => spec%dfun
        phidez              => spec%phidez
        pdpdez              => spec%pdpdez
        solout              => spec%solout

        call getIntegrationParameter(spec, 'irki',              irki)
        call getIntegrationParameter(spec, 'irks',              irks)
        call getIntegrationParameter(spec, 'ParentMethodName',  ParentMethodName)
        call getIntegrationParameter(spec, 'ODESolver',         ODESolver)

        !
        call getIntegrationParameter(spec, 'IntegrationList',   IntegrationList)

        call getIntegrationParameter(spec, 'Display',           Display)
        call getIntegrationParameter(spec, 'UseSolout',         UseSolout)
        call getIntegrationParameter(spec, 'UseRadauHampath',   UseRadauHampath)

        call getIntegrationParameter(spec, 'MaxSteps',          MaxSteps)
        call getIntegrationParameter(spec, 'TolAbs',            TolAbs)
        call getIntegrationParameter(spec, 'TolRel',            TolRel)
        call getIntegrationParameter(spec, 'MaxStepSize',       MaxStepSize)

!        IsFirstDisplay_mod      =  .true.

        !
        flag = 0
        nfev = 0

        eps  = 10d0*epsilon(1d0)
        if(abs(tout-tin).le.eps) then

            xf      = x0
            flag    = 0
            call solout(1,tout,tout,x0,neq,dummyDbl,0,dummyInt,0,fun,funpar,lfunpar,flag)
            call solout(2,tin,tout,xf,neq,dummyDbl,0,dummyInt,0,fun,funpar,lfunpar,flag)

        else

            select case (ODESolver)

                case ('dopri5')

                    !!Dormand Prince 5th order (dopri5)
                    call dopri5Int(fun,neq,x0,funpar,lfunpar,ncpas,tin,tout,xf,flag,nfev,solout,&
                        MaxSteps,TolAbs,TolRel,MaxStepSize,UseSolout)

                case ('dop853')

                    !!Dormand Prince 8th order (dop853)
                    call dop853Int(fun,neq,x0,funpar,lfunpar,ncpas,tin,tout,xf,flag,nfev,solout,&
                        MaxSteps,TolAbs,TolRel,MaxStepSize,UseSolout)

                case ('radau5')

                    call radauInt(fun,dfun,neq,x0,funpar,lfunpar,ncpas,tin,tout,xf,flag,nfev,solout,3,3,3,&
                        MaxSteps,TolAbs,TolRel,MaxStepSize,UseSolout,UseRadauHampath)

                case ('radau9')

                    call radauInt(fun,dfun,neq,x0,funpar,lfunpar,ncpas,tin,tout,xf,flag,nfev,solout,5,5,5,&
                        MaxSteps,TolAbs,TolRel,MaxStepSize,UseSolout,UseRadauHampath)

                case ('radau13')

                call radauInt(fun,dfun,neq,x0,funpar,lfunpar,ncpas,tin,tout,xf,flag,nfev,solout,7,7,7,&
                        MaxSteps,TolAbs,TolRel,MaxStepSize,UseSolout,UseRadauHampath)

                case ('radau')

                    call radauInt(fun,dfun,neq,x0,funpar,lfunpar,ncpas,tin,tout,xf,flag,nfev,solout,3,7,3,&
                        MaxSteps,TolAbs,TolRel,MaxStepSize,UseSolout,UseRadauHampath)

                case default

                    call getButcherTab(ODESolver,BTable,infosBT)

                    !On affiche un warning si la grille d'intégration est réduite qu'à [t0 tf]
                    !if(Display .and. IsFirstDisplay_mod .and. LONGUEUR(IntegrationList).eq.2) then
                    if(Display .and. LONGUEUR(IntegrationList).eq.2) then
                        WRITE (LINE,'(a)') 'Warning: fixed-step integrator is used with only one step! tspan = [t0 tf]!';
                        call myprint(LINE,.true.)
                    end if

                    select case (infosBT)

                        case ('explicit')

                            call integExp(fun,neq,x0,funpar,lfunpar,tin,xf,flag,nfev,solout,BTable,IntegrationList,&
                                Display)

                        case ('implicit')

                            call integImp(fun,neq,x0,funpar,lfunpar,tin,xf,flag,nfev,solout,BTable,&
                                phidez,pdpdez,irki,irks,IntegrationList,Display)

                        case default

                            CALL printandstop('  ||| ERROR: Integrate -> Unknown integrator ' // ODESolver)

                    end select

                    call destroyButcherTable(BTable)

            end select

        end if

        infos(1) = flag
        if(ninfos.ge.2) then
            infos(2) = nfev
        end if
        !WRITE (LINE,'("nfev=      ",i0.1)') nfev; call myprint(LINE,.true.)

        if(Display) then

            if(flag.lt.0)then
                WRITE (LINE,'(a)') '';                  call myprint(LINE,.true.)
                WRITE (LINE,'(a)') '  ||| WARNING: ';   call myprint(LINE,.false.)
                WRITE (LINE,'(a)') ParentMethodName;call myprint(LINE,.false.)
                WRITE (LINE,'(a)') ' Integration (';    call myprint(LINE,.false.)
                WRITE (LINE,'(a)') ODESolver;       call myprint(LINE,.false.)
            end if

            select case (flag)
                case (-1)
                    WRITE (LINE,'(a)') ')  ->  INPUT IS NOT CONSISTENT '
                case (-2)
                    WRITE (LINE,'(a)') ')  ->  LARGER MAXSTEPS IS NEEDED '
                case (-3)
                    WRITE (LINE,'(a)') ')  ->  STEP SIZE BECOMES TOO SMALL '
                case (-4)
                    select case (ODESolver)
                        case ('dopri5','dop853')
                            WRITE (LINE,'(a)') ')  ->  PROBLEM IS PROBABLY STIFF (INTERRUPTED) '
                        case ('radau5','radau9','radau13','radau')
                            WRITE (LINE,'(a)') ')  ->  MATRIX IS REPEATEDLY SINGULAR '
                        case default
                    end select
                case (-5)
                    WRITE (LINE,'(a)') ')  ->  |F| BECOME TOO BIG '
                case (-6)
                    WRITE (LINE,'(a)') ')  ->  STEP SIZE BECOMES TO SMALL '
                case (-7)
                    WRITE (LINE,'(a)') ')  ->  STOPS AFTER A TURNING POINT '
                case (-8)
                    WRITE (LINE,'(a)') ')  ->  ITERATIONS REACHED TO SOLVE NLE OF IRK METHOD '
            end select

            if(flag.lt.-10)then
                    WRITE (LINE,'(a)') ')  ->  STOPS BECAUSE OF USER IMPLEMENTATION OF MFUN! '
            end if

            if(flag.lt.0)then
                call myprint(LINE,.true.)
            end if

        end if

    end subroutine integration

    ! ------------------------------------------------------------------------
    ! ------------------------------------------------------------------------
    !
    ! DENSE OUTPUT
    !
    ! ------------------------------------------------------------------------
    ! ------------------------------------------------------------------------

    ! ------------------------------------------------------------------------
    ! ------------------------------------------------------------------------
    subroutine denseOutput(nameinte,i,t,y,told,time,hstep,lipar,ipar,ldpar,dpar)
        implicit none
        character(32),      intent(in)                         :: nameinte
        integer,            intent(in)                         :: lipar,ldpar,i
        integer,            intent(in),    dimension(lipar)    :: ipar
        double precision,   intent(in)                         :: t,told,time,hstep
        double precision,   intent(out)                        :: y
        double precision,   intent(in),    dimension(ldpar)    :: dpar

        !local variables
        double precision :: CONTD5, CONTD8, CONTRA, CONTRAHAMPATH

        select case (nameinte)
            case ('dopri5')
                y = CONTD5(i,t,told,time,hstep,dpar,ldpar,ipar,lipar)
            !case ('radau5old')
            !    y = CONTR5(i,t,told,time,hstep,dpar,ldpar,ipar,lipar)
            case ('dop853')
                y = CONTD8(i,t,told,time,hstep,dpar,ldpar,ipar,lipar)
            case ('radau5','radau9','radau13','radau')
                y = CONTRA(i,t,told,time,hstep,dpar,ldpar,ipar,lipar)
            case ('radau5Hampath','radau9Hampath','radau13Hampath','radauHampath')
                y = CONTRAHAMPATH(i,t,told,time,hstep,dpar,ldpar,ipar,lipar)
            case default
                CALL printandstop('  ||| ERROR: Integrate -> Unknown integrator...')
        end select

    end subroutine denseOutput


    ! ------------------------------------------------------------------------
    ! ------------------------------------------------------------------------
    !
    ! INTERFACE TO RUNGE-KUTTA SOLVERS
    !
    ! ------------------------------------------------------------------------
    ! ------------------------------------------------------------------------

    ! ------------------------------------------------------------------------
    ! ------------------------------------------------------------------------
    subroutine dopri5Int(fun,neq,x0,funpar,lfunpar,ncpas,tin,tout,xf,flag,nfev,solout,&
                        MaxSteps,TolAbs,TolRel,MaxStepSize,UseSolout)
        implicit none
        integer,            intent(in)                          :: lfunpar
        integer,            intent(in)                          :: neq
        integer,            intent(in)                          :: ncpas
        double precision,   intent(inout)                       :: tin
        double precision,   intent(inout)                       :: tout
        double precision,   intent(in),  dimension(neq)         :: x0
        double precision,   intent(in),  dimension(lfunpar)     :: funpar
        double precision,   intent(out), dimension(neq)         :: xf
        integer,            intent(out)                         :: flag, nfev
        integer,            intent(in)                          :: MaxSteps
        double precision,   intent(in)                          :: TolAbs
        double precision,   intent(in)                          :: TolRel
        double precision,   intent(in)                          :: MaxStepSize
        logical,            intent(in)                          :: UseSolout

        interface
        Subroutine solout(Niter,told,time,y,neq,dpar,ldpar,ipar,lipar,fun,funpar,lfunpar,irtrn)
            integer,            intent(in)                         :: Niter
            double precision,   intent(in)                         :: told
            double precision,   intent(inout)                      :: time
            double precision,   intent(inout), dimension(neq)      :: y
            integer,            intent(in)                         :: neq
            double precision,   intent(in),    dimension(ldpar)     :: dpar
            integer,            intent(in)                         :: ldpar
            integer,            intent(in),    dimension(lipar)    :: ipar
            integer,            intent(in)                         :: lipar
            double precision,   intent(in),    dimension(lfunpar)  :: funpar
            integer,            intent(in)                         :: lfunpar
            integer,            intent(inout)                      :: irtrn
            interface
                Subroutine fun(neq,time,y,value,funpar,lfunpar)
                    implicit none
                    integer,           intent(in)                      :: neq
                    integer,           intent(in)                      :: lfunpar
                    double precision,  intent(in)                      :: time
                    double precision,  intent(in),  dimension(lfunpar) :: funpar
                    double precision,  intent(in),  dimension(neq)     :: y
                    double precision,  intent(out), dimension(neq)     :: value
                end Subroutine fun
            end interface
        end Subroutine solout
        end interface

        interface
        Subroutine fun(neq,time,y,value,funpar,lfunpar)
            implicit none
            integer,           intent(in)                      :: neq
            integer,           intent(in)                      :: lfunpar
            double precision,  intent(in)                      :: time
            double precision,  intent(in),  dimension(lfunpar) :: funpar
            double precision,  intent(in),  dimension(neq)     :: y
            double precision,  intent(out), dimension(neq)     :: value
        end Subroutine fun
        end interface

        !local variables
        integer                                     :: iout
        integer                                     :: itol
        integer                                     :: lw
        integer                                     :: liw
        integer                                     :: NRDENS
        double precision                            :: tinin
        double precision                            :: toutout
        integer,      dimension(:), allocatable     :: iwork
        double precision, dimension(:), allocatable :: work
        double precision, dimension(:), allocatable :: atolv
        double precision, dimension(:), allocatable :: rtolv

        if(UseSolout)then
            NRDENS  = neq
            iout    = 2
        else
            NRDENS  = 0
            iout    = 0
        end if

        lw  = 8*neq + 5*NRDENS + 21
        liw = NRDENS + 21

        allocate(work(lw), iwork(liw))

        work(1:20)  = 0d0
        !work(7)=1d-4
        iwork(1:20) = 0
!    IWORK(4)  TEST FOR STIFFNESS IS ACTIVATED AFTER STEP NUMBER
!              J*IWORK(4) (J INTEGER), PROVIDED IWORK(4).GT.0.
!              FOR NEGATIVE IWORK(4) THE STIFFNESS TEST IS
!              NEVER ACTIVATED; DEFAULT VALUE IS IWORK(4)=1000
        iwork(4)    = 300
        iwork(3)    = -1
        flag        = 0
        iwork(5)    = NRDENS
        !use vector tolerances to force step control to use only y

        allocate(atolv(neq),rtolv(neq))
        atolv   = 0d0
        rtolv   = 0d0
        itol    = 2

        atolv(1:ncpas)  = TolAbs
        rtolv(1:ncpas)  = TolRel
        iwork(1)        = MaxSteps
        if(MaxStepSize.gt.0d0)then
            work(6) = MaxStepSize
        end if

        xf      = x0
        tinin   = tin
        toutout = tout

        call dopri5(neq,fun,tinin,xf,toutout,rtolv,atolv,itol,      &
                    solout,iout,work,lw,                            &
                    iwork,liw,funpar,lfunpar,flag)

        if (flag.eq.2) then
            flag = 1
        end if

        tout = tinin
        nfev = iwork(17)

!   IWORK(17)  NFCN    NUMBER OF FUNCTION EVALUATIONS
!   IWORK(18)  NSTEP   NUMBER OF COMPUTED STEPS
!   IWORK(19)  NACCPT  NUMBER OF ACCEPTED STEPS
!   IWORK(20)  NREJCT  NUMBER OF REJECTED STEPS (DUE TO ERROR TEST),

        deallocate(work,iwork,atolv,rtolv)

    end subroutine dopri5Int

    ! ------------------------------------------------------------------------
    ! ------------------------------------------------------------------------
    subroutine dop853Int(fun,neq,x0,funpar,lfunpar,ncpas,tin,tout,xf,flag,nfev,solout,&
                        MaxSteps,TolAbs,TolRel,MaxStepSize,UseSolout)
        implicit none
        integer,            intent(in)                           :: lfunpar
        integer,            intent(in)                           :: neq
        integer,            intent(in)                           :: ncpas
        double precision,   intent(inout)                        :: tin
        double precision,   intent(inout)                        :: tout
        double precision,   intent(in),  dimension(neq)          :: x0
        double precision,   intent(in),  dimension(lfunpar)      :: funpar
        double precision,   intent(out), dimension(neq)          :: xf
        integer,            intent(out)                          :: flag, nfev
        integer,            intent(in)                          :: MaxSteps
        double precision,   intent(in)                          :: TolAbs
        double precision,   intent(in)                          :: TolRel
        double precision,   intent(in)                          :: MaxStepSize
        logical,            intent(in)                          :: UseSolout

        interface
        Subroutine solout(Niter,told,time,y,neq,dpar,ldpar,ipar,lipar,fun,funpar,lfunpar,irtrn)
            integer,            intent(in)                         :: Niter
            double precision,   intent(in)                         :: told
            double precision,   intent(inout)                      :: time
            double precision,   intent(inout), dimension(neq)      :: y
            integer,            intent(in)                         :: neq
            double precision,   intent(in),    dimension(ldpar)     :: dpar
            integer,            intent(in)                         :: ldpar
            integer,            intent(in),    dimension(lipar)    :: ipar
            integer,            intent(in)                         :: lipar
            double precision,   intent(in),    dimension(lfunpar)  :: funpar
            integer,            intent(in)                         :: lfunpar
            integer,            intent(inout)                      :: irtrn
            interface
                Subroutine fun(neq,time,y,value,funpar,lfunpar)
                    implicit none
                    integer,           intent(in)                      :: neq
                    integer,           intent(in)                      :: lfunpar
                    double precision,  intent(in)                      :: time
                    double precision,  intent(in),  dimension(lfunpar) :: funpar
                    double precision,  intent(in),  dimension(neq)     :: y
                    double precision,  intent(out), dimension(neq)     :: value
                end Subroutine fun
            end interface
        end Subroutine solout
        end interface

        interface
        Subroutine fun(neq,time,y,value,funpar,lfunpar)
            implicit none
            integer,           intent(in)                      :: neq
            integer,           intent(in)                      :: lfunpar
            double precision,  intent(in)                      :: time
            double precision,  intent(in),  dimension(lfunpar) :: funpar
            double precision,  intent(in),  dimension(neq)     :: y
            double precision,  intent(out), dimension(neq)     :: value
        end Subroutine fun
        end interface

        !local variables
        integer                                     :: iout
        integer                                     :: itol
        integer                                     :: lw
        integer                                     :: liw
        integer                                     :: NRDENS
        double precision                            :: tinin
        double precision                            :: toutout
        integer,      dimension(:), allocatable     :: iwork
        double precision, dimension(:), allocatable :: work
        double precision, dimension(:), allocatable :: atolv
        double precision, dimension(:), allocatable :: rtolv

        if(UseSolout)then
            NRDENS  = neq
            iout    = 2
        else
            NRDENS  = 0
            iout    = 0
        end if

        lw = 11*neq + 8*NRDENS + 21
        liw = NRDENS + 21

        allocate(work(lw), iwork(liw))

        work(1:20)  = 0d0
        !work(7)=1d-4
        iwork(1:20) = 0
!    IWORK(4)  TEST FOR STIFFNESS IS ACTIVATED AFTER STEP NUMBER
!              J*IWORK(4) (J INTEGER), PROVIDED IWORK(4).GT.0.
!              FOR NEGATIVE IWORK(4) THE STIFFNESS TEST IS
!              NEVER ACTIVATED; DEFAULT VALUE IS IWORK(4)=1000
        iwork(4)    = 300
        iwork(3)    = -1
        flag        = 0
        iwork(5)    = NRDENS

        !use vector tolerances to force step control to use only y
        allocate(atolv(neq),rtolv(neq))
        atolv = 0d0
        rtolv = 0d0
        itol = 2

        atolv(1:ncpas)  = TolAbs
        rtolv(1:ncpas)  = TolRel
        iwork(1)        = MaxSteps
        if(MaxStepSize.gt.0d0)then
            work(6) = MaxStepSize
        end if

        xf      = x0
        tinin   = tin
        toutout = tout

        call dop853(neq,fun,tinin,xf,toutout,rtolv,atolv,itol,      &
                    solout,iout,work,lw,                            &
                    iwork,liw,funpar,lfunpar,flag)

        if (flag.eq.2) then
            flag = 1
        end if

        tout = tinin
        nfev = iwork(17)

!   IWORK(17)  NFCN    NUMBER OF FUNCTION EVALUATIONS
!   IWORK(18)  NSTEP   NUMBER OF COMPUTED STEPS
!   IWORK(19)  NACCPT  NUMBER OF ACCEPTED STEPS
!   IWORK(20)  NREJCT  NUMBER OF REJECTED STEPS (DUE TO ERROR TEST),

        deallocate(work,iwork,atolv,rtolv)

    end subroutine dop853Int

    ! ------------------------------------------------------------------------
    ! ------------------------------------------------------------------------
    subroutine mas_radau_dummy()
    end subroutine mas_radau_dummy

    ! ------------------------------------------------------------------------
    ! ------------------------------------------------------------------------
    subroutine radauInt(fun,dfun,neq,x0,funpar,lfunpar,ncpas,tin,tout,xf,flag,nfev,solout,NSmin,NSmax,NSdeb,&
                        MaxSteps,TolAbs,TolRel,MaxStepSize,UseSolout,UseRadauHampath)
        implicit none
        integer,            intent(in)                           :: lfunpar,NSmin,NSmax,NSdeb
        integer,            intent(in)                           :: neq
        integer,            intent(in)                           :: ncpas
        double precision,   intent(inout)                        :: tin
        double precision,   intent(inout)                        :: tout
        double precision,   intent(in),  dimension(neq)          :: x0
        double precision,   intent(in),  dimension(lfunpar)      :: funpar
        double precision,   intent(out), dimension(neq)          :: xf
        integer,            intent(out)                          :: flag,nfev
        integer,            intent(in)                          :: MaxSteps
        double precision,   intent(in)                          :: TolAbs
        double precision,   intent(in)                          :: TolRel
        double precision,   intent(in)                          :: MaxStepSize
        logical,            intent(in)                          :: UseSolout
        logical,            intent(in)                          :: UseRadauHampath

        interface
        Subroutine solout(Niter,told,time,y,neq,dpar,ldpar,ipar,lipar,fun,funpar,lfunpar,irtrn)
            integer,            intent(in)                         :: Niter
            double precision,   intent(in)                         :: told
            double precision,   intent(inout)                      :: time
            double precision,   intent(inout), dimension(neq)      :: y
            integer,            intent(in)                         :: neq
            double precision,   intent(in),    dimension(ldpar)     :: dpar
            integer,            intent(in)                         :: ldpar
            integer,            intent(in),    dimension(lipar)    :: ipar
            integer,            intent(in)                         :: lipar
            double precision,   intent(in),    dimension(lfunpar)  :: funpar
            integer,            intent(in)                         :: lfunpar
            integer,            intent(inout)                      :: irtrn
            interface
                Subroutine fun(neq,time,y,val,funpar,lfunpar)
                    implicit none
                    integer,           intent(in)                      :: neq
                    integer,           intent(in)                      :: lfunpar
                    double precision,  intent(in)                      :: time
                    double precision,  intent(in),  dimension(lfunpar) :: funpar
                    double precision,  intent(in),  dimension(neq)     :: y
                    double precision,  intent(out), dimension(neq)     :: val
                end Subroutine fun
            end interface
        end Subroutine solout
        end interface

        interface
        Subroutine fun(neq,time,y,val,funpar,lfunpar)
            implicit none
            integer,           intent(in)                      :: neq
            integer,           intent(in)                      :: lfunpar
            double precision,  intent(in)                      :: time
            double precision,  intent(in),  dimension(lfunpar) :: funpar
            double precision,  intent(in),  dimension(neq)     :: y
            double precision,  intent(out), dimension(neq)     :: val
        end Subroutine fun
        end interface

        interface
            Subroutine dfun(neq,time,y,val,funpar,lfunpar)
                implicit none
                integer,           intent(in)                      :: neq
                integer,           intent(in)                      :: lfunpar
                double precision,  intent(in)                      :: time
                double precision,  intent(in),  dimension(lfunpar) :: funpar
                double precision,  intent(in),  dimension(neq)     :: y
                double precision,  intent(out), dimension(neq,neq) :: val
            end Subroutine dfun
        end interface

        !local variables
        integer                                 :: iout
        integer                                 :: itol
        integer                                 :: lw
        integer                                 :: liw
        !integer                                 :: NRDENS
        double precision                            :: tinin
        double precision                            :: toutout
        integer,      dimension(:), allocatable :: iwork
        double precision, dimension(:), allocatable :: work
        double precision, dimension(:), allocatable :: atolv
        double precision, dimension(:), allocatable :: rtolv
        double precision :: H
        integer :: IJAC,MLJAC,MUJAC,IMAS,MLMAS,MUMAS,LJAC,LMAS,LE

        flag    = 0
        xf      = x0
        tinin   = tin
        toutout = tout
        H       = 0d0

        if(UseSolout)then
            iout    = 1
        else
            iout    = 0
        end if

        LJAC = neq
        LMAS = 0
        LE   = neq
        lw   = neq*(LJAC+LMAS+NSmax*LE+3*NSmax+3)+20
        liw  = (2+(NSmax-1)/2)*neq+20

        allocate(work(lw), iwork(liw))

        work(1:20)  = 0d0
        iwork(1:20) = 0
!       iwork(3) = -1 !Equivalent RADAU : nexiste pas ?! Pour rien afficher

        allocate(atolv(neq),rtolv(neq))
        !atolv = atolG
        !rtolv = rtolG
        atolv = 0d0
        rtolv = 0d0
        itol = 1

        atolv(1:ncpas)  = TolAbs
        rtolv(1:ncpas)  = TolRel
        iwork(2)        = MaxSteps
        if(MaxStepSize.gt.0d0)then
            work(7) = MaxStepSize
        end if

        !We provide the jacobian
        IJAC  = 1
        MLJAC = neq
        MUJAC = 0

        IMAS  = 0
        MLMAS = neq
        MUMAS = 0

        iwork(11) = NSmin
        iwork(12) = NSmax
        iwork(13) = NSdeb

        if(UseRadauHampath) then
            !On duplique à cause des problèmes liés aux variables globales
            !Il faut changer integhamG pour appeler la bonne fonction pour la sortie dense
            call RADAUHAMPATH(neq,fun,tinin,xf,toutout,         &
                        H,rtolv,atolv,itol,                     &
                        dfun,IJAC,MLJAC,MUJAC,       &
                        mas_radau_dummy,IMAS,MLMAS,MUMAS,       &
                        solout,iout,                            &
                        work,lw,iwork,liw,                      &
                        funpar,lfunpar,flag)
        else
            call RADAU(neq,fun,tinin,xf,toutout,         &
                        H,rtolv,atolv,itol,                     &
                        dfun,IJAC,MLJAC,MUJAC,       &
                        mas_radau_dummy,IMAS,MLMAS,MUMAS,       &
                        solout,iout,                            &
                        work,lw,iwork,liw,                      &
                        funpar,lfunpar,flag)
        end if

        if (flag.eq.2) then
            flag = 1
        end if

        tout = tinin
        nfev = iwork(14)

        deallocate(work,iwork,atolv,rtolv)

    end subroutine radauInt

    ! ------------------------------------------------------------------------
    ! ------------------------------------------------------------------------
    !
    ! AUXILIARY FUNCTIONS FOR IMPLICT RK SCHEMES
    !
    ! ------------------------------------------------------------------------
    ! ------------------------------------------------------------------------

    ! ------------------------------------------------------------------------
    ! ------------------------------------------------------------------------
    subroutine phidez(nz, Zc, fvec, iflag, fpar, lfpar, fun)
        implicit none
        integer,            intent(in)                          :: nz, lfpar
        double precision,   intent(in),  dimension(nz)          :: Zc
        double precision,   intent(out), dimension(nz)          :: fvec
        integer,            intent(inout)                       :: iflag
        double precision,   intent(inout),  dimension(lfpar)    :: fpar

        interface
            Subroutine fun(neq,time,y,val,funpar,lfunpar)
                implicit none
                integer,           intent(in)                      :: neq
                integer,           intent(in)                      :: lfunpar
                double precision,  intent(in)                      :: time
                double precision,  intent(in),  dimension(lfunpar) :: funpar
                double precision,  intent(in),  dimension(neq)     :: y
                double precision,  intent(out), dimension(neq)     :: val
            end Subroutine fun
        end interface

        !local variables
        integer                         :: neq, s, lfunpar, icur, j, nfev
        double precision                :: ti, h
        double precision, allocatable   :: yi(:), A(:,:), c(:), funpar(:), K(:), Id(:,:), AI(:,:)

        !fpar = neq, s, lfunpar, ti, yi, h, funpar, A(:), c, nfev
        neq     = int(fpar(1))
        s       = int(fpar(2))
        lfunpar = int(fpar(3))
        icur    = 3
        allocate(yi(neq), funpar(lfunpar), A(s,s), c(s), K(neq*s), Id(neq,neq), AI(s*neq,s*neq))
        ti      = fpar(icur+1);                             icur = icur + 1
        yi      = fpar(icur+1:icur+neq);                    icur = icur + neq
        h       = fpar(icur+1);                             icur = icur + 1
        funpar  = fpar(icur+1:icur+lfunpar);                icur = icur + lfunpar
        A       = reshape(fpar(icur+1:icur+s*s),(/s,s/));   icur = icur + s*s
        c       = fpar(icur+1:icur+s);                      icur = icur + s
        nfev    = int(fpar(icur+1))

        do j=1,s
            call fun(neq, ti+c(j)*h, yi + Zc((j-1)*neq+1:(j-1)*neq+neq),    &
            K((j-1)*neq+1:(j-1)*neq+neq), funpar, lfunpar)
            nfev = nfev + 1
        end do
        !construction de la matrice identite
        Id = 0d0
        do j=1,neq
            Id(j,j) = 1d0
        end do
        call kronecker(s,s,A,neq,neq,Id,AI)

        ! fvec = Zc - h * kron(A,eye(neq)) * K(:);
        fvec = Zc
        call DGEMV('N',s*neq,s*neq,-h,AI,s*neq,K,1,1d0,fvec,1) ! fvec = fvec - h * AI * K
        fpar(icur+1) = dble(nfev)

        iflag = 0

        deallocate(yi, funpar, A, c, K, Id, AI)

    end subroutine phidez

    ! ------------------------------------------------------------------------
    ! ------------------------------------------------------------------------
    subroutine dphidez(nz, Zc, fjac, iflag, fpar, lfpar, dfun)
        implicit none
        integer,            intent(in)                          :: nz, lfpar
        double precision,   intent(in),  dimension(nz)          :: Zc
        double precision,   intent(out), dimension(nz,nz)       :: fjac
        integer,            intent(inout)                       :: iflag
        double precision,   intent(inout),  dimension(lfpar)    :: fpar

        interface
            Subroutine dfun(neq,time,y,val,funpar,lfunpar)
                implicit none
                integer,           intent(in)                      :: neq
                integer,           intent(in)                      :: lfunpar
                double precision,  intent(in)                      :: time
                double precision,  intent(in),  dimension(lfunpar) :: funpar
                double precision,  intent(in),  dimension(neq)     :: y
                double precision,  intent(out), dimension(neq,neq) :: val
            end Subroutine dfun
        end interface

        !local variables
        integer :: j
        double precision :: h !fvec1(nz), fvec2(nz), Zctemp(nz), fjacaux(nz,nz) ,eps

        !specific analytic
        integer                         :: neq, s, lfunpar, icur
        double precision                :: ti
        double precision, allocatable   :: yi(:), A(:,:), c(:), funpar(:), df(:,:), Id1(:,:), Id2(:,:), AI(:,:), dFdZ(:,:)

        iflag = 1

        !fpar = neq, s, lfunpar, ti, yi, h, funpar, A(:), c, nfev
        neq     = int(fpar(1))
        s       = int(fpar(2))
        lfunpar = int(fpar(3))
        icur    = 3
        allocate(yi(neq), funpar(lfunpar), A(s,s), c(s), df(neq,neq))
        allocate(Id1(neq,neq), Id2(neq*s,neq*s), AI(s*neq,s*neq), dFdZ(neq*s,neq*s))
        ti      = fpar(icur+1);                             icur = icur + 1
        yi      = fpar(icur+1:icur+neq);                    icur = icur + neq
        h       = fpar(icur+1);                             icur = icur + 1
        funpar  = fpar(icur+1:icur+lfunpar);                icur = icur + lfunpar
        A       = reshape(fpar(icur+1:icur+s*s),(/s,s/));   icur = icur + s*s
        c       = fpar(icur+1:icur+s);                      icur = icur + s

        dFdZ = 0d0
        do j=1,s
            call dfun(neq, ti+c(j)*h, yi + Zc((j-1)*neq+1:(j-1)*neq+neq), df, funpar, lfunpar)
            dFdZ((j-1)*neq+1:(j-1)*neq+neq,(j-1)*neq+1:(j-1)*neq+neq) = df
        end do
        Id1 = 0d0
        do j=1,neq
            Id1(j,j) = 1d0
        end do
        call kronecker(s,s,A,neq,neq,Id1,AI)
        Id2 = 0d0
        do j=1,neq*s
            Id2(j,j) = 1d0
        end do

        call DGEMM('N','N',neq*s,neq*s,neq*s,-h,AI,neq*s,dFdZ,neq*s,1d0,Id2,neq*s)
        fjac = Id2

        deallocate(yi, A, c, funpar, df, Id1, Id2, AI, dFdZ)

        !----------------------------------------------------------------!
        !Comparaison avec les différences finies
!                eps  = epsilon(1d0)
!                Zctemp = Zc
!                call phidez(nz, Zctemp, fvec2, iflag, fpar, lfpar, fun)
!                do j=1,nz
!                    h = dsqrt(eps*max(1.d-5,abs(Zc(j))))
!                    Zctemp(j) = Zc(j) + h
!                    call phidez(nz, Zctemp, fvec1, iflag, fpar, lfpar, fun)
!                    Zctemp(j) = Zc(j)
!                    fjacaux(:,j) = (fvec1 - fvec2)/h
!                end do
!                call printMatrice('diffFJAC',fjacAux-fjac,neq*s,neq*s)
        !----------------------------------------------------------------!

    end subroutine dphidez

    ! ------------------------------------------------------------------------
    ! ------------------------------------------------------------------------
    Subroutine phidphidez(nz,Zc,fpar,lfpar,fvec,fjac,ldfjac,iflag,fun,dfun)
        implicit none
        integer,          intent(in)                        :: nz
        integer,          intent(in)                        :: lfpar
        integer,          intent(in)                        :: ldfjac
        double precision, intent(inout), dimension(lfpar)   :: fpar
        double precision, intent(in), dimension(nz)         :: Zc
        double precision, intent(inout), dimension(nz)        :: fvec
        double precision, intent(inout), dimension(ldfjac,nz) :: fjac
        integer,          intent(inout)                     :: iflag

        interface
            Subroutine fun(neq,time,y,val,funpar,lfunpar)
                implicit none
                integer,           intent(in)                      :: neq
                integer,           intent(in)                      :: lfunpar
                double precision,  intent(in)                      :: time
                double precision,  intent(in),  dimension(lfunpar) :: funpar
                double precision,  intent(in),  dimension(neq)     :: y
                double precision,  intent(out), dimension(neq)     :: val
            end Subroutine fun
        end interface

        interface
            Subroutine dfun(neq,time,y,val,funpar,lfunpar)
                implicit none
                integer,           intent(in)                      :: neq
                integer,           intent(in)                      :: lfunpar
                double precision,  intent(in)                      :: time
                double precision,  intent(in),  dimension(lfunpar) :: funpar
                double precision,  intent(in),  dimension(neq)     :: y
                double precision,  intent(out), dimension(neq,neq) :: val
            end Subroutine dfun
        end interface

        !Local variables

        ! iflag = 1 : function at x and return fvec. do not alter fjac.
        ! iflag = 2 : jacobian at x and return fjac. do not alter fvec.

        if (iflag == 1) then

            !Function evaluation
            call phidez(nz, Zc, fvec, iflag, fpar, lfpar, fun)

        elseif(iflag == 2) then

            !Jacobian evaluation
            call dphidez(nz, Zc, fjac, iflag, fpar, lfpar, dfun)

        end if

    end subroutine phidphidez


    ! ------------------------------------------------------------------------
    ! ------------------------------------------------------------------------
    !
    ! IMPLICT AND EXPLICIT RK SCHEMES
    !
    ! ------------------------------------------------------------------------
    ! ------------------------------------------------------------------------

    ! ------------------------------------------------------------------------
    ! ------------------------------------------------------------------------
    subroutine integImp(fun,neq,y0,funpar,lfunpar,tin,yf,flag,nfev,solout,BTable,phidezV,phidphidezV,&
            irki,irks,IntegrationList,Display)
        implicit none
        integer,            intent(in)                           :: lfunpar
        integer,            intent(in)                           :: neq
        double precision,   intent(inout)                        :: tin
        double precision,   intent(in),  dimension(neq)          :: y0
        double precision,   intent(in),  dimension(lfunpar)      :: funpar
        double precision,   intent(out), dimension(neq)          :: yf
        integer,            intent(out)                          :: flag,nfev
        type(TButcherTable),intent(in)                           :: BTable
        character(32), intent(in)                                :: irki, irks
        type(TLISTE), intent(in)                                 :: IntegrationList
        logical, intent(in)                                      :: Display

        interface
        Subroutine solout(Niter,told,time,y,neq,dpar,ldpar,ipar,lipar,fun,funpar,lfunpar,irtrn)
            integer,            intent(in)                         :: Niter
            double precision,   intent(in)                         :: told
            double precision,   intent(inout)                      :: time
            double precision,   intent(inout), dimension(neq)      :: y
            integer,            intent(in)                         :: neq
            double precision,   intent(in),    dimension(ldpar)     :: dpar
            integer,            intent(in)                         :: ldpar
            integer,            intent(in),    dimension(lipar)    :: ipar
            integer,            intent(in)                         :: lipar
            double precision,   intent(in),    dimension(lfunpar)  :: funpar
            integer,            intent(in)                         :: lfunpar
            integer,            intent(inout)                      :: irtrn
            interface
                Subroutine fun(neq,time,y,val,funpar,lfunpar)
                    implicit none
                    integer,           intent(in)                      :: neq
                    integer,           intent(in)                      :: lfunpar
                    double precision,  intent(in)                      :: time
                    double precision,  intent(in),  dimension(lfunpar) :: funpar
                    double precision,  intent(in),  dimension(neq)     :: y
                    double precision,  intent(out), dimension(neq)     :: val
                end Subroutine fun
            end interface
        end Subroutine solout
        end interface

        interface
        Subroutine fun(neq,time,y,val,funpar,lfunpar)
            implicit none
            integer,           intent(in)                      :: neq
            integer,           intent(in)                      :: lfunpar
            double precision,  intent(in)                      :: time
            double precision,  intent(in),  dimension(lfunpar) :: funpar
            double precision,  intent(in),  dimension(neq)     :: y
            double precision,  intent(out), dimension(neq)     :: val
        end Subroutine fun
        end interface

        interface
        subroutine phidezV(nz, Zc, fvec, iflag, fpar, lfpar)
            implicit none
            integer,            intent(in)                          :: nz, lfpar
            double precision,   intent(in),  dimension(nz)          :: Zc
            double precision,   intent(out), dimension(nz)          :: fvec
            integer,            intent(inout)                       :: iflag
            double precision,   intent(inout),  dimension(lfpar)    :: fpar
        end Subroutine phidezV
        end interface

        interface
        Subroutine phidphidezV(nz,Zc,fpar,lfpar,fvec,fjac,ldfjac,iflag)
            implicit none
            integer,          intent(in)                        :: nz
            integer,          intent(in)                        :: lfpar
            integer,          intent(in)                        :: ldfjac
            double precision, intent(inout), dimension(lfpar)   :: fpar
            double precision, intent(in), dimension(nz)         :: Zc
            double precision, intent(out), dimension(nz)        :: fvec
            double precision, intent(out), dimension(ldfjac,nz) :: fjac
            integer,          intent(inout)                     :: iflag
        end subroutine phidphidezV
        end interface

        !Local variables
        integer                                         :: Niter, irtrn, i, j, nsteps, lipar, ldpar
        double precision                                :: ti, tip1, tim1, h, him1
        double precision, dimension(neq)                :: y, yim1
        type(TNOEUD), pointer                           :: noeud
        double precision, dimension(1)                  :: dpar
        integer,          dimension(1)                  :: ipar
        CHARACTER(len=120)                              :: LINE


        integer :: s, ordre, sbis, INFO
        double precision, dimension(:,:), allocatable   :: K, A, Adgesv, d
        double precision, dimension(:)  , allocatable   :: b, c, IPIV

        double precision, dimension(:)  , allocatable   :: tinInterp, toutInterp
        double precision, dimension(:,:), allocatable   :: yinInterp, youtInterp

        !specific implicit
        double precision    :: fpeps, normprog, dnrm2, eta
        integer             :: fpitermax, nbiter, NiterS
        CHARACTER(len=32)   :: choixSolveur, choixInit
        double precision    :: eps

        double precision, dimension(:),   allocatable :: fpar, Zc, DZc, fdeZc
        double precision, dimension(:,:), allocatable :: dfdeZc
        integer :: lfpar, flagSolveur, icur, flagPdP

        !specific hybrj
        integer :: njac

        eps  = 1d0*epsilon(1d0)

        choixInit       = irki ! '2'
        choixSolveur    = irks ! 'pfixe'

        fpitermax       = 7 !15

        lipar   = 1
        ldpar   = 1
        Niter   = 0
        irtrn   = 1
        flag    = 1
        ti      = tin
        tip1    = tin
        tim1    = tin
        y       = y0
        yim1    = y0
        NiterS  = 0

        !get Butcher Table
        call getNbStages(BTable,s)

        allocate(A(s,s), Adgesv(s,s), b(s), c(s), d(s,1), IPIV(neq*s), K(neq,s))

        allocate(tinInterp(max(4,s+1)), yinInterp(neq,max(4,s+1)), toutInterp(s), youtInterp(neq,s))

        lfpar = 1 + 1 + 1 + 1 + neq + 1 + lfunpar + s*s + s + 1
        allocate(fpar(lfpar), Zc(neq*s), DZc(neq*s), fdeZc(neq*s), dfdeZc(neq*s,neq*s))

        ! On récupère les infos du tableau de Butcher
        call getAll(BTable,A,b,c,s,ordre)

        ! On calcule d tel que A d = b
        Adgesv  = transpose(A)
        d(:,1)  = b
        call DGESV( s, 1, Adgesv, s, IPIV(1:s), d, s, INFO )

        !Appel a solout en t0 pour enregistrer la premiere valeur
        call solout(Niter,ti,tip1,y,neq,dpar,ldpar,ipar,lipar,fun,funpar,lfunpar,irtrn)

        !Gestion des pas ou du premier pas
        !!! Indiquer que l'on ne tient pas compte des options : si display = 1
        if(Display) then
        !if(Display .and. IsFirstDisplay_mod) then
        !    IsFirstDisplay_mod = .false.
            WRITE (LINE,'(a)') 'Warning: fixed-step integrator with a given'  ;    call myprint(LINE,.false.)
            WRITE (LINE,'(a)') ' grid of the form: [t0 t1 ... tf].';    call myprint(LINE,.true.)
            WRITE (LINE,'(a)') 'Non used options for this choice:' ;    call myprint(LINE,.false.)
            WRITE (LINE,'(a)') ' MaxStepsOde, MaxStepSizeOde, TolOdeAbs, TolOdeRel.';   call myprint(LINE,.true.)
        end if
        nsteps  = LONGUEUR(IntegrationList) - 1
        call GET_NOEUD_SUIVANT(noeud,IntegrationList)
        ti      = GET_TIME(noeud)

        eta     = 1d0
        nfev    = 0

        ! Mise à jour du vecteur fpar
        !
        !fpar  = neq, s, lfunpar, ti, yi, h, funpar, A(:), c, nfev
        !lfpar = 1 + 1 + 1 + 1 + neq + 1 + lfunpar + s*s + s + 1
        !Zc(neq*s), fdeZc(neq*s), flagSolveur

        fpar(1)  = dble(neq)
        fpar(2)  = dble(s)
        fpar(3)  = dble(lfunpar)
        icur                                    = 3
        !fpar(icur+1)                            = ti;
        icur = icur + 1         ! Mis à jour dans les itérations
        !fpar(icur+1:icur+neq)                   = y ;
        icur = icur + neq       ! Mis à jour dans les itérations
        !fpar(icur+1)                            = h ;
        icur = icur + 1         ! Mis à jour dans les itérations
        fpar(icur+1:icur+lfunpar)               = funpar;
        icur = icur + lfunpar
        fpar(icur+1:icur+s*s)                   = reshape(A,(/s*s/));   icur = icur + s*s
        fpar(icur+1:icur+s)                     = c;                    icur = icur + s
        !fpar(icur+1)                            = dble(nfev)                                    ! Mis à jour dans les itérations

        !Integration numerique
        do i=1,nsteps

            !temps suivant
            call GET_NOEUD_SUIVANT(noeud)
            tip1    = GET_TIME(noeud)
            h       = tip1 - ti
            him1    = h
            fpeps   = 1d-1*(h**ordre)

            !Initialisation des Zi
            select case (choixInit)

                case ('0')

                    Zc = 0d0

                case ('1')

                    call fun(neq, ti, y, K(:,1), funpar, lfunpar); nfev = nfev + 1
                    do j=1,s
                        Zc((j-1)*neq+1:(j-1)*neq+neq) = c(j) * h * K(:,1);
                    end do

                case ('2')

                    if(i.eq.1) then

                        call fun(neq, ti, y, K(:,1), funpar, lfunpar); nfev = nfev + 1
                        do j=1,s
                            Zc((j-1)*neq+1:(j-1)*neq+neq) = c(j) * h * K(:,1);
                        end do

                    else

                        sbis            = 0
                        tinInterp(1)    = tim1
                        yinInterp(:,1)  = yim1
                        do j=1,s
                            if(abs(c(j)).gt.eps) then
                                sbis                = sbis + 1
                                tinInterp(sbis+1)   = tim1 + c(j) * him1
                                yinInterp(:,sbis+1) = yim1 + Zc((j-1)*neq+1:(j-1)*neq+neq)
                            end if
                            toutInterp(j) = ti + c(j) * h
                        end do
                        call interpolationLagrange(neq,sbis+1,tinInterp(1:sbis+1),yinInterp(1:neq,1:sbis+1),    &
                                                                    s,toutInterp(1:s),youtInterp(1:neq,1:s))
                        do j=1,s
                            Zc((j-1)*neq+1:(j-1)*neq+neq) = youtInterp(:,j) - y
                        end do
                    end if

                case default

                    !free memory
                    deallocate(A, b, c, K, tinInterp, yinInterp, toutInterp, youtInterp, fpar, Zc, &
                                DZc, fdeZc, dfdeZc)

                    CALL printandstop('  ||| ERROR: Implicit integrator -> Unknown Zi initialization choice ' // choixInit)

            end select


            ! Mise à jour du vecteur fpar

!            fpar(1)  = dble(neq)
!            fpar(2)  = dble(s)
!            fpar(3)  = dble(lfunpar)
            icur                                    = 3
            fpar(icur+1)                            = ti;                   icur = icur + 1         ! Mis à jour dans les itérations
            fpar(icur+1:icur+neq)                   = y ;                   icur = icur + neq       ! Mis à jour dans les itérations
            fpar(icur+1)                            = h ;                   icur = icur + 1         ! Mis à jour dans les itérations
            !fpar(icur+1:icur+lfunpar)               = funpar;
            icur = icur + lfunpar
            !fpar(icur+1:icur+s*s)                   = reshape(A,(/s*s/));
            icur = icur + s*s
            !fpar(icur+1:icur+s)                     = c;
            icur = icur + s
            fpar(icur+1)                            = dble(nfev)                                    ! Mis à jour dans les itérations

            ! Calcul des Ki
            select case (choixSolveur)

                case ('pfixe')

                    nbiter = 0
                    calculZi : DO

                        call phidezV(neq*s, Zc, fdeZc, flagSolveur, fpar, lfpar)
                        Zc       = Zc - fdeZc
                        normprog = dnrm2(neq*s, fdeZc, 1)/(dnrm2(neq*s, Zc, 1) + sqrt(eps))                           ! Delta Z = fdeZc
                        nbiter   = nbiter + 1

                    IF ((normprog.le.fpeps) .or. (nbiter.ge.fpitermax)) EXIT calculZi
                    END DO calculZi

                case ('hybrd')

                    call hybrdint(phidezV,neq*s,Zc,fpeps,fpitermax,nbiter,Zc,fdeZc,flagSolveur,fpar,lfpar)

                case ('hybrj')

                    call hybrjint(phidphidezV,neq*s,Zc,lfpar,fpar,fpeps,fpitermax,Zc,fdeZc,nbiter,njac,flagSolveur)

                case ('newton')

                    !On calcule la jacobienne de phi que l'on cherche à annuler
                    flagPdP = 2
                    call phidphidezV(neq*s,Zc,fpar,lfpar,fdeZc,dfdeZc,neq*s,flagPdP)

                    !On fait la facto LU qui est stockée dans dfdeZc
                    CALL DGETRF( neq*s, neq*s, dfdeZc, neq*s, IPIV, INFO )

                    nbiter = 0
                    calculZiNewton : DO

                        ! On résout le système : phi(Zc) + dphi(Zc) * DZc = 0
                        call phidezV(neq*s, Zc, fdeZc, INFO, fpar, lfpar)
                        DZc = -fdeZc
                        CALL DGETRS( 'No transpose', neq*s, 1, dfdeZc, neq*s, IPIV, DZc, neq*s, INFO )

                        Zc       = Zc + DZc
                        normprog = dnrm2(neq*s, DZc, 1)/(dnrm2(neq*s, Zc, 1) + sqrt(eps))                           ! Delta Z = fdeZc
                        nbiter   = nbiter + 1

                    IF ((normprog.le.fpeps) .or. (nbiter.ge.fpitermax)) EXIT calculZiNewton
                    END DO calculZiNewton


                case default

                    !free memory
                    deallocate(A, Adgesv, b, c, d, IPIV, K, tinInterp, yinInterp, toutInterp, &
                                youtInterp, fpar, Zc, DZc, fdeZc, dfdeZc)

                    CALL printandstop('  ||| ERROR: Implicit integrator -> Unknown Ki computation choice ' // choixSolveur)

            end select

            yim1    = y
            him1    = h
            tim1    = ti

            ! On met à jour y
            ! Attention : Zc est en colonne mais c'est bon car fortran stocke les matrices en colonnes
            call DGEMV('N',neq,s,1d0,Zc,neq,d(:,1),1,1d0,y,1) !y       = y + matmul(Z,d)

            ! On stocke la nouvelle valeur
            call solout(NiterS+1,ti,tip1,y,neq,dpar,ldpar,ipar,lipar,fun,funpar,lfunpar,irtrn)

            ti      = tip1
            NiterS  = NiterS + 1

            if(nbiter.gt.fpitermax) then
                flag = -8
            end if

            ! On doit mettre à jour car nfev est modifié si on choisit l'initialisation 1 ou 2
            nfev    = int(fpar(icur+1))

        end do !fin integration

        yf      = y

        !free memory
        deallocate(A, Adgesv, b, c, d, IPIV, K, tinInterp, yinInterp, toutInterp, youtInterp, fpar, &
                    Zc, DZc, fdeZc, dfdeZc)

    end subroutine integImp

    ! ------------------------------------------------------------------------
    ! ------------------------------------------------------------------------
    subroutine integExp(fun,neq,y0,funpar,lfunpar,tin,yf,flag,nfev,solout,BTable,IntegrationList,Display)
        implicit none
        integer,            intent(in)                           :: lfunpar
        integer,            intent(in)                           :: neq
        double precision,   intent(inout)                        :: tin
        double precision,   intent(in),  dimension(neq)          :: y0
        double precision,   intent(in),  dimension(lfunpar)      :: funpar
        double precision,   intent(out), dimension(neq)          :: yf
        integer,            intent(out)                          :: flag,nfev
        type(TButcherTable),intent(in)                           :: BTable
        type(TLISTE), intent(in)                                 :: IntegrationList
        logical, intent(in)                                      :: Display

        interface
        Subroutine solout(Niter,told,time,y,neq,dpar,ldpar,ipar,lipar,fun,funpar,lfunpar,irtrn)
            integer,            intent(in)                         :: Niter
            double precision,   intent(in)                         :: told
            double precision,   intent(inout)                      :: time
            double precision,   intent(inout), dimension(neq)      :: y
            integer,            intent(in)                         :: neq
            double precision,   intent(in),    dimension(ldpar)     :: dpar
            integer,            intent(in)                         :: ldpar
            integer,            intent(in),    dimension(lipar)    :: ipar
            integer,            intent(in)                         :: lipar
            double precision,   intent(in),    dimension(lfunpar)  :: funpar
            integer,            intent(in)                         :: lfunpar
            integer,            intent(inout)                      :: irtrn
            interface
                Subroutine fun(neq,time,y,val,funpar,lfunpar)
                    implicit none
                    integer,           intent(in)                      :: neq
                    integer,           intent(in)                      :: lfunpar
                    double precision,  intent(in)                      :: time
                    double precision,  intent(in),  dimension(lfunpar) :: funpar
                    double precision,  intent(in),  dimension(neq)     :: y
                    double precision,  intent(out), dimension(neq)     :: val
                end Subroutine fun
            end interface
        end Subroutine solout
        end interface

        interface
            Subroutine fun(neq,time,y,val,funpar,lfunpar)
                implicit none
                integer,           intent(in)                      :: neq
                integer,           intent(in)                      :: lfunpar
                double precision,  intent(in)                      :: time
                double precision,  intent(in),  dimension(lfunpar) :: funpar
                double precision,  intent(in),  dimension(neq)     :: y
                double precision,  intent(out), dimension(neq)     :: val
            end Subroutine fun
        end interface

        !Local variables
        integer                                         :: Niter, irtrn, i, j, nsteps, lipar, ldpar
        double precision                                :: ti, tip1, h
        double precision, dimension(neq)                :: y, yaux
        type(TNOEUD), pointer                           :: noeud
        double precision, dimension(1)                  :: dpar
        integer,          dimension(1)                  :: ipar
        CHARACTER(len=120)  :: LINE

        integer :: s, ordre
        double precision, dimension(:,:), allocatable   :: K, A
        double precision, dimension(:)  , allocatable   :: b, c

        lipar   = 1
        ldpar   = 1
        Niter   = 0
        irtrn   = 1
        flag    = 1
        ti      = tin
        tip1    = tin
        y       = y0

        !get Butcher Table
        call getNbStages(BTable,s)
        allocate(A(s,s), b(s), c(s), K(neq,s))
        call getAll(BTable,A,b,c,s,ordre)

        !Appel a solout en t0 pour enregistrer la premiere valeur
        call solout(Niter,ti,tip1,y,neq,dpar,ldpar,ipar,lipar,fun,funpar,lfunpar,irtrn)

        !Gestion des pas ou du premier pas
        !!! Indiquer que l'on ne tient pas compte des options : si display = 1
        if(Display) then
        !if(Display .and. IsFirstDisplay_mod) then
        !    IsFirstDisplay_mod = .false.
            WRITE (LINE,'(a)') 'Warning: fixed-step integrator with a given'  ;    call myprint(LINE,.false.)
            WRITE (LINE,'(a)') ' grid of the form: [t0 t1 ... tf].';    call myprint(LINE,.true.)
            WRITE (LINE,'(a)') 'Non used options for this choice:' ;    call myprint(LINE,.false.)
            WRITE (LINE,'(a)') ' MaxStepsOde, MaxStepSizeOde, TolOdeAbs, TolOdeRel.';   call myprint(LINE,.true.)
        end if
        nsteps  = LONGUEUR(IntegrationList) - 1
        call GET_NOEUD_SUIVANT(noeud,IntegrationList)
        ti      = GET_TIME(noeud)

        nfev = nsteps*ordre

        !Integration numerique
        do i=1,nsteps

            !temps suivant
            call GET_NOEUD_SUIVANT(noeud)
            tip1    = GET_TIME(noeud)
            h       = tip1 - ti

            ! Calcul des Ki
            call fun(neq, ti, y, K(:,1), funpar, lfunpar)
            do j=2,s
                yaux = y
                call DGEMV('N',neq,j-1,h,K(:,1:j-1),neq,A(j,1:(j-1)),1,1d0,yaux,1)
                call fun(neq, ti+c(j)*h, yaux, K(:,j), funpar, lfunpar)
                !call fun(neq, ti+c(j)*h, y + h * matmul(K(:,1:j-1), transpose(A(j,1:(j-1)))), K(:,j), funpar, lfunpar)
            end do

            call DGEMV('N',neq,s,h,K,neq,b,1,1d0,y,1) !y       = y + h * matmul(K,b)
            !y       = y + h * matmul(K,b) !mise à jour de y

            call solout(Niter,ti,tip1,y,neq,dpar,ldpar,ipar,lipar,fun,funpar,lfunpar,irtrn)

            Niter   = Niter + 1
            ti      = tip1

        end do

        yf      = y

        !free memory
        deallocate(A, b, c, K)

    end subroutine integExp

    ! ------------------------------------------------------------------------
    ! ------------------------------------------------------------------------
    !
    ! BUTCHER TABLE
    !
    ! ------------------------------------------------------------------------
    ! ------------------------------------------------------------------------

    ! ------------------------------------------------------------------------
    ! ------------------------------------------------------------------------
    subroutine createButcherTable(table,A,b,c,s,ordre,infos)
        integer,                            intent(in)  :: s
        integer,                            intent(in)  :: ordre
        double precision, dimension(s,s),   intent(in)  :: A
        double precision, dimension(s),     intent(in)  :: b
        double precision, dimension(s),     intent(in)  :: c
        character(8),                       intent(in)  :: infos
        type(TButcherTable),                intent(out) :: table

        allocate(table%A(s,s),table%b(s),table%c(s))
        table%A = A
        table%b = b
        table%c = c

        table%s = s
        table%ordre = ordre
        table%infos = infos

    end subroutine createButcherTable

    ! ------------------------------------------------------------------------
    ! ------------------------------------------------------------------------
    subroutine destroyButcherTable(table)
        type(TButcherTable), intent(inout) :: table
        deallocate(table%A,table%b,table%c)
    end subroutine destroyButcherTable

    ! ------------------------------------------------------------------------
    ! ------------------------------------------------------------------------
    subroutine getNbStages(table,s)
        integer,                            intent(out) :: s
        type(TButcherTable),                intent(in)  :: table
        s = table%s
    end subroutine getNbStages

    ! ------------------------------------------------------------------------
    ! ------------------------------------------------------------------------
    subroutine getAll(table,A,b,c,s,ordre)
        integer,                            intent(in)  :: s
        integer,                            intent(out) :: ordre
        type(TButcherTable),                intent(in)  :: table
        double precision, dimension(s,s),   intent(out) :: A
        double precision, dimension(s),     intent(out) :: b
        double precision, dimension(s),     intent(out) :: c

        A = table%A
        b = table%b
        c = table%c
        ordre = table%ordre

    end subroutine getAll

    ! ------------------------------------------------------------------------
    ! ------------------------------------------------------------------------
    subroutine getInfos(table,infos)
        type(TButcherTable),                intent(in)  :: table
        character(8),                       intent(out) :: infos
        infos = table%infos
    end subroutine getInfos

    ! ------------------------------------------------------------------------
    ! ------------------------------------------------------------------------
    subroutine getButcherTab(inte,BTable,infos)
        implicit none
        character(32),                      intent(in)  :: inte
        character(8),                       intent(out) :: infos
        type(TButcherTable),                intent(out) :: BTable

        !local variables
        double precision, dimension(10,10)  :: A
        double precision, dimension(10)     :: b
        double precision, dimension(10)     :: c
        integer                             :: s, ordre

        !gauss6, 8 et raudas
        double precision :: r536, s15, s1524, s1536, s1530, s1515
        double precision :: w1, w2, w3, w4, w5, w1p, w2p, w3p, w4p, w5p
        double precision :: s6, s5, gam, del

        select case (StrLowCase(inte))

            case ('euler_explicite')

                s           = 1
                ordre       = 1
                infos       = 'explicit'

                A(1,1:s)    = 0d0
                b(1:s)      = (/ 1d0 /)
                c(1:s)      = (/ 0d0 /)

            case ('runge')

                s           = 2
                ordre       = 2
                infos       = 'explicit'

                A(1,1:s)    = 0d0
                A(2,1:s)    = (/ 1d0/2d0, 0d0 /)
                b(1:s)      = (/ 0d0, 1d0 /)
                c(1:s)      = (/ 0d0, 1d0/2d0 /)

            case ('heun')

                s           = 3
                ordre       = 3
                infos       = 'explicit'

                A(1,1:s)    = 0d0
                A(2,1:s)    = (/ 1d0/3d0, 0d0    , 0d0 /)
                A(3,1:s)    = (/ 0d0    , 2d0/3d0, 0d0 /)
                b(1:s)      = (/ 1d0/4d0, 0d0, 3d0/4d0 /)
                c(1:s)      = (/ 0d0, 1d0/3d0, 2d0/3d0 /)

            case ('rk4')

                s           = 4
                ordre       = 4
                infos       = 'explicit'

                A(1,1:s)    = 0d0
                A(2,1:s)    = (/ 1d0/2d0, 0d0       , 0d0, 0d0 /)
                A(3,1:s)    = (/ 0d0    , 1d0/2d0   , 0d0, 0d0 /)
                A(4,1:s)    = (/ 0d0    , 0d0       , 1d0, 0d0 /)

                b(1:s)      = (/ 1d0/6d0, 2d0/6d0, 2d0/6d0, 1d0/6d0 /)
                c(1:s)      = (/ 0d0, 1d0/2d0, 1d0/2d0, 1d0 /)

            case ('rk5')
                !rk5a
                !page 271, Hairer I
                s           = 5
                ordre       = 5
                infos       = 'explicit'

                A(1,1:s)    = 0d0
                A(2,1:s)    = (/ 1d0/5d0    , 0d0       , 0d0       , 0d0, 0d0 /)
                A(3,1:s)    = (/ 0d0        , 2d0/5d0   , 0d0       , 0d0, 0d0 /)
                A(4,1:s)    = (/ 3d0/16d0   , 0d0       , 5d0/16d0  , 0d0, 0d0 /)
                A(5,1:s)    = (/ 1d0/4d0    , 0d0       , -5d0/4d0  , 2d0, 0d0 /)

                b(1:s)      = (/ 1d0/6d0, 0d0, 0d0, 2d0/3d0, 1d0/6d0 /)
                c(1:s)      = (/ 0d0, 1d0/5d0, 2d0/5d0, 1d0/2d0, 0d0 /)

            case ('euler_implicite','euler')

                s           = 1
                ordre       = 1
                infos       = 'implicit'

                A(1,1:s)    = (/ 1d0 /)
                b(1:s)      = (/ 1d0 /)
                c(1:s)      = (/ 1d0 /)

            case ('midpoint')

                s           = 1
                ordre       = 1
                infos       = 'implicit'

                A(1,1:s)    = (/ 1d0/2d0 /)
                b(1:s)      = (/ 1d0 /)
                c(1:s)      = (/ 1d0/2d0 /)

            case ('gauss4')
                ! Description : Gauss (implicite, symetrique et symplectique ordre 4)
                ! Page 207, Hairer I

                s           = 2
                ordre       = 4
                infos       = 'implicit'

                A(1,1:s)    = (/ 1d0/4d0,               1d0/4d0-sqrt(3d0)/6d0 /)
                A(2,1:s)    = (/ 1d0/4d0+sqrt(3d0)/6d0, 1d0/4d0  /)

                b(1:s)      = (/ 1d0/2d0, 1d0/2d0 /)
                c(1:s)      = (/ 1d0/2d0-sqrt(3d0)/6d0, 1d0/2d0+sqrt(3d0)/6d0 /)

            case ('gauss6')
                ! Description : Gauss (implicite, symetrique et symplectique ordre 6)
                ! Page 209, Hairer I

                s           = 3
                ordre       = 6
                infos       = 'implicit'

                r536    = 5d0/36d0
                s15     = sqrt(15d0)
                s1524   = s15/24d0
                s1536   = s15/36d0
                s1530   = s15/30d0
                s1515   = s15/15d0

                A(1,1:s)    = (/ 5d0/36d0,          2d0/9d0-s1515,  5d0/36d0-s1530  /)
                A(2,1:s)    = (/ 5d0/36d0+s1524,    2d0/9d0,        5d0/36d0-s1524  /)
                A(3,1:s)    = (/ 5d0/36d0+s1530,    2d0/9d0+s1515,  5d0/36d0        /)

                b(1:s)      = (/ 5d0/18d0, 4d0/9d0, 5d0/18d0 /)
                c(1:s)      = (/ 1d0/2d0-s15/10d0, 1d0/2d0, 1d0/2d0+s15/10d0 /)

            case ('gauss8')
                ! Description : Gauss (implicite, symetrique et symplectique ordre 8)
                ! Page 209, Hairer I

                s           = 4
                ordre       = 8
                infos       = 'implicit'

                w1 = 1d0/8d0-sqrt(30d0)/144d0
                w2 = 1d0/2d0*sqrt((15d0+2d0*sqrt(30d0))/35d0)
                w3 = w2*(1d0/6d0+sqrt(30d0)/24d0)
                w4 = w2*(1d0/21d0+5d0*sqrt(30d0)/168d0)
                w5 = w2-2d0*w3

                w1p = 1d0/8d0+sqrt(30d0)/144d0
                w2p = 1d0/2d0*sqrt((15d0-2d0*sqrt(30d0))/35d0)
                w3p = w2p*(1d0/6d0-sqrt(30d0)/24d0)
                w4p = w2p*(1d0/21d0-5d0*sqrt(30d0)/168d0)
                w5p = w2p-2d0*w3p

                A(1,1:s)    = (/ w1,         w1p-w3+w4p,    w1p-w3-w4p,     w1-w5       /)
                A(2,1:s)    = (/ w1-w3p+w4,  w1p,           w1p-w5p,        w1-w3p-w4   /)
                A(3,1:s)    = (/ w1+w3p+w4,  w1p+w5p,       w1p,            w1+w3p-w4   /)
                A(4,1:s)    = (/ w1+w5,      w1p+w3+w4p,    w1p+w3-w4p,     w1          /)

                b(1:s)      = (/ 2d0*w1,        2d0*w1p,        2d0*w1p,        2d0*w1      /)
                c(1:s)      = (/ 1d0/2d0-w2,    1d0/2d0-w2p,    1d0/2d0+w2p,    1d0/2d0+w2  /)

            case ('radauia1')
                ! 
                ! Description : Radau IA (implicite ordre 1)
                ! Page 73, Hairer II

                s           = 1
                ordre       = 1
                infos       = 'implicit'

                A(1,1:s)    = (/ 1d0 /)
                b(1:s)      = (/ 1d0 /)
                c(1:s)      = (/ 0d0 /)

            case ('radauia3')
                ! Description : Radau IA (implicite ordre 3)
                ! Page 73, Hairer II

                s           = 2
                ordre       = 3
                infos       = 'implicit'

                A(1,1:s)    = (/ 1d0/4d0, -1d0/4d0 /)
                A(2,1:s)    = (/ 1d0/4d0,  5d0/12d0/)
                b(1:s)      = (/ 1d0/4d0,  3d0/4d0 /)
                c(1:s)      = (/ 0d0,      2d0/3d0 /)

            case ('radauia5')
                ! Description : Radau IA (implicite ordre 5)
                ! Page 73, Hairer II

                s           = 3
                ordre       = 5
                infos       = 'implicit'

                s6 = sqrt(6d0)

                A(1,1:s)    = (/ 1d0/9d0, (-1d0-s6)/18d0,       (-1d0+s6)/18d0 /)
                A(2,1:s)    = (/ 1d0/9d0, (88d0+7d0*s6)/360d0,  (88d0-43d0*s6)/360d0 /)
                A(3,1:s)    = (/ 1d0/9d0, (88d0+43d0*s6)/360d0, (88d0-7d0*s6)/360d0 /)

                b(1:s)      = (/ 1d0/9d0, (16d0+s6)/36d0,   (16d0-s6)/36d0 /)
                c(1:s)      = (/ 0d0,     (6d0-s6)/10d0,    (6d0+s6)/10d0  /)

            case ('radauiia1')
                ! Description : Radau IIA (implicite ordre 1)
                ! Page 74, Hairer II

                s           = 1
                ordre       = 1
                infos       = 'implicit'

                A(1,1:s)    = (/ 1d0 /)
                b(1:s)      = (/ 1d0 /)
                c(1:s)      = (/ 1d0 /)

            case ('radauiia3')
                ! Description : Radau IIA (implicite ordre 3)
                ! Page 74, Hairer II

                s           = 2
                ordre       = 3
                infos       = 'implicit'

                A(1,1:s)    = (/ 5d0/12d0, -1d0/12d0 /)
                A(2,1:s)    = (/ 3d0/4d0,  1d0/4d0/)

                b(1:s)      = (/ 3d0/4d0,  1d0/4d0 /)
                c(1:s)      = (/ 1d0/3d0,  1d0     /)

            case ('radauiia5')
                ! Description : Radau IIA (implicite ordre 5)
                ! Page 215, Hairer I ou 74, Hairer II

                s           = 3
                ordre       = 5
                infos       = 'implicit'

                s6 = sqrt(6d0)

                A(1,1:s)    = (/ (88d0-7d0*s6)/360d0,       (296d0-169d0*s6)/1800d0,    (-2d0+3d0*s6)/225d0 /)
                A(2,1:s)    = (/ (296d0+169d0*s6)/1800d0,   (88d0+7d0*s6)/360d0,        (-2d0-3d0*s6)/225d0 /)
                A(3,1:s)    = (/ (16d0-s6)/36d0,            (16d0+s6)/36d0,             1d0/9d0 /)

                b(1:s)      = (/ (16d0-s6)/36d0,    (16d0+s6)/36d0,     1d0/9d0 /)
                c(1:s)      = (/ (4d0-s6)/10d0,     (4d0+s6)/10d0,      1d0     /)

            case ('radaus')
                ! Description : Radau IIA (implicite et symplectique mais non symetrique, ordre 5)
                ! Page 318, Hairer I

                s           = 3
                ordre       = 5
                infos       = 'implicit'

                s6 = sqrt(6d0)

                A(1,1:s)    = (/ (16d0-s6)/72d0,            (328d0-167d0*s6)/1800d0,    (-2d0+3d0*s6)/450d0 /)
                A(2,1:s)    = (/ (328d0+167d0*s6)/1800d0,   (16d0+s6)/72d0,             (-2d0-3d0*s6)/450d0 /)
                A(3,1:s)    = (/ (85d0-10d0*s6)/180d0,      (85d0+10d0*s6)/180d0,       1d0/18d0            /)

                b(1:s)      = (/ (16d0-s6)/36d0,    (16d0+s6)/36d0,     1d0/9d0 /)
                c(1:s)      = (/ (4d0-s6)/10d0,     (4d0+s6)/10d0,      1d0     /)

            case ('lobatto4')
                !Butcher's Lobatto formula (implicite ordre 4)
                !page 211 Hairer I

                s           = 3
                ordre       = 4
                infos       = 'implicit'

                A(1,1:s)    = 0d0
                A(2,1:s)    = (/ 1d0/4d0, 1d0/4d0, 0d0  /)
                A(3,1:s)    = (/ 0d0, 1d0, 0d0  /)

                b(1:s)      = (/ 1d0/6d0, 2d0/3d0, 1d0/6d0 /)
                c(1:s)      = (/ 0d0, 1d0/2d0, 1d0 /)

            case ('lobatto6')
                !Butcher's Lobatto formula (implicite ordre 6)
                !page 211 Hairer I

                s           = 4
                ordre       = 6
                infos       = 'implicit'

                s5 = sqrt(5d0)

                A(1,1:s)    = 0d0
                A(2,1:s)    = (/ (5d0+s5)/60d0, 1d0/6d0, (15d0-7d0*s5)/60d0, 0d0  /)
                A(3,1:s)    = (/ (5d0-s5)/60d0 ,(15d0+7d0*s5)/60d0, 1d0/6d0, 0d0  /)
                A(4,1:s)    = (/ 1d0/6d0, (5d0-s5)/12d0, (5d0+s5)/12d0, 0d0  /)

                b(1:s)      = (/ 1d0/12d0, 5d0/12d0, 5d0/12d0, 1d0/12d0 /)
                c(1:s)      = (/ 0d0, (5d0-s5)/10d0, (5d0+s5)/10d0, 1d0 /)

            case ('lobattoiiia2')
                !Lobatto IIIA (implicite ordre 2)
                !page 75 Hairer II
                !
                s           = 2
                ordre       = 2
                infos       = 'implicit'

                A(1,1:s)    = 0d0
                A(2,1:s)    = (/ 1d0/2d0, 1d0/2d0 /)

                b(1:s)      = (/ 1d0/2d0, 1d0/2d0 /)
                c(1:s)      = (/ 0d0,     1d0     /)

            case ('lobattoiiia4')
                !Lobatto IIIA (implicite ordre 4)
                !page 75 Hairer II
                !
                s           = 3
                ordre       = 4
                infos       = 'implicit'

                A(1,1:s)    = 0d0
                A(2,1:s)    = (/ 5d0/24d0, 1d0/3d0, -1d0/24d0  /)
                A(3,1:s)    = (/ 1d0/6d0, 2d0/3d0, 1d0/6d0  /)

                b(1:s)      = (/ 1d0/6d0, 2d0/3d0, 1d0/6d0 /)
                c(1:s)      = (/ 0d0, 1d0/2d0, 1d0 /)

            case ('lobattoiiia6')
                !Lobatto IIIA (implicite ordre 6)
                !page 75 Hairer II

                s           = 4
                ordre       = 6
                infos       = 'implicit'

                s5 = sqrt(5d0)

                A(1,1:s)    = 0d0
                A(2,1:s)    = (/ (11d0+s5)/120d0, (25d0-s5)/120d0, (25d0-13d0*s5)/120d0, (-1d0+s5)/120d0  /)
                A(3,1:s)    = (/ (11d0-s5)/120d0 ,(25d0+13d0*s5)/120d0, (25d0+s5)/120d0, (-1d0-s5)/120d0  /)
                A(4,1:s)    = (/ 1d0/12d0, 5d0/12d0, 5d0/12d0, 1d0/12d0  /)

                b(1:s)      = (/ 1d0/12d0, 5d0/12d0, 5d0/12d0, 1d0/12d0 /)
                c(1:s)      = (/ 0d0, (5d0-s5)/10d0, (5d0+s5)/10d0, 1d0 /)

            case ('lobattoiiib2')
                !Lobatto IIIB (implicite ordre 2)
                !page 76 Hairer II
                !
                s           = 2
                ordre       = 2
                infos       = 'implicit'

                A(1,1:s)    = (/ 1d0/2d0, 0d0 /)
                A(2,1:s)    = (/ 1d0/2d0, 0d0 /)

                b(1:s)      = (/ 1d0/2d0, 1d0/2d0 /)
                c(1:s)      = (/ 0d0,     1d0     /)

            case ('lobattoiiib4')
                !Lobatto IIIB (implicite ordre 4)
                !page 76 Hairer II
                !
                s           = 3
                ordre       = 4
                infos       = 'implicit'

                A(1,1:s)    = (/ 1d0/6d0, -1d0/6d0, 0d0  /)
                A(2,1:s)    = (/ 1d0/6d0,  1d0/3d0, 0d0  /)
                A(3,1:s)    = (/ 1d0/6d0,  5d0/6d0, 0d0  /)

                b(1:s)      = (/ 1d0/6d0, 2d0/3d0, 1d0/6d0 /)
                c(1:s)      = (/ 0d0, 1d0/2d0, 1d0 /)

            case ('lobattoiiib6')
                !Lobatto IIIB (implicite ordre 6)
                !page 76 Hairer II

                s           = 4
                ordre       = 6
                infos       = 'implicit'

                s5 = sqrt(5d0)

                A(1,1:s)    = (/ 1d0/12d0, (-1d0-s5)/24d0,          (-1d0+s5)/24d0,         0d0  /)
                A(2,1:s)    = (/ 1d0/12d0, (25d0+s5)/120d0,         (25d0-13d0*s5)/120d0,   0d0  /)
                A(3,1:s)    = (/ 1d0/12d0, (25d0+13d0*s5)/120d0,    (25d0-s5)/120d0,        0d0  /)
                A(4,1:s)    = (/ 1d0/12d0, (11d0-s5)/24d0,          (11d0+s5)/24d0,         0d0  /)

                b(1:s)      = (/ 1d0/12d0, 5d0/12d0, 5d0/12d0, 1d0/12d0 /)
                c(1:s)      = (/ 0d0, (5d0-s5)/10d0, (5d0+s5)/10d0, 1d0 /)

            case ('lobattoiiic2')
                !Lobatto IIIC (implicite ordre 2)
                !page 76 Hairer II
                !
                s           = 2
                ordre       = 2
                infos       = 'implicit'

                A(1,1:s)    = (/ 1d0/2d0, -1d0/2d0 /)
                A(2,1:s)    = (/ 1d0/2d0, 1d0/2d0 /)

                b(1:s)      = (/ 1d0/2d0, 1d0/2d0 /)
                c(1:s)      = (/ 0d0,     1d0     /)

            case ('lobattoiiic4')
                !Lobatto IIIC (implicite ordre 4)
                !page 76 Hairer II
                !
                s           = 3
                ordre       = 4
                infos       = 'implicit'

                A(1,1:s)    = (/ 1d0/6d0, -1d0/3d0, 1d0/6d0  /)
                A(2,1:s)    = (/ 1d0/6d0,  5d0/12d0, -1d0/12d0  /)
                A(3,1:s)    = (/ 1d0/6d0,  2d0/3d0, 1d0/6d0  /)

                b(1:s)      = (/ 1d0/6d0, 2d0/3d0, 1d0/6d0 /)
                c(1:s)      = (/ 0d0, 1d0/2d0, 1d0 /)

            case ('lobattoiiic6')
                !Lobatto IIIC (implicite ordre 6)
                !page 76 Hairer II

                s           = 4
                ordre       = 6
                infos       = 'implicit'

                s5 = sqrt(5d0)

                A(1,1:s)    = (/ 1d0/12d0, -s5/12d0,            s5/12d0,            -1d0/12d0   /)
                A(2,1:s)    = (/ 1d0/12d0, 1d0/4d0,             (10d0-7d0*s5)/60d0, s5/60d0     /)
                A(3,1:s)    = (/ 1d0/12d0, (10d0+7d0*s5)/60d0,  1d0/4d0,            -s5/60d0    /)
                A(4,1:s)    = (/ 1d0/12d0, 5d0/12d0,            5d0/12d0,           1d0/12d0    /)

                b(1:s)      = (/ 1d0/12d0, 5d0/12d0, 5d0/12d0, 1d0/12d0 /)
                c(1:s)      = (/ 0d0, (5d0-s5)/10d0, (5d0+s5)/10d0, 1d0 /)

            case ('sdirk3')
                !SDIRK method (implicite ordre 3)
                !page 207 Hairer I
                !
                s           = 2
                ordre       = 3
                infos       = 'implicit'

                gam = (3d0+sqrt(3d0))/6d0

                A(1,1:s)    = (/ gam,           0d0 /)
                A(2,1:s)    = (/ 1d0-2d0*gam,   gam /)

                b(1:s)      = (/ 1d0/2d0, 1d0/2d0 /)
                c(1:s)      = (/ gam, 1d0-gam /)

            case ('sdirk4l')
                !L-stable SDIRK method of order 4 (implicite ordre 4)
                !page 100 Hairer II
                !
                s           = 5
                ordre       = 4
                infos       = 'implicit'

                gam = 1d0/4d0

                A(1,1:s)    = (/ gam, 0d0, 0d0, 0d0, 0d0 /)
                A(2,1:s)    = (/ 1d0/2d0, gam, 0d0, 0d0, 0d0 /)
                A(3,1:s)    = (/ 17D0/50D0, -1D0/25D0, gam, 0D0, 0D0 /)
                A(4,1:s)    = (/ 371D0/1360D0, -137D0/2720D0, 15D0/544D0, gam, 0D0 /)
                A(5,1:s)    = (/ 25D0/24D0, -49D0/48D0, 125D0/16D0, -85D0/12D0, gam /)

                b(1:s)      = (/ 25D0/24D0, -49D0/48D0, 125D0/16D0, -85D0/12D0, gam /)
                c(1:s)      = (/ 1D0/4D0, 3D0/4D0, 11D0/20D0, 1D0/2D0, 1D0 /)

            case ('sdirk4a')
                !A-stable SDIRK method of order 4 (implicite ordre 4)
                !page 100 Hairer II
                !
                s           = 3
                ordre       = 4
                infos       = 'implicit'

                gam = cos(3.14159265359d0/18D0)/sqrt(3d0) + 1D0/2D0
                del = 1D0/(6D0*(2*gam-1D0)**2)

                A(1,1:s)    = (/ gam, 0D0, 0D0 /)
                A(2,1:s)    = (/ 1D0/2D0-gam, gam, 0D0 /)
                A(3,1:s)    = (/ 2D0*gam, 1D0-4D0*gam, gam /)

                b(1:s)      = (/ del, 1D0-2D0*gam, del /)
                c(1:s)      = (/ gam, 1D0/2d0, 1D0-gam /)

            case ('dirk5')
                !A-stable SDIRK method of order 5 (implicite ordre 5)
                !page 101 Hairer II
                !
                s           = 5
                ordre       = 5
                infos       = 'implicit'

                s6 = sqrt(6D0)
                gam = (6D0-s6)/10D0

                A(1,1:s)    = (/ gam, 0D0, 0D0, 0D0, 0D0 /)
                A(2,1:s)    = (/ (-6D0+5D0*s6)/14D0, gam, 0D0, 0D0, 0D0 /)
                A(3,1:s)    = (/ (888D0+607D0*s6)/2850D0, (126D0-161D0*s6)/1425D0, gam, 0D0, 0D0 /)
                A(4,1:s)    = (/ (3153D0-3082D0*s6)/14250D0, (3213D0+1148D0*s6)/28500D0, (-267D0+88D0*s6)/500D0, gam, 0D0 /)
                A(5,1:s)    = (/ (-32583D0+14638D0*s6)/71250D0, (-17199D0+364D0*s6)/142500D0, (1329D0-544D0*s6)/2500D0, &
                                    (-96D0+131D0*s6)/625D0, gam /)

                b(1:s)      = (/ 0D0, 0D0, 1D0/9D0, (16D0-s6)/36D0, (16D0+s6)/36D0, 0D0 /)
                c(1:s)      = (/ (6D0-s6)/10D0, (6D0+9D0*s6)/35D0, 1D0, (4D0-s6)/10D0, (4D0+s6)/10D0 /)

            case default

                s     = 0
                ordre = 0
                infos = 'error'

        end select

        select case (infos)

            case ('error')

                CALL printandstop('  ||| ERROR: Integrate -> Unknown integrator' // ' ' // inte)

            case default

                call createButcherTable(BTable,A(1:s,1:s),b(1:s),c(1:s),s,ordre,infos)

        end select

    end subroutine getButcherTab

end module mod_integration
