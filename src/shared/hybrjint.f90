!! -----------------------------------------------------------------------------
!!
!! Copyright 2016, Olivier Cots.
!!
!! This file is part of HamPath.
!!
!! HamPath is free software: you can redistribute it and/or modify
!! it under the terms of the GNU Lesser General Public License as published by
!! the Free Software Foundation, either version 3 of the License, or
!! (at your option) any later version.
!!
!! HamPath is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU Lesser General Public License for more details.
!!
!! You should have received a copy of the GNU Lesser General Public License
!! along with HamPath. If not, see <http://www.gnu.org/licenses/>.
!!
!! -----------------------------------------------------------------------------
!!
!>     @ingroup ssolvePackage
!!     @brief  hybrj interface.
!!     @param[in] jacfun    gives function for which one wants to find a zero and its Jacobian
!!    \param[in] ny         dimension of y
!!    \param[in] y0i        initial guess
!!    \param[in] lparaux    number of parameters
!!    \param[in] paraux     parameters given to jacfun
!!    \param[in] xtol       tolerance
!!    \param[in] maxfev     maximum number of evaluations of fcn
!!    \param[in] ysol       solution
!!    \param[in] fysol      evaluation of fcn at the solution
!!    \param[in] nfev       number of evaluations of the function
!!    \param[in] njac       number of evaluations of the Jacobian
!!    \param[in] flag       should be 1
!!
!!  \author Olivier Cots
!!  \date   2009-2013
!!  \copyright LGPL
!!
Subroutine hybrjint(jacfun,ny,y0i,lparaux,paraux,xtol,maxfev,ysol,fysol,nfev,njac,flag)
    implicit none
    integer,            intent(in)                    :: ny
    integer,            intent(in)                    :: lparaux,maxfev
    double precision,   intent(in)                    :: xtol
    double precision,   intent(inout), dimension(lparaux) :: paraux
    double precision,   intent(in), dimension(ny)     :: y0i
    double precision,   intent(out), dimension(ny)    :: ysol
    double precision,   intent(out), dimension(ny)    :: fysol
    integer,            intent(out)                   :: nfev
    integer,            intent(out)                   :: njac
    integer,            intent(out)                   :: flag

    interface
        Subroutine jacfun(ny,y,paraux,lparaux,nfev,fvec,fjac,ldfjac,iflag)
        implicit none
        integer,            intent(in)                        :: ny
        integer,            intent(in)                        :: lparaux
        integer,            intent(in)                        :: ldfjac
        integer,            intent(in)                        :: nfev
        double precision,   intent(inout),  dimension(lparaux)   :: paraux
        double precision,   intent(in),  dimension(ny)        :: y
        double precision,   intent(out), dimension(ny)        :: fvec
        double precision,   intent(out), dimension(ldfjac,ny) :: fjac
        integer,            intent(inout)                     :: iflag
        end Subroutine jacfun
    end interface

    !local declarations for HYBRJ
    integer                                     :: mode
    integer                                     :: np
    integer                                     :: l
    integer                                     :: ifail
    double precision                            :: fa
    double precision, dimension(ny)             :: diag
    double precision, dimension(ny)             :: qtf
    double precision, dimension(ny)             :: w1
    double precision, dimension(ny)             :: w2
    double precision, dimension(ny)             :: w3
    double precision, dimension(ny)             :: w4
    double precision, dimension((ny*(ny+1))/2)  :: r
    double precision, dimension(ny,ny)          :: fjac

    !HYBRJ solver
    ifail   = -1
    mode    = 1
    fa      = 100d0
    np      = 0
    l       = (ny*(ny+1))/2
    njac    = 0
    nfev    = 0
    w1      = 0d0
    w2      = 0d0
    w3      = 0d0
    w4      = 0d0
    qtf     = 0d0
    fysol   = 0d0
    diag    = 1d0
    fjac    = 0d0
    r       = 0d0

    ysol    = y0i

    call hybrj(jacfun,ny,ysol,paraux,lparaux,fysol,fjac,ny,xtol,maxfev, &
                diag,mode,fa,np,ifail,nfev,njac,r,l,qtf,w1,w2,w3,w4)

    flag    = ifail

end subroutine hybrjint
