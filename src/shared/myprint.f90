!! -----------------------------------------------------------------------------
!!
!! Copyright 2016, Olivier Cots.
!!
!! This file is part of HamPath.
!!
!! HamPath is free software: you can redistribute it and/or modify
!! it under the terms of the GNU Lesser General Public License as published by
!! the Free Software Foundation, either version 3 of the License, or
!! (at your option) any later version.
!!
!! HamPath is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU Lesser General Public License for more details.
!!
!! You should have received a copy of the GNU Lesser General Public License
!! along with HamPath. If not, see <http://www.gnu.org/licenses/>.
!!
!! -----------------------------------------------------------------------------
!!
!>     \ingroup commonUtils
!!     \brief   Print a message in fortran, matlab or other interface.
!!       \param[in] message     Message to print
!!       \param[in] doprintOpt  In matlab case, drawnow is executed if doprintOpt is true
!!                              In fortran case, advance = no if false and yes if true
!!                              It is an optional parameter which has a true default value
!!
!!  \author Olivier Cots
!!  \date   2009-2013
!!  \copyright LGPL
!!
subroutine myprint(message,doprintOpt)
    implicit none
    character (LEN=*), intent(in)   :: message
    logical, intent(in), optional   :: doprintOpt

    !local variables
    logical :: doprint

    IF (PRESENT(doprintOpt)) THEN
        doprint = doprintOpt
    ELSE
        doprint = .true.
    END IF

!#if defined(InterfaceFortran) || defined(InterfacePython) || defined(InterfaceMatlabFile)
    IF (doprint) THEN
        write(*,'(a)',advance='yes') trim(message)
    ELSE
        write(*,'(a)',advance='no') trim(message)
    END IF
!#endif

!#if defined(InterfaceMatlabMex)
!    call mexPrintf(trim(message))
!    if(doprint)then
!        call mexPrintf('\n')
!        call mexEvalString("drawnow;")
!    end if
!#endif

end subroutine myprint
