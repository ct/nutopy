!! -----------------------------------------------------------------------------
!!
!! Copyright 2016, Olivier Cots.
!!
!! This file is part of HamPath.
!!
!! HamPath is free software: you can redistribute it and/or modify
!! it under the terms of the GNU Lesser General Public License as published by
!! the Free Software Foundation, either version 3 of the License, or
!! (at your option) any later version.
!!
!! HamPath is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU Lesser General Public License for more details.
!!
!! You should have received a copy of the GNU Lesser General Public License
!! along with HamPath. If not, see <http://www.gnu.org/licenses/>.
!!
!! -----------------------------------------------------------------------------
!!
!>     \ingroup commonUtils
!!     \brief  Print message and then stop execution of the program.
!!       \param[in] message Message to print
!!
!!  \author Olivier Cots
!!  \date   2009-2013
!!  \copyright LGPL
!!
subroutine printandstop(message)
    implicit none
    character (LEN=*), intent(in)   :: message

!#if defined(InterfaceFortran) || defined(InterfacePython) || defined(InterfaceMatlabFile)
    write(*,'(a)',advance='yes') trim(message)
    write(*,*) ''
    stop
!#endif

!#if defined(InterfaceMatlabMex)
!!        call mexPrintf(trim(message))
!!        call mexPrintf('\n')
!!        call mexEvalString("drawnow;")

!!        write(*,'(a)',advance='yes') trim(message)
!!        write(*,*) ''
!!        stop

!!        call mexErrMsgTxt(trim(message))
!!        call mexWarnMsgTxt(trim(message)); stop

!    call mexPrintf(trim(message)); stop;
!#endif

end subroutine printandstop
