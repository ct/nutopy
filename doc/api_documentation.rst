API documentation
=================

.. toctree::
   :maxdepth: 2

   api/misc/errors
   api/ivp
   api/nle
   api/ocp
   api/tools
   api/path

