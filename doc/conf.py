#!/usr/bin/env python3

import os
import sys

sys.path.insert(0, os.path.abspath('../src'))

# Use sphinx-quickstart to create your own conf.py file!
# After that, you have to edit a few things.  See below.

# ----------------------------------------------------------------------------------------
# ----------------------------------------------------------------------------------------
# Select nbsphinx and, if needed, other Sphinx extensions:
extensions = [
#    'nbsphinx',
#    'nbsphinx_link',                # A sphinx extension for including notebook files from outside the sphinx source root.
#    'sphinxcontrib.bibtex',         # for bibliographic references
#    'sphinx_gallery.load_style',    # load CSS for gallery (needs SG >= 0.6)
    ##
    'sphinx.ext.viewcode',
    'sphinx.ext.autodoc',   # for generation of api documentation
    'sphinx.ext.napoleon',  # numpy docstrings
    'sphinx.ext.intersphinx', # to reference others projects, as numpy.ndarray
    'sphinx_copybutton',  # for "copy to clipboard" buttons
    'sphinx.ext.mathjax',           # for math equations
#    'recommonmark',         # for the use of markdown files for the documentation
    'matplotlib.sphinxext.plot_directive',
]

## Napoleon settings
napoleon_google_docstring = True
napoleon_numpy_docstring = True
#napoleon_include_init_with_doc = False
#napoleon_include_private_with_doc = False
#napoleon_include_special_with_doc = True
napoleon_use_admonition_for_examples = True
napoleon_use_admonition_for_notes = True
napoleon_use_admonition_for_references = False
napoleon_use_ivar = True
napoleon_use_param = True
napoleon_use_rtype = False
napoleon_use_keyword = True

# The name of the Pygments (syntax highlighting) style to use.
pygments_style = 'sphinx'

intersphinx_mapping = {'python': ('https://docs.python.org/3', None),
                       'numpy': ('https://numpy.org/doc/stable', None),
                       'scipy': ('https://docs.scipy.org/doc/scipy/reference', None),
                       'matplotlib': ('https://matplotlib.org', None)}

# Default language for syntax highlighting in reST and Markdown cells:
highlight_language = 'none'

# Exclude build directory and Jupyter backup files:
exclude_patterns = ['_build', '**.ipynb_checkpoints', 'Thumbs.db', '.DS_Store']

mathjax_config = {
    'TeX': {'equationNumbers': {'autoNumber': 'AMS', 'useLabelIds': True}},
}

# ----------------------------------------------------------------------------------------
# ----------------------------------------------------------------------------------------
# -- The settings below this line are not specific to nbsphinx ------------
#

# The suffix(es) of source filenames.
source_suffix = {
    '.rst': 'restructuredtext',
    '.md': 'markdown'
}

# where we stop the documentation: when coming to fortran files
autodoc_mock_imports = ['nutopy.mod_nlesolve',
                        'nutopy.mod_ivpsolve',
                        'nutopy.mod_pathsolve',
                        ]

# Default processing flags for sphinx
autoclass_content = 'class'
autodoc_member_order = 'bysource'
autodoc_default_options = {
    'members': True,
    'undoc-members':True,
    'show-inheritance':True
}

#autodoc_member_order = 'alphabetical' #'bysource'
add_module_names = False
add_function_parentheses = False

#---sphinx-themes-----
html_theme = 'pydata_sphinx_theme'

html_theme_options = {
#    "collapse_navigation": True,
    "navigation_depth": 1,
    "show_toc_level": 1,
    "use_edit_page_button": False,
    "external_links": [
        {"url": "https://ct.gitlabpages.inria.fr/gallery/", "name": "ct Gallery"},
        {"url": "https://ct.gitlabpages.inria.fr/bocop3/", "name": "Bocop Docs"}
    ],
    "gitlab_url": "https://gitlab.inria.fr/ct/nutopy",
     "navbar_align": "left",  # [left, content, right] For testing that the navbar items align properly
     "navbar_end": ["search-field", "navbar-icon-links"],  # Just for testing
     "search_bar_text": "Search...",
     #"page_sidebar_items": ["page-toc", "edit-this-page"],
     #"footer_items": ["copyright", "sphinx-version", ""]
}


# Remove the sidebar from some pages
html_sidebars = {
    "**": [], # no left sidebar
#    "**": ["sidebar-nav-bs", "sidebar-ethical-ads"],
}

html_context = {
    "gitlab_url": "https://gitlab.inria.fr", # or your self-hosted GitLab
    "gitlab_user": "ct",
    "gitlab_repo": "nutopy",
    "gitlab_version": "master",
    "doc_path": "examples",
}

html_logo   = 'logo-nt.svg'

author      = 'Olivier Cots'
project     = u'control toolbox gallery'
import time
copyright   =  u'%s, ct project' % time.strftime('%Y') # a changer
#version     = '0.2' # a voir comment gerer doc et version
#release     = '0.2.1'

master_doc = 'index'


# -- Get version information and date from Git ----------------------------

try:
    from subprocess import check_output
    release = check_output(['git', 'describe', '--tags', '--always'])
    release = release.decode().strip()
    today = check_output(['git', 'show', '-s', '--format=%ad', '--date=short'])
    today = today.decode().strip()
except Exception:
    release = '<unknown>'
    today = '<unknown date>'

# -- Options for HTML output ----------------------------------------------

html_title = project #+ ' version ' + release
