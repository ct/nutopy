===============================
Welcome to nutopy documentation
===============================

The `nutopy` package aims to provide numerical tools to define and solve optimal control problems.
It is mainly based on indirect methdods such as indirect shooting, differential homotopy and
conjugate points calculation.

It contains:

- `ivp`: numerical integration methods;
- `nle`: non linear equations solver;
- `ocp`: class to define Hamiltonians, flow of Hamiltonian vector fields, etc;
- `path`: differential homotopy;

Please visit the `ct Gallery <https://ct.gitlabpages.inria.fr/gallery/>`_ for examples.

Installation
============

You need first `conda <https://docs.conda.io>`_ to install `nutopy` package.
You can install `conda` as part of a `Miniforge <https://github.com/conda-forge/miniforge>`_ installer.

.. code-block:: console

   conda install -c control-toolbox -c conda-forge nutopy

User guide
==========

.. toctree::
   :maxdepth: 3

   api_documentation

Examples
========

Please visit the `ct Gallery <https://ct.gitlabpages.inria.fr/gallery/>`_ for examples.

Index
======

* :ref:`genindex`

.. * :ref:`modindex`
.. * :ref:`search`

